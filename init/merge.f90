program merge

! Merges two hydra data files

  use io_routines
  use itype_definitions
  use particle_data
  use run_parameters
  use sph_parameters
  use units

  ! Files
  character*11 :: file1='d0180.01000'
  character*11 :: file2='d0181.01000'
  character*11 :: file3='d0182.00000'

  integer :: nobj1, nobj2, nobj_m

  ! Temporary arrays 
  integer, allocatable :: itype_m(:)    ! particle type identifier
  real, allocatable :: rm_m(:)          ! mass
  real, allocatable :: r_m(:,:)         ! position
  real, allocatable :: v_m(:,:)         ! velocity
  real, allocatable :: h_m(:)           ! smoothing length
  real, allocatable :: dn_m(:)          ! surface density

  ! Find size of arrays
  datafile_start=file2
  call read_data
  nobj2=nobj
  print*,'nobj2=',nobj2
  deallocate(itype,rm,r,v,a)
  deallocate(h,dn)
  datafile_start=file1
  call read_data
  nobj1=nobj
  print*,'nobj1=',nobj1
  nobj_m=nobj1+nobj2

  ! Allocate arrays
  allocate(itype_m(nobj_m))
  allocate(rm_m(nobj_m))
  allocate(r_m(2,nobj_m))
  allocate(v_m(2,nobj_m))
  allocate(h_m(nobj_m))
  allocate(dn_m(nobj_m))

  ! Load up first part of the arrays
  itype_m(1:nobj1)=itype(:)
  rm_m(1:nobj1)=rm(:)
  r_m(:,1:nobj1)=r(:,:)
  v_m(:,1:nobj1)=v(:,:)
  h_m(1:nobj1)=h(:)
  dn_m(1:nobj1)=dn(:)
  deallocate(itype,rm,r,v,a)
  deallocate(h,dn)

  ! And now the second set
  datafile_start=file2
  call read_data
  itype_m(nobj1+1:nobj_m)=itype(:)
  rm_m(nobj1+1:nobj_m)=rm(:)
  r_m(:,nobj1+1:nobj_m)=r(:,:)
  v_m(:,nobj1+1:nobj_m)=v(:,:)
  h_m(nobj1+1:nobj_m)=h(:)
  dn_m(nobj1+1:nobj_m)=dn(:)
  deallocate(itype,rm,r,v,a)
  deallocate(h,dn)

  ! Now need to rename in order to use the standard write routines
  nobj=nobj_m
  nbary=nobj
  print*,'nobj=',nobj
  allocate(itype(nobj))
  allocate(rm(nobj))
  allocate(r(2,nobj))
  allocate(v(2,nobj))
  allocate(h(nobj))
  allocate(dn(nobj))
  itype=itype_m
  rm=rm_m
  r=r_m
  v=v_m
  h=h_m
  dn=dn_m
  datafile=file3
  time=0.
  istep=0
  call dump_data

  deallocate(itype,rm,r,v)
  deallocate(h,dn)

end program merge
  
