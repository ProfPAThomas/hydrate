program create_square_large

! Creates some points on a uniform grid in a cuboid.
! The positions of the particles can be randomised within a grid cell.
! The temperature can be randomised uniformly in the log.

  use io_routines
  use itype_definitions
  use particle_data
  use random
  use run_parameters
  use sph_parameters
  use units

  implicit none

  integer, parameter :: irun_par=37
  real, parameter :: time_par=0.
  integer, parameter :: Ncell=1024
  integer, parameter :: Lbox(2)=(/ 5,5 /)

  character :: nobj_string*12
  character :: pos_file*72
  integer :: ix,iy,iobj_low,iobj_high
  real :: r_cell(2,Ncell)
  real :: rm0, h0, dnbar
  real :: r_offset(2)   ! Relative location of each cell

  Nsph=4096
  
  ! Allocate arrays
  Nobj=Ncell*product(Lbox)
  Nbary=Nobj

  ! Box dimensions
  pos_min(1)=0.
  pos_max(1)=Lbox(1)
  pos_min(2)=0.
  pos_max(2)=Lbox(2)

  ! Define units and set mean values
  dnbar=1.
  rm0=dnbar/float(Ncell)
  h0=sqrt(Nsph/(pi*Ncell))

  ! Allocate arrays
  allocate(itype(Nobj),rm(Nobj),r(2,Nobj),v(2,Nobj))
  allocate(h(Nbary),dn(Nbary))

  ! Some header bits and bobs
  irun=irun_par
  write(datafile_original,'(a1,i4.4,a6)') 'd',irun,'.00000'
  datafile=datafile_original
  istep=0; time=time_par

  ! Read in particle positions
  write(nobj_string,'(i12)') Ncell
  pos_file='../rundir_clean/data/glass_'//trim(adjustl(nobj_string))//'.dat'
  open(1,file=pos_file,status='old',form='unformatted')
  read(1) r_cell
  close(1)
  
  ! Populate arrays
  iobj_high=0
  do iy=1,Lbox(2)
     r_offset(2)=iy-1
     do ix=1,Lbox(1)
        r_offset(1)=ix-1
        iobj_low=iobj_high+1
        iobj_high=iobj_low+Ncell-1
        itype(iobj_low:iobj_high)=itype_gas
        rm(iobj_low:iobj_high)=rm0
        ! Need to spread r_offset to get correct shape
        r(:,iobj_low:iobj_high)=spread(r_offset,2,Ncell)+r_cell
        v(:,iobj_low:iobj_high)=0.
        h(iobj_low:iobj_high)=h0
        dn(iobj_low:iobj_high)=dnbar
     end do
  end do

  ! Write out 
  call dump_data

  ! Dellocate arrays
  deallocate(itype,rm,r,v)
  deallocate(h,dn)

end program create_square_large
