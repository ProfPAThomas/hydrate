program create_grid

! Creates some points on a uniform grid in the unit cube.
! The positions of the particles can be randomised within the lattice.

! These boxes should be run with -DRELAX to generate relaxed particle
! distributions.

  use io_routines
  use itype_definitions
  use particle_data
  use random
  use run_parameters
  use sph_parameters
  use units

  integer, parameter :: irun_par=0057
  real, parameter :: time_par=0.
#ifdef PERIODIC
  logical, parameter :: isolated=.false., periodic=.true.
#else
  logical, parameter :: isolated=.true., periodic=.false.
#endif
  real, parameter :: randomise_r=0.5  ! 0=uniform; 1=completely random lattice
  integer, parameter :: iseed=165423
  integer, parameter :: Lgrid=16
  integer, parameter :: N_per_grid=1
  real, parameter :: rm_per_particle=1./(N_per_grid*Lgrid**2) ! particle mass in 1000 kg

  integer, parameter :: Nbary_max=N_per_grid*(Lgrid+1)**3
  integer, parameter :: Nobj_max=Nbary_max
  real, parameter :: dgrid=1./Lgrid
  
  integer :: iobj, i_per_grid, ix, iy
  real :: pos_x, pos_y

  ! Temporary arrays used to truncate data arrays
  integer, allocatable :: temp_int(:)
  real, allocatable :: temp_real(:), temp_real2(:,:)

  ! Check flags
#ifdef PERIODIC
  if (isolated) STOP 'create_grid: wrong compilation options: PERIODIC'
#endif
#ifdef ISOLATED
  if (periodic) STOP 'create_grid: wrong compilation options: ISOLATED'
#endif

  ! Allocate arrays
  allocate(itype(Nobj_max),rm(Nobj_max),r(2,Nobj_max),v(2,Nobj_max))
  allocate(h(Nbary_max),dn(Nbary_max))

  ! Initialise random number generator
  call ran_uniform_reset(iseed)

  ! Set units
  units_length_si=1.
  units_mass_density_si=1000.
  units_time_si=1.

  ! Some header bits and bobs
  irun=irun_par
  write(datafile_original,'(a1,i4.4,a6)') 'd',irun,'.00000'
  datafile=datafile_original
  istep=0; time=time_par

  ! Populate unit area
  iobj=0
  do i_per_grid=1,N_per_grid

#ifdef PERIODIC
     do iy=1,Lgrid
        pos_y=(iy-0.5)*dgrid
        do ix=1,Lgrid
           pos_x=(ix-0.5)*dgrid
#else
     do iy=1,Lgrid+1
        pos_y=(iy-1)*dgrid
        do ix=1,Lgrid+1
           pos_x=(ix-1)*dgrid
#endif
           iobj=iobj+1
           itype(iobj)=itype_gas
           rm(iobj)=rm_per_particle
           r(1,iobj)=pos_x+randomise_r*dgrid*(0.5-ran_uniform())
           r(2,iobj)=pos_y+randomise_r*dgrid*(0.5-ran_uniform())
           v(:,iobj)=0.
           h(iobj)=dgrid*sqrt(Nsph/(pi*float(N_per_grid)))
           dn(iobj)=rm_per_particle*N_per_grid*Lgrid**2
        end do
     end do
  end do

  Nobj=iobj; print*,'Nobj=',Nobj
  Nbary=iobj; print*,'Nbary=',Nbary
  
#ifdef PERIODIC
  r=modulo(r,1.)
#endif

  ! Arrays may be larger than required, so need to truncate them
  allocate(temp_int(Nobj_max))
  temp_int=itype;deallocate(itype);allocate(itype(Nobj));itype=temp_int(1:Nobj)
  deallocate(temp_int)
  allocate(temp_real(Nobj_max))
  temp_real=rm; deallocate(rm); allocate(rm(Nobj)); rm=temp_real(1:Nobj)
  deallocate(temp_real)
  allocate(temp_real2(2,Nobj_max))
  temp_real2=r; deallocate(r); allocate(r(2,Nobj)); r=temp_real2(:,1:Nobj)
  temp_real2=v; deallocate(v); allocate(v(2,Nobj)); v=temp_real2(:,1:Nobj)
  deallocate(temp_real2)
  allocate(temp_real(Nbary_max))
  temp_real=h; deallocate(h); allocate(h(Nbary)); h=temp_real(1:Nbary)
  temp_real=dn; deallocate(dn); allocate(dn(Nbary)); dn=temp_real(1:Nbary)
  deallocate(temp_real)

  ! Write out 
  call dump_data

  ! Dellocate arrays
  deallocate(itype,rm,r,v)
  deallocate(h,dn)

end program create_grid
