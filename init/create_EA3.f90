program create_EA3

! Creates a rectangle of abutting cells with geometry 1xLrect
! The number of particles per grid cell is given by n_per_cell.
! The positions of the particles are read in from relaxed square boxes.
! The particles are put in at locations and speed to reproduce the flow
! in the EA benchmark test number 3.

  use io_routines
  use itype_definitions
  use particle_data
  use run_parameters
  use sph_parameters
  use units

  ! Run number
  integer, parameter :: irun_par=131
  real, parameter :: time_par=0.

  ! Box geometry 
  integer, parameter :: Lw=4               ! Width of box in units of unit cube
  integer, parameter :: n_per_cell=512
  integer, parameter :: n_sph=24
  real, parameter :: xmin=-100.            ! Bottom edge of box in m
  real, parameter :: xmax=300.             ! Upper edge of box in m
  real, parameter :: box=25.                ! Size of unit cube in m
  
  ! Water geometry
  real, parameter :: flowrate=0.65         ! R/w=m^2/s
  real, parameter :: tstart=10., tend=30.  ! Start and end of mass influx in seconds
  real, parameter :: tedge=5.              ! Boundary region
  logical, parameter :: vadjust=.false.    ! If true, adjust speed of water at edge 
                                           ! to reproduce correct profile after expansion (does not work)
  logical, parameter :: radjust=.true.    ! If true, adjust position of water at edge 
  real, parameter :: manning=0.01          ! Manning's coeff
  real, parameter :: slope=0.005           ! slope of inflow region

  ! Translate input box, if desired
  real, parameter :: r_shift(2)=(/ 0.,0. /)

  ! Temporary arrays used to truncate data arrays
  integer, allocatable :: temp_int(:)
  real, allocatable :: temp_real(:), temp_real2(:,:)

  character :: nobj_string*12,nsph_string*12
  character :: pos_file*72
  integer :: j,ix,iy,iobj_low,iobj_high
  integer :: iobj,jobj
  integer :: Lrect,Lfill
  integer :: Nobj_max, Nbary_max
  real :: r_offset(2)
  real, allocatable :: r_cell(:,:), h_cell(:)
  real :: rm0, v0(2), h0, dn0
  real :: xstart, xend, xedge

  ! Derived parameters
  dn0=(flowrate*manning)**0.6*slope**(-0.3)
  rm0=dn0*box**2/float(n_per_cell)
  h0=sqrt(Nsph/(pi*n_per_cell))*box
  v0(1)=flowrate/dn0
  v0(2)=0.
  xstart=-tstart*v0(1)
  xend=-tend*v0(1)
  xedge=tedge*v0(1)
  print*,'xend,xstart,xedge=',xend,xstart,xedge

  ! Box dimensions
  Lrect=ceiling((xmax-xmin)/box)
  pos_min(1)=xmin
  pos_max(1)=pos_min(1)+Lrect*box
  pos_min(2)=0.
  pos_max(2)=box*Lw

  ! Allocate arrays
  Lfill=ceiling((xstart-xend)/box)
  print*,'(xstart-xend)/box=',(xstart-xend)/box
  print*,'Lfill=',Lfill
  print*,'n_per_cell=',n_per_cell
  Nobj_max=Lw*Lfill*n_per_cell
  Nbary_max=Nobj_max
  print*,'Nobj_max=',Nobj_max
  allocate(itype(Nobj_max),rm(Nobj_max),r(2,Nobj_max),v(2,Nobj_max))
  allocate(h(Nbary_max),dn(Nbary_max))

  ! Some header bits and bobs
  irun=irun_par
  write(datafile_original,'(a1,i4.4,a6)') 'd',irun,'.00000'
  datafile=datafile_original
  istep=0; time=time_par

  ! Read in particle positions
  allocate(r_cell(2,n_per_cell),h_cell(n_per_cell))
  write(nobj_string,'(i12)') n_per_cell
  write(nsph_string,'(i12)') n_sph
  pos_file='../rundir_clean/data/glass_'//trim(adjustl(nobj_string)) &
       &//'_'//trim(adjustl(nsph_string))//'.dat'
  open(1,file=pos_file,status='old',form='unformatted')
  read(1) r_cell
  read(1) h_cell
  close(1)
  do j=1,2
     r_cell(j,:)=modulo(r_cell(j,:)+r_shift(j),1.)
  end do
  
  ! Loop over boxes
  iobj_high=0
  do iy=1,Lw
     r_offset(2)=float(iy-1)*box+pos_min(2)
     do ix=1,Lfill
        r_offset(1)=float(ix-1)*box+xend
        ! Define particle range
        iobj_low=iobj_high+1
        iobj_high=iobj_low+n_per_cell-1
        ! Set particle properties
        itype(iobj_low:iobj_high)=itype_gas
        rm(iobj_low:iobj_high)=rm0
        r(:,iobj_low:iobj_high)=spread(r_offset,2,n_per_cell)+r_cell*box
        v(:,iobj_low:iobj_high)=spread(v0,2,n_per_cell)
        h(iobj_low:iobj_high)=h_cell*box
        dn(iobj_low:iobj_high)=dn0
     end do
  end do
  if (iobj_high.ne.nobj_max) STOP 'Incorrect number of particles'

  ! Trim to correct range (eliminate particles with x>xstart)
  ! and at the same time adjust speed of particles within edge
  jobj=0
  do iobj=1,nobj_max
     if (r(1,iobj).le.xstart) then
        jobj=jobj+1
        itype(jobj)=itype(iobj)
        rm(jobj)=rm(iobj)
        h(jobj)=h(iobj)
        dn(jobj)=dn(iobj)
        r(:,jobj)=r(:,iobj)
        v(:,jobj)=v(:,iobj)
        if(vadjust) then
           if (r(1,jobj).lt.xend+xedge) then
              ! Note should really be const_g but units not set
              ! These velocity changes have to be in CoM frame, I think
              v(1,jobj)=v(1,jobj)+sqrt(2.*const_g_si*dn0*(xedge+xend-r(1,jobj))/xedge)
           else if (r(1,jobj).gt.xstart-xedge) then
              v(1,jobj)=v(1,jobj)-sqrt(2.*const_g_si*dn0*(xedge+r(1,jobj)-xstart)/xedge)
           end if
        else if (radjust) then
           if (r(1,jobj).lt.xend+xedge) then
              r(1,jobj)=xend-xedge+2*xedge*sqrt((r(1,jobj)-xend)/xedge)
              dn(jobj)=0.5*dn0*(r(1,jobj)-xend+xedge)/xedge
              h(jobj)=min(0.125*box,h0*sqrt(dn0/dn(jobj)))
           else if (r(1,jobj).gt.xstart-xedge) then
              r(1,jobj)=xstart+xedge-2*xedge*sqrt((xstart-r(1,jobj))/xedge)
              dn(jobj)=0.5*dn0*(xstart+xedge-r(1,jobj))/xedge
              ! This is a poor smoothing length estimate near dn(jobj)=0
              h(jobj)=min(0.125*box,h0*sqrt(dn0/dn(jobj)))
           end if           
        end if
     end if
  end do
  nobj=jobj
  nbary=nobj
  print*,'nobj=',nobj
  print*,
 
  ! Arrays may be larger than required, so need to truncate them
  allocate(temp_int(Nobj_max))
  temp_int=itype;deallocate(itype);allocate(itype(Nobj));itype=temp_int(1:Nobj)
  deallocate(temp_int)
  allocate(temp_real(Nobj_max))
  temp_real=rm; deallocate(rm); allocate(rm(Nobj)); rm=temp_real(1:Nobj)
  deallocate(temp_real)
  allocate(temp_real2(2,Nobj_max))
  temp_real2=r; deallocate(r); allocate(r(2,Nobj)); r=temp_real2(:,1:Nobj)
  temp_real2=v; deallocate(v); allocate(v(2,Nobj)); v=temp_real2(:,1:Nobj)
  deallocate(temp_real2)
  allocate(temp_real(Nbary_max))
  temp_real=h; deallocate(h); allocate(h(Nbary)); h=temp_real(1:Nbary)
  temp_real=dn; deallocate(dn); allocate(dn(Nbary)); dn=temp_real(1:Nbary)
  deallocate(temp_real)

  ! Write out 
  call dump_data

  ! Dellocate arrays
  deallocate(r_cell,h_cell)
  deallocate(itype,rm,r,v)
  deallocate(h,dn)

end program create_EA3
