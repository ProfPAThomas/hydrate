program add_vel

  use io_routines
  use itype_definitions
  use particle_data
  use run_parameters
  use sph_parameters
  use units


  ! Files
  character*11 :: file1='d0006.t0101'
  character*11 :: file2='d0007.00000'

  ! Necessary Parameters
  integer :: nobj1, nobj_n, i, j
  real :: depth_initial            ! Initial depth for working out Froude Number
  real :: C                        ! Mass flow per unit width
  real :: g=9.80665
  ! Temporary Arrays

  integer, allocatable :: itype_n(:)    ! particle type identifier
  real, allocatable :: rm_n(:)          ! mass
  real, allocatable :: r_n(:,:)         ! position
  real, allocatable :: v_n(:,:)         ! velocity
  real, allocatable :: h_n(:)           ! smoothing length
  real, allocatable :: dn_n(:)          ! surface density
  real, allocatable :: plus_vel(:,:)    ! velocity we wish to add
  real, allocatable :: temp(:)          ! temporary array

  ! Froude Number to be applied

  real :: Froude=0.2

  ! Find size of arrays

  datafile_start=file1
  call read_data
  nobj1=nobj
  print*,'nobj1=',nobj
  nobj_n=nobj1
  print*,'nobj_n=',nobj_n

  ! Allocate arrays
  allocate(itype_n(nobj_n))
  allocate(rm_n(nobj_n))
  allocate(r_n(2,nobj_n))
  allocate(v_n(2,nobj_n))
  allocate(h_n(nobj_n))
  allocate(dn_n(nobj_n))
  allocate(plus_vel(2,nobj_n))
  allocate(temp(size(dn)))
 

! Assign values
!Assign C 
   j = 0
   do i = 1, size(dn)
            if (r(1,i) .lt. 1.) then
                j = j + 1
                temp(j) = dn(i)
            endif
  enddo
  depth_initial = sum(temp(:j))/size(temp(:j))
  C=(Froude*g*depth_initial)**0.5

  r_n=r
  dn_n=dn
  plus_vel(1,:)=C/dn(:)
  itype_n=itype
  rm_n=rm
  v_n(:,:)=v(:,:)+plus_vel(:,:)
  h_n=h
  deallocate(itype,rm,r,v,a)
  deallocate(h,dn)

  ! Now need to rename in order to use the standard write routines
  nobj=nobj_n
  nbary=nobj
  allocate(itype(nobj))
  allocate(rm(nobj))
  allocate(r(2,nobj))
  allocate(v(2,nobj))
  allocate(h(nobj))
  allocate(dn(nobj))
  itype=itype_n
  rm=rm_n
  r=r_n
  v=v_n
  h=h_n
  dn=dn_n
  datafile=file2
  time=0.
  istep=0
  call dump_data

  deallocate(itype,rm,r,v)
  deallocate(h,dn)

end program add_vel
