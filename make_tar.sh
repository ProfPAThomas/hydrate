mv rundir rundir_bak
cp -pR rundir_clean rundir

tar cvzf hydrate.tgz \
    --exclude *.jpg \
    --exclude *.mod \
    --exclude *.o \
    --exclude *.pdf \
    --exclude *.png \
    --exclude *.ps \
    Makefile \
    makeflags \
    makerules \
    hydra \
    idl \
    init \
    lib \
    modules \
    python \
    rundir \
    rundir_clean

mv rundir_bak rundir
