\documentclass[User_Manual.tex]{subfiles}
\begin{document}
The \texttt{run\_parameters.dat} file is found in your run directory, \texttt{rundir}. It contains a whole bunch of information such; as the name of the file containing the initial data; when to dump data; the Manning coefficient for the simulation, etc. If you haven’t messed around with your \texttt{run\textunderscore paramaters} file yet, it should look like this:

\begin{verbatim}
&nml_verbosity
verbosity=1                    ! control info written to terminal
                               ! 0=none, 1=normal, 2=verbose,
                               ! 3=diagnostic
/

&nml_startfile
datafile_start = 'd0001.00000'  ! starting data file
irun           = 1              ! run number
/

&nml_control_parameters
istep_end = 1000000             ! final timestep
istep_dump_frequency = 1000000  ! frequency of data dumps based 
                                ! on time step (permanent)
istep_backup_frequency = 100000 ! frequency of data backups 
                                !(temporary)
time_end  = 100.                 ! final time
time_dump_frequency=-1.         ! frequency at of data dumps based
                                ! on time, if <0 then does not dump
                                ! at all
time_out(1)=1.0                 ! populate output time array at 
                                !specific time
time_out(2)=20.0                ! populate output time array at
                                ! specific time
time_out(3)=40.0                ! populate output time array at
                                ! specific time
time_out(4)=60.0                ! populate output time array at
                                ! specific time
time_out(5)=80.0                ! populate output time array at
                                ! specific time
time_out(6)=100.0                ! populate output time array at
                                ! specific time
dtime_vel_multiplier=0.4        ! timestep multiplier: velocity 
                                ! default 0.4
dtime_acc_multiplier=0.25       ! timestep mult: acceleration 
                                ! default 0.25
dtime_man_multiplier=0.4        ! timestep multiplier: manning drag
                                ! default 0.4
damping='linear'                ! quadratic, linear or none (default)
damping_multiplier=3.           ! damping timescale (/dtime); default
                                ! infinity
soft=0.01                       ! Minimum search length
surface_file='surface_test.dat' ! Empty, or absent, implies flat 
                                ! surface
manning_si=0.02                 ! Manning drag coefficient: 
                                ! default 0.
walls_file=''                   ! Empty, or absent, implies 
                                ! no walls
/

! The following control parameters are all optional
&nml_sph_control_parameters
Lsph_max = 0            ! if>0 the max size of sph search grid
Lsph_search=1           ! search length in cell units 
                        !(defaults to 1)
Nsph = 24               ! number of sph neighbours 
                        !(defaults to 24)
/

&nml_track_particle
i_track_particle=1             ! id of particle to track
/
\end{verbatim}

There’s quite a lot of stuff there, and it might look quite daunting, but don't worry, anything following an exclamation mark is a comment or disabled, and much of it can be left alone for simple use. The main parts that will be interesting to you are contained between verbosity and walls\textunderscore file. If you want to understand a bit better how this information is used, look at \texttt{run_parameters.F90} in the \texttt{modules} directory.

The first option is verbosity. Rather than have hydra writing all the information possible to the terminal, you can tailor what is written to your satisfaction. If you aren't running diagnostics to debug you will probably want to set this to 1 or 0. Just be aware that if you set it to zero you won't be able to track the progress of your simulation.

The next two options are fairly self explanatory, \texttt{datafile_start} is the datafile that will be used in as the initial conditions of the particles in the simulation, whereas \texttt{irun} is the run number. This will be the integer that is written between the d and the point when hydra dumps data. You will probably want to keep irun to be the same as its counterpart in the datafile, unless you have a specific reason to, such as running multiple tests on the same initial data file. Note that you do not need to write \texttt{.data} after the data file name.

Following on there are a number of ways to specify when you wish hydra to dump data, and when to end the test. You choose a time step at which to end the simulation by specifying \texttt{istep_end}. You may also choose a time which if passed the simulation will end by specifying \texttt{time_end}. By specifying the \texttt{istep_dump_frequency} or {\tt time_dump_freq}, you can choose with what time step or time frequency hydra will dump data.

It is also possible to tell hydra to dump data when it reaches a specific designated time. This is done by writing \texttt{time_out(n)=\emph{time}}, where is n the nth specified output time and \emph{time} is the specified output time. By default n can go up to 100, but if you just really love specifying when to dump data, you can change this in \texttt{run_parameters.F90}. It should be noted that time here does not mean how long the program is running, but means time within the simulation!

As you can see, in the test file, \texttt{istep_end} and \texttt{istep_dump_frequency} are both set to 100000. This is an incredibly high number of steps, and essentially just means that the simulation ends when the data is dumped is determined entirely by the time passed.

Hydra needs a surface or floor to be simulated over, otherwise the whole idea of simulating the flow of water over a surface becomes nonsensical. Therefore we specify a \texttt{surface_file} in the form of a \texttt{.dat} containing all the information pertaining to the surface. If you do not enter a file name, hydra will use the pre-existing \texttt{surface_flat.dat}. This is unsurprisingly, just a flat surface.

Not quite all the information about the surfaces are contained in the surface file, there is in fact something called a Manning coefficient. It essentially describes how `sticky' a surface is. It creates a drag on the flow, slowing it. If desired, it can be changed by altering \texttt{manning_si}. We can see there is a small drag on the default test.

Hydra isn't just designed to simulate water flowing over surfaces, it is also meant to be able to simulate the effects of barriers or walls. The information for any walls in your run are contained in a \texttt{.dat} file similar to the surfaces. You specify which file contains this information by changing \texttt{walls_file}. It is currently empty, as there were no walls in the default test.

This concludes the description of all the options that you will probably need to change in your first few sets of simulations. There are however some others that do bear paying attention to for more advanced use.

Two important options are related to the damping of fluctuations in velocity of the simulation. This is so that unnecessarily short timesteps due to fluctuations on scales smaller than the smoothing length are avoided. The damping imposed can be chosen to be of a linear or quadratic variation. The latter is the better choice in theory, but in practice rarely makes a significant difference.

The scale of the damping can be specified by changing the {\tt damping_multip-
lier} option. Possibly counter intuitively, the lower this value is, the more damping that is applied. Setting it too low is ill advised as you may start changing the behaviour of the fluid, so we recommended to keep this above 2.

There is also the option to change the various multipliers that control how large a time step hydra can take. {\tt Hydra} works out the time step to take from the velocity of the particles, their acceleration, and the Manning drag. To stay on the safe side, it then multiplies these by the {\tt dtime} multipliers. It then takes the minimum of the resulting values and advances the time by that amount. By changing the {\tt dtime} multipliers you can adjust how large a timestep hydra will take, but if you make them too large you may not conserve the accuracy of the simulation.

Under the heading of {\tt nml_sph_control_parameters} are a trio of options for controlling the SPH searching operation. The way that the searching function works is by creating a grid to contain the particles, and then each particle tries to find enough neighbours perform the SPH calculation within {\tt Lsph_search} grid cells.  If specified {\tt Lsph_max} is the maximum number of cells in each direction. If it fails to find {\tt Nsph} neighbours, then it will decrease the number of cells from {\tt Lsph_max} so there is a less coarser, and repeats the search.

By altering {\tt Lsph_search} or {\tt Lsph_max} you may speed up (or slowdown) the code.




\end{document}