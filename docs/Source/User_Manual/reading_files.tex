\documentclass[User_Manual.tex]{subfiles}
\begin{document}
There have been four classes created to contain the information from {\tt surfa-\\ce}, {\tt wall}, {\tt qdp}, and {\tt data} and {\tt head} files. Though they carry out slightly different purposes they all work in more or less the same way. You define some object to be of that class, then use the applicable respective read method to assign whatever properties that class has, whether it is the number of objects in a {\tt .data} file, the thickness of a wall in a {\tt wall.dat} file, or the array of surface cell heights in a {\tt surface.dat} file.

The most important is {\tt datafile.py}. This file defines a class called {\tt data}. This contains all the information extracted from the header and data files of a dump. The method to read in data is simply {\tt read()}, which takes a string of the file name that you want as a mandatory argument. There are several `True'/`False' options in case you have enabled any of the SAVE_ACCEL, SAVE_SPH, SAVE_DEBUG, or TEST compilation options. In this case you should set accel=`True', sphflr=`True', debug=`True', or test=`True' and damp=`True' respectively (they are by default set to `False'.)
For example, if you were to want to look at the information contained in the dump {\tt d0001.t0010}, and you had enabled SAVE_ACCEL, then you would enter into python:
\begin{verbatim}
>>from datafile import *
>>dataobject=data()
>>dataobject.read('d0001.t0010',accel=`True')
\end{verbatim}

The first thing this will do is take any headers in the {\tt .head} file and create a dictionary of them under {\tt self.header}. It also finds the number of objects in the simulation and assigns this value to {\tt self.Nobj}. For a description of the various headers, see Appendix C.

Next it searches through the unformatted {\tt .data} file and extracts arrays containing the data of the properties of each particle. It stores them as follows:

\begin{description}
\item[self.itype :] A 1-D array of shape (Nobj). It contains the itype of each particle.
\item[self.mass :] A 1-D array of shape (Nobj). It contains the mass of each particle.
\item[self.pos :] A 2-D array of shape (Nobj,2). It contains the x and y coordinates of every particle, with x coordinate first.
\item[self.vel :] A 2-D array of shape (Nobj,2). It contains the velocity of each particle in the x and y direction, with x direction first.
\item[self.smth_len :] A 1-D array of shape (Nobj). It contains the smoothing length for each particle.
\item[self.density :] A 1-D array of shape (Nobj). It contains the surface density of the system at the particles position. This can be considered a representation of the depth at that point.
\end{description}
{\bf Optional Parameters :}
\begin{description}
\item[self.accel :] A 2-D array of shape (Nobj,2). Contains the saved acceleration output from the SAVE_ACCEL compilation option if enabled.
\item[self.flr_height :] A 1-D array of shape (Nobj). Contains the SPH evaluated floor height saved by enabling the SPH_FLOOR compilation option.
\item[self.debug :] A 1-D array of shape (Nobj). Contains the saved diagnostic info when SAVE_DEBUG is enabled as a compilation option.
\item[self.nnsep :] A 1-D array of shape (Nobj). Contains the separation between this particle and its nearest neighbouring particle. Only enabled when test=`True', and TEST is enabled as a compilation option.
\item[self.avg_1 :] A 1-D array of shape (Nobj). Contains the mean value of 1. Needed for damping. Enabled when damp=`True', and DAMP (part of TEST) is enabled as a compilation option.
\item[self.avg_r2 :] A 1-D array of shape (Nobj). Contains the mean value of position squared ($r^2$). Needed for damping. Enabled when damp=`True', and DAMP (part of TEST) is enabled as a compilation option.
\item[self.avg_r4 :]  A 1-D array array of shape (Nobj). Contains the mean value of position raised to the power four, ($r^4$).
\item[self.avg_vel :] A 2-D array of shape (Nobj,2). Contains the mean velocity. Needed for damping. Enabled when damp=`True', and DAMP (part of TEST) is enabled as a compilation option.
\item[self.avg_vr2 :] A 2-D array of shape (Nobj,2) Contains the mean value of velocity multiplied by $r^4$. Needed for damping. Enabled when damp=`True', and DAMP (part of TEST) is enabled as a compilation option.
\end{description}

We also have a class to read the information contained in a simulation run's {\tt .qdp} file. This class is contained inside {\tt qdp.py}. The class is called {\tt qdpdata}, and the method used to read in the {\tt qdp} file is {\tt qdpdata.read()}, where {\tt qdpdata} is the name of your initialised {\tt qdpdata} type object. This takes just the name of the irun as a mandatory argument, and then an option to set the number of steps with {\tt nstep}, which is by default set to {\tt nstep=10000}.

It stores information in the following way:

\begin{description}
\item[self.step :] A 1-D array of shape (nstep). Contains the step number.
\item[self.time :] A 1-D array of shape (nstep). Contains the time passed in the system at step.
\item[self.ke :] A 1-D array of shape (nstep). Contains the total kinetic energy of the system at step.
\item[self.the :] A 1-D array of shape (nstep). Contains the total thermal energy of the system at step.
\item[self.pe :] A 1-D array of shape (nstep). Contains the total potential energy of the system at step.
\item[self.de :] A 1-D array of shape (nstep). Contains the difference between total energy of the system at step and total energy of the system at the start of the simulation.
\item[self.error] A 1-D array of shape (nstep). Contains the fractional error between energy of the system at step and total energy of the system at the start of the simulation.
\end{description}


This leaves us with the two classes to read in the information pertaining to the surfaces and walls in the simulation. The class for surfaces is contained in {\tt surface.py}. The class itself is called {\tt surface}, and the reading method is called {\tt surface.read()} where {\tt surface} is the name of your initialised object of type {\tt surface}. Are you beginning to see a pattern?
This time the method just takes the surface file name as its only argument.
It stores information as follows:
\begin{description}
\item[self.location :] String which should give location of surface cells as either `corner' or `centre'.
\item[self.order :] String which should specify surface cells to be read in as either `row' (major order) or `column' (major order).
\item[self.ncols :] Integer giving the number of columns in the surface array.
\item[self.nrows :] Integer giving the number of rows in the surface array.
\item[self.xllcorner :] Gives x coordinate of either the edge of the first cell in the array or the centre depending on {\tt self.location}.
\item[self.yllcorner :] Gives y coordinate of either the edge of the first cell in the array or the centre depending on {\tt self.location}.
\item[self.cellsize :] Float containing the size of each cell.
\item[self.nodata_value :] Float. Currently has no function.
\item[self.x0 :] Gives x coordinate of cell centre before first cell.
\item[self.y0 :] Gives y coordinate of cell centre before first cell.
\item[self.cell :] A 2-D array of shape (ncols,nrows). Contains the height at the centre of each cell.
\item[self.cell_dx :] A 2-D array of shape (ncols-1,nrows-1). Contains the rate at which surface height increases with x in each cell.
\item[self.cell_dy :] A 2-D array of shape (ncols-1,nrows-1). Contains the rate at which surface height increases with y in each cell.
\item[self.cell_dxdy :] A 2-D array of shape (ncols-1,nrows-1). Contains the rate at which surface height changes in relation to both x and y.
\end{description}

Several of the plotting functions involve at some point of their calculations the height of the surface floor of the flow that they are evaluating. To find and calculate this a simple method for the {\tt surface} class called {\tt surface_height} has been created. It takes a position expressed as an x and y coordinate in an array of length two. It  returns the height of the surface floor at that point. If the position evaluated is such that it falls outside the area defined in the surface file then the optional argument {\tt extrapolate} should be chosen to be {\tt `True'}.

\begin{verbatim}
self.surface_height(r,extrapolate='False')

\end{verbatim}

\begin{description}
\item[surf :] A type {\tt surface} object. See {\tt read_surface}.
\item[r :] An array of length two which specifies a position on the surface. The first element should be the x position and the second the y position.
\item[extrapolate :] Can be set to either {\tt `False'} (the default) or {\tt `True'}. Determines whether {\tt surface_height} will extrapolate from the nearest known height when a position is outside the defined surface or just return an error.
\end{description}

This just leaves the class which reads in wall information from a {\tt wall.dat} file. The python file containing this class is {\tt wall.py}, the class itself is {\tt wall}, and the method to read the {\tt wall.dat} file is {\tt wall.read()} where {\tt wall} is the name of your initialised {\tt wall} type object. Like the {\tt surface.read} method, {\tt wall.read()} just takes the file name of the {\tt wall.dat} file. It should be noted it won't read any line with an exclamation mark as it considers this to be a commented line. It stores wall information such that the nth element (from the zeroth element onwards) in each array corresponds to the nth + 1 wall.

Information is stored as follows:

\begin{description}
\item[self.wallnumber :] Integer, number of walls that have been found in file.
\item[self.xpoint1 :] A 1-D array of shape (wallnumber). Contains the x coordinate of the first point of a wall. Walls are determined by drawing a line between two points. The nth element corresponds to the nth + 1 wall.
\item[self.ypoint1 :] A 1-D array of shape (wallnumber). Contains the y coordinate of the first point of a wall. The nth element corresponds to the nth + 1 wall.
\item[self.xpoint2 :] A 1-D array of shape (wallnumber). Contains the x coordinate of the second point of a wall. The nth element corresponds to the nth + 1 wall.
\item[self.ypoint2 :] A 1-D array of shape (wallnumber). Contains the y coordinate of the first point of a wall. The nth element corresponds to the nth + 1 wall.
\item[self.thickness :] A 1-D array of shape (wallnumber). Contains the thickness of a wall. The nth element corresponds to the nth + 1 wall.
\end{description}


\end{document}