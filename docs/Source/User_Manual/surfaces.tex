\documentclass[User_Manual.tex]{subfiles}
\begin{document}
If you were to navigate to the running directory of \hydrate, the default being \texttt{./rundir}, you will find a directory called \texttt{data}. Inside here a number of things are stored, such as the data dumped during any simulation you run, as well as the initial data files for a simulation generated using the codes in the \texttt{./init directory}. This is also where the \texttt{surface.dat} files are kept.

These files contain the information read by \texttt{surface.F90} located in the modules directory. It would be worth having a little look at this code, specifically the subroutine \texttt{surface_initialise}, to understand a little of how the information is read in and dealt with. 

The general principle is that \hydrate \ simulates the flow of water over surfaces or floors. We determine the form of these floors by specifying their height at step points. This essentially sets up a set of cells each containing a height value. The \texttt{surface.dat} file specifies these cell heights and how to read them. Then \texttt{surface\_initialise} and \texttt{surface\_height} linearly interpolate between the cells to find the height of the floor across the entire surface. 

If \textbf{SLOPE_EXTRAPOLATION} is defined in the makeflags (see compilation options), then \texttt{surface_height} will also be allowed to extrapolate beyond the first and last columns and rows. It is important to note that if \textbf{SLOPE_EXTRAPOLATION} is not defined then you must define the heights for 1 cell either side of the box you're interested in running your simulation in.

Inside a surface file the information is contained fairly simply. First there is a brief blurb as to the surfaces function, the next text should be the word location and then either {\tt centre} or {\tt corner}. This is to specify whether you wish to define cell {\tt location} by the x and y coordinates at the lower left hand corner, or whether you want to define them by the centre of the cell. 

The next line down should start with the word {\tt order}. This is to specify whether the cells are read row first or column first. Essentially what this means is that if you want each new column to specify the next cell in the x direction, you should write row after this. If on the other hand you want each new row to specify the next cell in the x direction, you should write column after order.

The next option is {\tt ncols}, which stands for number of columns. Unsurprisingly this should simply be the amount of columns you have in each row. You could put more columns than this in each row, but this would be pointless as none of them would be read by \texttt{surface_initialise}. Continuing on, we have {\tt nrows}. This is exactly the same as {\tt ncols}, but instead it specifies the number of rows to be read. 

After this you should enter two more lines, {\tt X0} and {\tt Y0}, in that order, and their values beside them. {\tt X0} specifies the X coordinate of the first cell, so the one that appears in the first row and first column. {\tt Y0} specifies the Y coordinate of the first cell. Make sure you take into account whether you set location to corner or centre.

Continuing, there is the designation of the {\tt cellsize}. The value of this should be set to the desired distance in the x or y direction between where the surface heights are evaluated. 

The final option is {\tt nodataval} which indicates missing data. This currently serves no purpose in the code, but has been left there for compatibility with pre-existing data files. Set it to what you like, but to ensure the file is read properly do set it to at least \textbf{\textit{something}}.

Now all that remains is to enter in the desired surface heights in the form of a simple table below this. The table should just list the values for each cell column by column and row by row; look at any of the ready-made \texttt{surface.dat} files for an example. As mentioned before, you should check to see if \textbf{SLOPE_EXTRAPOLATION} is defined in your makeflags file. If it is not, ensure that you specify the height for one cell either side of the area you are interested in simulating. It is important to note that the height is always specified for the centre of the cell, regardless of how location is defined.

For example, with \textbf{SLOPE_EXTRAPOLATION} undefined, if your intention is to simulate a flow over a rectangle of length 10 in the x direction and width 1 in the y direction, with a bump that plateaus at a height of 0.15 in the centre, the contents of your creatively named \texttt{surface\_bump.dat} file might look like this:

\begin{verbatim}
! Specifies a surface with a bump along the x-direction with a 
! plateau from x=4.5 and x=5.5
! Columns are the x-direction and rows the y-direction.
! Note that these are the values at the CENTREs of the cells.
! Will interpolate linearly.
location      corner
order          row
ncols         12
nrows         3
xllcorner     -1.
yllcorner     -1.
cellsize      1.
nodata_value  -9999
0. 0. 0. 0.05 0.1 0.15 0.15 0.1 0.05 0. 0. 0.
0. 0. 0. 0.05 0.1 0.15 0.15 0.1 0.05 0. 0. 0.
0. 0. 0. 0.05 0.1 0.15 0.15 0.1 0.05 0. 0. 0.
\end{verbatim}

As you can see, although there are only 10 columns and 1 rows that we are interested in, the surface must be defined over 12 columns and 3 rows so that there can be interpolation to find the height over the entire area of interest. 

If \textbf{SLOPE_EXTRAPOLATION} is used, then care must be taken to ensure that a slope is not extrapolated where one is not desired. I spent quite some time trying to figure out what was producing nonsense results in one of my simulations before I realised I had allowed a small slope to be extrapolated from my surface file. 
\end{document}