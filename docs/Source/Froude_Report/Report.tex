\documentclass[10pt,a4paper,notitlepage]{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage[strings]{underscore}
\usepackage{float}
\newcommand{\hydrate}{{\sc HydraTE}}
\graphicspath{{./img/}}
\author{Daniel Cutting}
\date{}
\title{\bfseries Measuring the performance of \hydrate \ for a uniform flow travelling over a small bump}
\begin{document}
\maketitle
For this test we wanted to investigate how \hydrate \ performed when simulating a frictionless flow of water down a uniform  channel where the height of the channel varies slowly over a bump. This is a basic fluid dynamics phenomenon which has a fairly well known theoretical solution so it is important that \hydrate \ can correctly predict the solution to this flow.

The behaviour of the the flow of water down the uniform channel can be theoretically predicted by using Bernoulli's equation and mass conservation and comparing these at the start of the flow before the bump and to the flow when travelling over the bump. This will give us the cubic 

\begin{equation}
(2 g h - U^2)(D+ h -h_0)^2 + U^2=0
\end{equation}
which predicts the height of the water from the initial depth and flow speed. In this equation $g$ is standard gravity, $h$ is the change in depth of the water, $h_0$ is the variation in the floor height, $D$ is the depth of the water before the bump and $U$ is the flow speed before the bump.

Equation (1) can easily be turned into the dimensionless equation
\begin{equation}
\left( \dfrac{2}{F} h^* -1 \right) (1 + h^*-h^*_0)^2+1=0 \text{,}
\end{equation}
where the Froude number is $F=U^2/gD$, the height $h=Dh^*$, and the floor height is $h_0=Dh^*_0$.

A short discussion of such a frictionless flow over a bump can be seen in chapter 10 of Fluid Mechanics by Frank White \cite{White}[p.~675-677].

For our flow we chose a floor that starts flat at $x=0$ and then increases to reach a plateau between $x=1.5$ and  $x=4.5$. It then decreases to its initial flat level between $x=5.5$ and $x = 8.5$. The channel has a width of 1 metre. We allow a body a still body of water with a flat surface to settle above this floor as depicted in Figure 1.
\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{Initial_Height}
\caption{A depiction of the initial flow state and the floor upon which it rests.}
\end{figure}


To create the initial flow we take this flat body of water and add a flow speed to it. We calculate this flow speed by taking the density of the first 100 particles in the x direction and measure their surface density, which can be equated to the depth of the water at each point. We then average this to find the depth at the start of the flow, $D$. We then decide the Froude number we are interested in observing the behaviour of and calculate the flow speed before the bump to be $U=(FgD)^{1/2}$.

We then vary the flow speed over the bump so that the velocity, $v$, at each point is inversely proportional to the depth according to $v=U/d$, where $d$ is the depth at that point. This is to ensure we have a constant mass flow rate throughout the channel.

It is expected that, although we choose an input Froude number to investigate, once the flow settles into a steady state the end state Froude number will have changed to some degree with the height of the flow so that it satisfies equation (2).

For this reason we use a number of input Froude numbers to choose our initial flow speed to ensure we can examine the behaviour of a spread of subcritical and supercritical Froude number flows. Ideally we wish to examine the flows behaviour as the end state Froude number approaches the critical point.

Included in the appendix A is a selection of water height maps of the final state of each flow discussed to show that variation in height is seen predominantly in the x direction rather than y direction, as we expect for our channel which is uniform in the y direction.

For the first run we use an input Froude number $F=0.2$ to determine $U$. This results in a velocity evolution that is shown in Figure 2. The surface density of particles or water surface height is shown in Figure 3. 

In Figure 4 we see that the final state closely matches with the theoretical model predicted by equation (2). We have run the simulation in this run for longer than those that follow so that the flow might settle even further.  The Froude number the flow settles to is listed in the legend of Figure 4, which will be where the equivalent end state Froude number will be listed in further flow comparisons. As we suspect for a subcritical Froude number there is a significant dip over the bump in the channel floor where the velocity of our flow increases.

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{Vel_run_1}
\centering
\caption{The velocity evolution of run 1 which used input Froude of $F=0.2$.}
\end{figure}


\begin{figure}[H]
\begin{center}
\includegraphics[width=\textwidth]{Dens_run_1}
\caption{The surface density evolution of run 1 which used input Froude of $F=0.2$.}
\end{center}
\end{figure}


\begin{figure}[H]
\begin{center}
\includegraphics[width=\textwidth]{Froude_run_1}
\caption{Comparison between end of simulation water surface height and theoretical prediction. From run 1 which used an input Froude of $F=0.2$.}
\end{center}
\end{figure}


For the second run we use an input Froude of $F=0.4$ to determine $U$. The velocity evolution is similar to that of the first run,  and is shown in Figure 5. In Figure 6 we can see that the model predicted by the cubic in equation (2) matches our final state in our second run quite well.



\begin{figure}[H]
\begin{center}
\includegraphics[width=\textwidth]{Vel_run_2}
\caption{The velocity evolution of run 2 which used an input Froude $F=0.4$.}
\end{center}
\end{figure}


\begin{figure}[H]
\begin{center}
\includegraphics[width=\textwidth]{Froude_run_2}
\caption{Comparison between end of simulation water surface height and theoretical prediction. From run 2 which used an input Froude of $F=0.4$.}
\end{center}
\end{figure}

After this run we discovered what is thought to be some problem in the \hydrate \ code. For the next 8 runs we kept increasing the input Froude number by 0.2 per run but as it increased it was seen that momentum stopped being conserved. For run 3 we see that by looking at its velocity evolution, there is a decrease in the average velocity. As all particles are the same and this is a closed, looping system, with no Manning coefficient, this is clearly a violation of a law of physics. This is shown in figure 7. 

\begin{figure}[H]
\begin{center}
\includegraphics[width=\textwidth]{Vel_run_3}
\caption{The velocity evolution of run 3. As can be seen there is an overall decrease in velocity over time which shows momentum is not being conserved.}
\end{center}
\end{figure}

The problem gets continuously worse. By run 6 we are seeing a decrease in average velocity of $1.5$ ms$^{-1}$. We also see large shock waves, though this may be unconnected to the problem as \hydrate \ has handled larger shocks without violating conservation of momentum before. The velocity evolution is seen in Figure 8. By run 9 we see a decrease in average velocity of nearly $2.3$ ms$^{-1}$. This velocity evolution can be seen in Figure 9. 

This presents a clear problem when trying to examine the behaviour of the end state Froude numbers at different states, for despite imparting different velocities to our still body of water in each run we see the flow settles towards a state with a Froude number around $F=0.3$. This is demonstrated by comparing the final states of run 3, 6, and 9 in Figure 10.

\begin{figure}[H]
\begin{center}
\includegraphics[width=\textwidth]{Vel_run_6}
\caption{The velocity evolution of run 6. As can be seen there is an overall decrease in velocity over time which shows momentum is not being conserved. There are some small shocks.}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}
\includegraphics[width=\textwidth]{Vel_run_9}
\caption{The velocity evolution of run 9. As can be seen there is an overall decrease in velocity over time which shows momentum is not being conserved. There is some quite chaotic behaviour that settles eventually.}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}
\includegraphics[width=\textwidth]{Froude_run_3}
\\
\includegraphics[width=\textwidth]{Froude_run_6}
\\
\includegraphics[width=\textwidth]{Froude_run_9}
\caption{The state at the end of each simulation compared with the theoretical prediction. As can be seen all runs seem to be settling towards similar Froude numbers.}
\end{center}
\end{figure}

For the final run we determine used an input Froude number of $F =2.0$. Curiously in this run momentum appears to be conserved as normal and we see a much more normal velocity evolution in Figure 11. 

As this is a flow with a supercritical end state Froude number we see a rise in the flow height as it travels over the bump in the channel, and the flows velocity drops in relation to this. We show the water surface height evolution in Figure 12.



\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{Vel_run_10}
\centering
\caption{The velocity evolution of run 10 which used an input Froude of $F=2.0$.}
\end{figure}

\begin{figure}[H]
\begin{center}
\includegraphics[width=\textwidth]{Dens_run_10}
\caption{The surface density evolution of run 10 which used an input Froude of $F=2.0$.}
\end{center}
\end{figure}

In this flow we have a very close match between the simulation and the predicted flow height from equation (2). This can be observed in Figure 13.

\begin{figure}[H]
\begin{center}
\includegraphics[width=\textwidth]{Froude_run_10}
\caption{Comparison between end of simulation water surface height and theoretical prediction. From run 10 which used an input Froude of $F=2.0$.}
\end{center}
\end{figure}

This raises some interesting questions as the only difference between this flow and the that of run 9 is that there was a slightly higher flow speed throughout the run, which arose from an increase of just 0.2 to the input Froude number.

There is clearly some error in the code here leading to this behaviour. While it would appear that \hydrate \ cannot handle this fairly simple flow experiment, the fact that it can handle and predict good matches to the flows in run 1, 2, and 10 implies that it should be able to be adapted to correctly predict all the flows. 


To begin the investigation into what is causing this loss of momentum it is recommended to examine where the force violating the momentum conservation is coming from. The only place this should be able to be is from the floor, as there is an exact pair wise particle conservation.

If there is no violation from the floor itself there is the possibility that there is an issue relating to the time-stepping of the simulation. If there is too large a time step then this could lead to some imbalance of forces on the up and down slopes of the floor. 

Sadly as a result of this error it was not possible to examine the behaviour of the fluid as it approached a critical Froude number. It would be interesting to return to this once the code has been adapted so there is momentum conservation in this scenario.

\appendix
\newpage
\section{\\Water Height Maps} \label{App:Appendix}

\begin{figure}[H]
\begin{center}
\includegraphics[width=\textwidth]{D_Map_Run_1}
\caption{A map of the water surface height of the 1$^{st}$ run. Includes the height of the floor in the measurements. Map of time $t=200$ seconds}
\end{center}
\end{figure}


\begin{figure}[H]
\begin{center}
\includegraphics[width=\textwidth]{D_Map_Run_2}
\caption{A map of the water surface height of the 2$^{nd}$ run. Includes the height of the floor in the measurements. Map of time $t=100$ seconds}
\end{center}
\end{figure}


\begin{figure}[H]
\begin{center}
\includegraphics[width=\textwidth]{D_Map_Run_3}
\caption{A map of the water surface height of the 3$^{rd}$ run. Includes the height of the floor in the measurements. Map of time $t=100$ seconds}
\end{center}
\end{figure}


\begin{figure}[H]
\begin{center}
\includegraphics[width=\textwidth]{D_Map_Run_6}
\caption{A map of the water surface height of the 6$^{th}$ run. Includes the height of the floor in the measurements. Map of time $t=100$ seconds}
\end{center}
\end{figure}


\begin{figure}[H]
\begin{center}
\includegraphics[width=\textwidth]{D_Map_Run_9}
\caption{A map of the water surface height of the 9$^{th}$ run. Includes the height of the floor in the measurements. Map of time $t=100$ seconds}
\end{center}
\end{figure}


\begin{figure}[H]
\begin{center}
\includegraphics[width=\textwidth]{D_Map_Run_10}
\caption{A map of the water surface height of the 10$^{th}$ run. Includes the height of the floor in the measurements. Map of time $t=100$ seconds}
\end{center}
\end{figure}



\begin{thebibliography}{9}
\bibitem{White}
Frank White,
\emph{Fluid Mechanics},
WCB Mc Graw Hill, 4th Edition, 
University of Rhode Island,
1998
\end{thebibliography}


\end{document} 