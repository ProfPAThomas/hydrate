program test

  integer, parameter :: nbin=1000
  integer :: i
  real, parameter :: pi=3.141592654
  real :: dr, r, total
  real :: kernel

  dr=1./float(nbin)
  print*,'dr=',dr
  total=0.
  do i=1,nbin
     r=(i-0.5)*dr
     total=total+kernel(r)*r
  end do
  print*,'total=',total*2.*pi*dr

end program test
