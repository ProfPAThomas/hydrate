subroutine sph

! Calculates hydrodynamic forces and energy changes using SPH

! Loops over range of search cell sizes.
! If we use a fixed search length, h, it makes little difference whether
! we search with large or small cells first.
! Using EXACT_NSPH requires that we work from small scales upwards,
! or else that we add a small padding to the search length.
! Sorting into cells is O(N) so we can allow Lsph to (in|de)crease
! slowly (this is different from gravity calculation).
! Knowing the distribution of h values, it must be possible to estimate 
! the work for different Lsph.
! However, it seems simpler just to take a search length that gradually
! increases with each iteration.
! The use of refinements (not implemented) would allow one to get away with
! a much lower value of Lsph_max.

! Lsph_max may be specified in run_parameters.dat.
! Otherwise the code will choose a suitable value,
! subject to memory limitations.

  use itype_definitions
  use particle_data, only: itype, h, r
  use run_parameters, only: nobj, pos_min, soft, verbosity
#ifdef ISOLATED
  use run_parameters, only: pos_max, pos_size
#else
  use run_parameters, only: pos_factor, Lpos_shape
#endif
  use sph_parameters
  use unit_numbers

  ! Rate at which search-cell changes in size
  real, parameter :: search_cell_size_growth_factor=1.5
  real :: search_cell_size   ! size of search cell in grid units
#ifdef ISOLATED
  real :: search_cell_number(2) ! (non-integral) number of search cells
#endif

  integer :: ip
  integer :: Lsph_old(2)             ! used to check that Lsph has changed
  integer :: Ngas_unprocessed        ! number of unprocessed particles
  real :: h_process                  ! sph length that needs to be processed
  real :: cpu_time_sph_now, cpu_time_sph_last
#ifdef CPU_TIME_SPH
#ifdef EXACT_NSPH
  real :: cpu_time_sph_exact
#endif
#endif

  if (verbosity.gt.1) write(lun_log,*) 'Starting SPH calculation'
  
  ! Set flag not to force SPH evaluation
  flag_force_sph_evaluation=.false.

  ! Allocate linked list for sph neighbour search
  allocate(ll_sph(Nbary))

#ifdef CPU_TIME_SPH
#ifdef EXACT_NSPH
  cpu_time_sph_exact=0.
#endif
#endif

#ifdef COUNT_SPH_NEIGHBOURS
  ! Initialise diagnostic counters
  Nneigh_inf=huge(1)
  Nneigh_sup=0
  Nneigh_sum=0
  Nneigh_num=0
#endif

  ! Extent of particles
#ifdef ISOLATED
  pos_min=minval(r,2,spread(itype,1,2).gt.0)
  pos_max=maxval(r,2,spread(itype,1,2).gt.0)
  pos_size=pos_max-pos_min
#endif

  ! Find minimum value of h
  h_min=minval(h,itype.eq.itype_gas)
#ifdef EXACT_NSPH
  ! Have to search out a little further when using EXACT_NSPH
  h_min=h_min*exact_nsph_search_factor
#endif
  ! h is not allowed to shrink below a certain fraction of the softening
  h_min=max(h_min,h_min_over_soft*soft)
  if (verbosity.gt.1) write(lun_log,*) 'h_min=',h_min

  ! Minimum possible search length
  ! Note that size gets mutliplied by growth_factor before use
  search_cell_size=h_min/float(Lsph_search)/search_cell_size_growth_factor

  ! Start iteration over search lengths
  Lsph_old=0
  Ngas_unprocessed=count(itype.eq.itype_gas)
  do while (Ngas_unprocessed.gt.0)

     if (verbosity.ge.3) then
        call cpu_time(cpu_time_sph_last)
        write(lun_log,*) 'Ngas_unprocessed=',Ngas_unprocessed
     end if

     ! Set number and allocate sph-search-cells
     call sph_allocate_cells

     ! Sort particles into sph-search-cells
     call sph_sort

     ! Calculate the sph forces
     ! Do all particles with smoothing lengths that fit in search box
     h_process=search_cell_size*Lsph_search
     call sph_force(h_process)

     if (verbosity.ge.3) then
        call cpu_time(cpu_time_sph_now)
        write(lun_log,*) 'Lsph, Nprocessed, cputime = '&
          &,Lsph,Ngas_unprocessed-count(itype.eq.itype_gas)&
          &,cpu_time_sph_now-cpu_time_sph_last
#ifdef CPU_TIME_SPH
        write(lun_log,*) 'cputime, neighbour search = ',cpu_time_sph_0
        write(lun_log,*) 'cputime, first sph loop   = ',cpu_time_sph_1
        write(lun_log,*) 'cputime, second sph loop  = ',cpu_time_sph_2
#ifdef EXACT_NSPH
        write(lun_log,*) 'cputime, exact_nsph sorts = ',cpu_time_sph_exact
#endif
#endif
     end if

     ! Deallocate linked-list head-of-chain array
     deallocate(ihc_sph)

     ! Loop exits if this count drops to zero
     Ngas_unprocessed=count(itype.eq.itype_gas)
     
     ! End loop over cell-sizes
  end do

  ! Check that all gas particles were processed
  if (count(itype.eq.itype_gas).gt.0) then
     write(lun_err,*) 'sph: particle not processed'
     STOP
  end if

#ifdef COUNT_SPH_NEIGHBOURS
  ! Output diagnostic counters
  write(lun_log,*) 'Nneigh_min=',Nneigh_inf
  write(lun_log,*) 'Nneigh_max=',Nneigh_sup
  write(lun_log,*) 'Nneigh_mean=',Nneigh_sum/Nneigh_num
#endif

  ! Reset itypes
  where (itype.eq.itype_done) itype=itype_gas

  ! Dellocate linked list for sph neighbour search
  deallocate(ll_sph)

contains

!-----------------------------------------------------------------

subroutine sph_allocate_cells

! Chooses and allocates search cells

  integer :: ier                        ! local variables
#ifdef DEBUG
  integer :: i
#endif

  ! This loop is to trap memory overflows when allocating ihc
  ier=1
  do while (ier.ne.0)
     search_cell_size=search_cell_size*search_cell_size_growth_factor
#ifdef ISOLATED
     ! There are two possible approaches to the definition of Lsph:
     ! (i) Lsph to extend only over the particle distribution, with an
     !     extra padding layer of thickness Lsph_search.
     ! (ii) Lsph to include the padding layer so that the particles
     !     extend only over Lsph-Lsph_search.
     ! The latter means that sph_force is the same for periodic and isolated
     ! boxes and so is the one that we adopt here.
     ! So Lsph=ceiling(search_cell_number)+Lsph_search
     search_cell_number=pos_size/search_cell_size
     if (verbosity .ge. 2) write(lun_log,*) 'search_cell_number=',search_cell_number 
     ! If Lsph_max is set then no direction may exceed this cell number,
     ! including the padding region.
     if (Lsph_max.gt.0) search_cell_size=search_cell_size&
          &*max(1.,maxval(search_cell_number/real(Lsph_max-Lsph_search)))
     search_cell_number=pos_size/search_cell_size
     Lsph=ceiling(search_cell_number-epsilon(1.))+Lsph_search
     if (verbosity .ge. 2) write(lun_log,*) 'Lsph=',Lsph
#else
     Lsph_factor=int(pos_factor/search_cell_size)
     ! If Lsph_max is set then obey it
     if (Lsph_max.gt.0) Lsph_factor=min(Lsph_factor&
          &,minval(Lsph_max/Lpos_shape))
     Lsph=Lsph_factor*Lpos_shape
     ! Need at least 2*Lsph_search+1 cells in each direction.
     ! This differs from the isolated case in that padding cells 
     ! are distinct at each edge of the box.
     do while (minval(Lsph)<2*Lsph_search+1)
        Lsph_factor=Lsph_factor+1
        Lsph=Lsph_factor*Lpos_shape
     end do
     search_cell_size=pos_factor/real(Lsph_factor)
#endif
     ! check that Lsph != Lsph_old
     if (all(Lsph.eq.Lsph_old)) then
        if (flag_force_sph_evaluation) then
#ifdef DEBUG
           write(lun_log,*) 'not evaluated: '
           do i=1,size(itype)
              if (itype(i).ne.itype_done) write(lun_log,*) '  ',i
           end do
#endif
           write(lun_err,*) 'sph: not all particles evaluated'
           write(lun_log,*) 'id, itype ='
           do ip=1, nobj
              write(lun_log,*) ip,itype(ip)
           end do
           STOP
        else
           write(lun_err,*) 'sph: Lsph=Lsph_old: forcing sph evaluation'
           flag_force_sph_evaluation=.true.
        end if
     end if
     Lsph_old=Lsph
     ! Allocate linked-list array
#ifdef ISOLATED
     allocate(ihc_sph(Lsph(1)+Lsph_search&
                    &,Lsph(2)+Lsph_search),stat=ier)
#else
     allocate(ihc_sph(Lsph(1),Lsph(2)),stat=ier)
#endif
     if (ier.gt.0) write(lun_err,*) &
          &'sph: ihc_sph allocation error using Lsph = ',Lsph
  end do

  Lsph_wrap_min=1-Lsph_search
  Lsph_wrap_max=maxval(Lsph)+Lsph_search
  
end subroutine sph_allocate_cells
   
!-----------------------------------------------------------------

subroutine sph_sort

! Create a linked list of particles in search cells.
! Include all sph particles whether processed or not.

  integer :: iobj,ix,iy
  real :: rscale

  rscale=1./search_cell_size
  ! To prevent rounding error pushing particles over boundary:
  ! The following sets rscale to the next lowest representable number:
  !rscale=nearest(rscale,-1.)
  ! If the above is not enough then try the following:
  rscale=rscale*(1-epsilon(1.))

  ihc_sph=0
  ll_sph=0
  do iobj=1,Nbary
     if (itype(iobj).gt.0) then
        ix=1+floor((r(1,iobj)-pos_min(1))*rscale)
        iy=1+floor((r(2,iobj)-pos_min(2))*rscale)
#ifdef DEBUG_2
        if ( ix.lt.1 .or. ix.gt.Lsph(1) .or.&
            &iy.lt.1 .or. iy.gt.Lsph(2) ) then
           write(lun_log,*) 'Lsph=',Lsph
           write(lun_log,*) 'ix,iy',ix,iy
           write(lun_log,*) 'iobj,r=',iobj,r(:,iobj)
        end if
#endif
        ll_sph(iobj)=ihc_sph(ix,iy)
        ihc_sph(ix,iy)=iobj
     end if
  end do

end subroutine sph_sort

!-----------------------------------------------------------------

end subroutine sph
