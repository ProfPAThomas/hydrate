program temp

! Done to test timing of divide cf multiply

call cpu_time(cpu_time_start)

a=1.d0
b=1.001d0
do i=1,10000
   a=a*b
end do

call cpu_time(cpu_time_end)

print*,'Multiply time is ',cpu_time_end-cpu_time_start

end program temp
