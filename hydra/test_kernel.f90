program test_kernel

  integer, parameter :: nbin=3
  real, parameter :: pi=3.141592654

  integer :: i
  real :: dx, x
  real :: int=0., intr2=0., intr4=0.

  dx=1./float(nbin)
  do i=1,nbin
     x=(i-0.5)*dx
     int=int+kernel(x)*x
     intr2=intr2+kernel(x)*x**3
     intr4=intr4+kernel(x)*x**5
  end do
  int=int*2.*pi*dx
  intr2=intr2*2.*pi*dx
  intr4=intr4*2.*pi*dx
  print*,'int=',int
  print*,'intr2=',intr2
  print*,'intr4=',intr4

contains

real function kernel(x)

! Smoothing kernel in 2-d (only normalisation changes from 3-d)

  real, intent(in) :: x

  real, parameter :: pi=3.141592654, wnorm=40./(7.*pi) !wnorm=8./pi

  if (x.lt.0.0) then
     STOP 'kernel: invalid range for argument'
  else if (x.le.0.5) then
     kernel=wnorm*(1.-6.*x**2+6.*x**3)
  else if (x.le.1.0) then
     kernel=wnorm*2.*(1.-x)**3
  else
     STOP 'kernel: invalid range for argument'
  end if

end function kernel

end program test_kernel
