program test_kernel_sum

  integer, parameter :: nbin=1000
  real, parameter :: pi=3.141592654

  ! Smoothing kernel & derivative
  integer, parameter :: Nkernel=30000
  real :: wtab(0:Nkernel+1)=-1.
  real :: dr2_to_rtab    ! converts separation to location in kernel table
  integer :: itab        ! location in table
  real :: rtab, ftab     ! location in table  rtab=itab+ftab, 0<=ftab<1

  integer :: i
  real :: dx, x, x2
  real :: int=0., intr2=0., intr4=0.

  if (wtab(0).lt.0.) CALL create_kernel(Nkernel,wtab)
  dr2_to_rtab=float(Nkernel)

  dx=1./float(nbin)
  do i=1,nbin
     x=(i-0.5)*dx
     x2=x**2
     rtab=dr2_to_rtab*x2
     itab=floor(rtab); ftab=rtab-itab
     w_n=(wtab(itab)+ftab*(wtab(itab+1)-wtab(itab)))
     int=int+w_n*x
     intr2=intr2+w_n*x*x2
     intr4=intr4+w_n*x*x2**2
  end do
  int=int*2.*pi*dx
  intr2=intr2*2.*pi*dx
  intr4=intr4*2.*pi*dx
  print*,'int=',int
  print*,'intr2=',intr2
  print*,'intr4=',intr4

contains

!--------------------------------------------------------------

subroutine create_kernel(ntab,wtab)

! Creates lookup tables for the smoothing kernel

  integer, intent(in) :: ntab
  real, intent(out) :: wtab(0:ntab+1)

  ! Local variables
  integer :: itab
  real :: rad, rad2

  ! Function declarations
  !real, external :: kernel

  wtab(0)=kernel(0.)
  do itab=1,ntab
     rad2=float(itab)/float(ntab)
     rad=sqrt(rad2)
     wtab(itab)=kernel(rad)
  enddo
  wtab(ntab+1)=0.

end subroutine create_kernel

!--------------------------------------------------------------

real function kernel(x)

! Smoothing kernel in 2-d (only normalisation changes from 3-d)

  real, intent(in) :: x

  real, parameter :: pi=3.141592654, wnorm=40./(7.*pi) !wnorm=8./pi

  if (x.lt.0.0) then
     STOP 'kernel: invalid range for argument'
  else if (x.le.0.5) then
     kernel=wnorm*(1.-6.*x**2+6.*x**3)
  else if (x.le.1.0) then
     kernel=wnorm*2.*(1.-x)**3
  else
     STOP 'kernel: invalid range for argument'
  end if

end function kernel

!--------------------------------------------------------------

end program test_kernel_sum
