subroutine create_wbtab(ntab,wbtab)

! Creates lookup table for the pairwise repuslive force used to
! prevent interpenetration.

  integer, intent(in) :: ntab
  real, intent(out) :: wbtab(0:ntab+1)

  ! Local variables
  integer :: itab
  real :: rad, rad2

  ! Function declarations
  real, external :: bkernel

  wbtab(0)=bkernel(0.)
  do itab=1,ntab
     rad2=float(itab)/float(ntab)
     rad=sqrt(rad2)
     wbtab(itab)=bkernel(rad)
  enddo
  wbtab(ntab+1)=0.

end subroutine create_wbtab

!--------------------------------------------------------------
 
real function bkernel(x)

! Function describing repuslive force.
! Zero outside x=xmax
! Normalised to unity at x=xmin
! Set equal to huge(1.) within x=xmin as a flag
! Formula is ((xmax-x)/(xmax-xmin))^nbounce

  real, intent(in) :: x

  real, parameter :: bnorm=0.
  real, parameter :: xmin=0.5
  real, parameter :: xmax=0.85
  real, parameter :: nbounce=4

  if (x.lt.0.) then
     STOP 'kernel: invalid range for argument'
  else if (x.le.xmin) then
     bkernel=huge(1.)
  else if (x.le.xmax) then
     bkernel=((xmax-x)/(xmax-xmin))**nbounce
  else
     bkernel=0.
  end if

end function bkernel

