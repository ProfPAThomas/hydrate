# Top-level hydra makefile
# Note: it may first be necessary to alter the compiler names and flags
# in makeflags.

# Make library first
all: lib_ hydra_ init_

# This is a pretty ruthless clean: removes all data
bleach: hydra_clean idl_clean init_clean lib_clean rundir_clean

# Removes object files but leaves rundir intact
clean: hydra_clean idl_clean init_clean lib_clean

# If compilation options have changed
new_options: hydra_new init_new

# For new install
test:
	make bleach
	cp makeflags_test makeflags
	make all

hydra_:
	cd hydra; make hydra

hydra_clean:
	cd hydra; make clean

hydra_new:
	cd hydra; make new_options

idl_clean:
	cd idl; make clean

init_:
	cd init; make

init_clean:
	cd init; make clean

init_new:
	cd init; make new_options

lib_:
	cd lib; make all

lib_clean:
	cd lib; make clean

.PHONY: rundir_clean
rundir_clean:
	/bin/rm -rf rundir
	cp -r rundir_clean rundir
