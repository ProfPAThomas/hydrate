clear all;
close all;
clc;

dblflag=1;
nfiles=300;
nskip=1;
topview=1;
drepw=0.25;
zwall=0.03;
theta=26*pi/180.0;
aw=0.4;
bw=0.8;
dw=0.8;

% Read particle data
filelist=dir('/Users/cs266/My_SPH_water_2D/hydra95_water_2d/rundir/data/d0021.**data');

framenr=0;
figure;
for fnr=1:nskip:nfiles
    file=['/Users/cs266/My_SPH_water_2D/hydra95_water_2d/rundir/data/',filelist(fnr).name];
    
    % Read this in from header file!
    nobj=2048;
    
    fid=fopen(file,'rb');
    
    % Particle types
    dummy=fread(fid,1,'int32','ieee-be');
    itype=fread(fid,nobj,'int32','ieee-be');
    dummy=fread(fid,1,'int32','ieee-be');
    
    % Masses
    dummy=fread(fid,1,'int32','ieee-be');
    if dblflag
        mass=fread(fid,nobj,'float64','ieee-be');
    else
        mass=fread(fid,nobj,'float32','ieee-be');
    end
    dummy=fread(fid,1,'int32','ieee-be');
    
    % Positions
    dummy=fread(fid,1,'int32','ieee-be');
    if dblflag
        pos=fread(fid,[2,nobj],'float64','ieee-be');
    else
        pos=fread(fid,[2,nobj],'float32','ieee-be');
    end
    dummy=fread(fid,1,'int32','ieee-be');
    
    x=pos(1,:);
    y=pos(2,:);
    
    % Velocities
    dummy=fread(fid,1,'int32','ieee-be');
    if dblflag
        vel=fread(fid,[2,nobj],'float64','ieee-be');
    else
        vel=fread(fid,[2,nobj],'float32','ieee-be');
    end
    dummy=fread(fid,1,'int32','ieee-be');
    
    % Smoothing lengths
    dummy=fread(fid,1,'int32','ieee-be');
    if dblflag
        hsml=fread(fid,nobj,'float64','ieee-be');
    else
        hsml=fread(fid,nobj,'float32','ieee-be');
    end
    dummy=fread(fid,1,'int32','ieee-be');
    
    % Density
    dummy=fread(fid,1,'int32','ieee-be');
    if dblflag
        dn=fread(fid,nobj,'float64','ieee-be');
    else
        dn=fread(fid,nobj,'float32','ieee-be');
    end
    dummy=fread(fid,1,'int32','ieee-be');
    
    fclose(fid);
    
    % Plot data
    if topview
        %plot3(x,y,dn,'b.','MarkerSize',20);
        data=[x.' y.' dn];
        plot3k(data,'ColorRange',[0.0 0.5],'Marker',{'o',4});
        % Plot walls
        hold on;
        plot([0 0],[0 3.6],'r-');
        plot([97.2 97.2],[0 3.6],'r-');
        plot([0 97.2],[0 0],'r-');
        plot([0 97.2],[3.6 3.6],'r-');
        % Plot obstacle
        plot([10.99 10.99+aw*cos(theta)],[1.75 1.75-aw*sin(theta)],'r-');
        plot([10.99+aw*cos(theta) 10.99+aw*cos(theta)+bw*sin(theta)],[1.75-aw*sin(theta) 1.75-aw*sin(theta)+bw*cos(theta)],'r-');
        plot([10.99+aw*cos(theta)+bw*sin(theta) 10.99+bw*sin(theta)],[1.75-aw*sin(theta)+bw*cos(theta) 1.75+bw*cos(theta)],'r-');
        plot([10.99+bw*sin(theta) 10.99],[1.75+bw*cos(theta) 1.75],'r-');
        % Plot dam 
        plot([7.2 7.2],[2.3 3.6],'r-');
        plot([7.2+dw 7.2+dw],[2.3 3.6],'r-');
        plot([7.2 7.2+dw],[2.3 2.3],'r-');
        plot([7.2 7.2],[0 1.3],'r-');
        plot([7.2+dw 7.2+dw],[0 1.3],'r-');
        plot([7.2 7.2+dw],[1.3 1.3],'r-');
        hold off;
        grid off;
        axis equal;
        %axis([-1 101 -1 5 0 0.5]);
        axis([0 20 0 3.6 0 0.5]);
        view(0,90);
    else
        %plot3(x,y,dn,'b.','MarkerSize',20);
        data=[x.' y.' dn];
        plot3k(data,'ColorRange',[0.0 0.5]);
        grid on;
        %axis([-1 101 -1 5 0 0.5]);
        axis([-1 98.2 0 3.6 0 0.5]);
        view(0,0);
        % Plot walls
        hold on;
        plot3([0 0],[0 0],[0 0.5],'r-');
        plot3([97.2 97.2],[0 0],[0 0.5],'r-');
        plot3([7.2 7.2],[0 0],[0 0.5],'r-');
        plot3([7.2+dw 7.2+dw],[0 0],[0 0.5],'r-');
%         plot3([40-drepw 40-drepw],[0 0],[0 zwall],'r-');
%         plot3([60+drepw 60+drepw],[0 0],[0 zwall],'r-');
%         plot3([40-drepw 60+drepw],[0 0],[zwall zwall],'r-');
        hold off;
    end
    
    framenr=framenr+1;
    M(:,framenr)=getframe(gcf); 
    clf;
end

%show movie
SHOW_MOVIE = 1;
while SHOW_MOVIE == 1
    display(' ')
    SHOW_MOVIE = input('Show movie (again) [0 = no; 1 = yes]?:  ');
    if SHOW_MOVIE == 1
        FPSEC = input('Specify how many "frames per second" [integer]:  ');
        movie(M,1,FPSEC)
    end
end

%save movie
SAVE_MOVIE = 1;
while SAVE_MOVIE == 1
    display(' ')
    SAVE_MOVIE = input('Save movie (again) as avi file [0 = no; 1 = yes]?:  ');
    if SAVE_MOVIE == 1
        display(' ')
        FPSEC = input('Specify how many "frames per second" [integer]:  ');
        MNAME = input('Specify movie name between single quotes:  ');
        moviename = [MNAME '.avi'];
        movie2avi(M,moviename,'compression','none','fps',FPSEC,'quality',100);
    end
end
