clear all;
close all;
clc;

dblflag=1;
nfiles=700;
nskip=10;

% Read in DEM
file='/Users/cs266/My_SPH_water_2D/hydra95_water_2d/rundir/data/fulldem.asc';

fid=fopen(file);
header=textscan(fid,'%s %f',6);
ncols=header{2}(1);
nrows=header{2}(2);
xmin=header{2}(3);
ymin=header{2}(4);
cellsize=header{2}(5);
dem=fscanf(fid,'%f',[ncols nrows]);
dem=dem';
fclose(fid);

% Grid for plotting DEM
xmax=xmin+(ncols-1)*cellsize;
xdem=[xmin:cellsize:xmax];
ymax=ymin+(nrows-1)*cellsize;
ydem=[ymin:cellsize:ymax];
[Xdem,Ydem]=meshgrid(xdem,ydem);

% 1D DEM
dem1d=dem(1,:);

% Read particle data

filelist=dir('/Users/cs266/My_SPH_water_2D/hydra95_water_2d/data/run8/d0008.**data');

framenr=0;
figure;
for fnr=1:nskip:nfiles
    file=['/Users/cs266/My_SPH_water_2D/hydra95_water_2d/data/run8/',filelist(fnr).name];
    
    % Read this in from header file!
    %nobj=3500;
    %nobj=1400;
    nobj=700;
    %nobj=351;
    %nobj=176;
    
    fid=fopen(file,'rb');
    
    % Particle types
    dummy=fread(fid,1,'int32','ieee-be');
    itype=fread(fid,nobj,'int32','ieee-be');
    dummy=fread(fid,1,'int32','ieee-be');
    
    % Masses
    dummy=fread(fid,1,'int32','ieee-be');
    if dblflag
        mass=fread(fid,nobj,'float64','ieee-be');
    else
        mass=fread(fid,nobj,'float32','ieee-be');
    end
    dummy=fread(fid,1,'int32','ieee-be');
    
    % Positions
    dummy=fread(fid,1,'int32','ieee-be');
    if dblflag
        pos=fread(fid,[2,nobj],'float64','ieee-be');
    else
        pos=fread(fid,[2,nobj],'float32','ieee-be');
    end
    dummy=fread(fid,1,'int32','ieee-be');
    
    x=pos(1,:);
    y=pos(2,:);
    
    % Velocities
    dummy=fread(fid,1,'int32','ieee-be');
    if dblflag
        vel=fread(fid,[2,nobj],'float64','ieee-be');
    else
        vel=fread(fid,[2,nobj],'float32','ieee-be');
    end
    dummy=fread(fid,1,'int32','ieee-be');
    
    % Smoothing lengths
    dummy=fread(fid,1,'int32','ieee-be');
    if dblflag
        hsml=fread(fid,nobj,'float64','ieee-be');
    else
        hsml=fread(fid,nobj,'float32','ieee-be');
    end
    dummy=fread(fid,1,'int32','ieee-be');
    
    % Density
    dummy=fread(fid,1,'int32','ieee-be');
    if dblflag
        dn=fread(fid,nobj,'float64','ieee-be');
    else
        dn=fread(fid,nobj,'float32','ieee-be');
    end
    dummy=fread(fid,1,'int32','ieee-be');
    
    fclose(fid);
    
    % Adjust depths to allow for DEM
    dem1dip=interp1(xdem,dem1d,x).';
    dn=dn+dem1dip;
    
    % Plot data
    hsurf=surf(Xdem,Ydem,dem);
    set(hsurf,'FaceColor',[0 0 0],'FaceAlpha',0.5);
    set(hsurf,'EdgeColor',[0 0 0],'EdgeAlpha',1.0);
    %view(-13,52);
    %view(0,0);
    hold on;
    %plot3(x,y,dn,'b.','MarkerSize',20);
    data=[x.' y.' dn];
    plot3k(data,'ColorRange',[9.5 11]);
    hold off;
    grid on;
    axis([-100 300 0 20 9.5 11.2]);
    view(0,0)
    
    framenr=framenr+1;
    M(:,framenr)=getframe(gcf); 
    clf;
end

%show movie
SHOW_MOVIE = 1;
while SHOW_MOVIE == 1
    display(' ')
    SHOW_MOVIE = input('Show movie (again) [0 = no; 1 = yes]?:  ');
    if SHOW_MOVIE == 1
        FPSEC = input('Specify how many "frames per second" [integer]:  ');
        movie(M,1,FPSEC)
    end
end

%save movie
SAVE_MOVIE = 1;
while SAVE_MOVIE == 1
    display(' ')
    SAVE_MOVIE = input('Save movie (again) as avi file [0 = no; 1 = yes]?:  ');
    if SAVE_MOVIE == 1
        display(' ')
        FPSEC = input('Specify how many "frames per second" [integer]:  ');
        MNAME = input('Specify movie name between single quotes:  ');
        moviename = [MNAME '.avi'];
        movie2avi(M,moviename,'compression','none','fps',FPSEC,'quality',100);
    end
end
