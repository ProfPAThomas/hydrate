subroutine heap_index(n,arrin,indx)

! Makes an index in ascending order of the elements in arrin
! using the heapsort algorithm
  integer, intent(in) :: n
  real, intent(in) :: arrin(n)
  integer, intent(out) :: indx(n)

  integer :: i, j, l, ir, indxt
  real :: q

  ! Initialise index array
  do j=1,n
     indx(j)=j
  end do
  l=n/2+1
  ir=n

  do while (.true.)
     if (l.gt.1) then
        l=l-1
        indxt=indx(l)
        q=arrin(indxt)
     else
        indxt=indx(ir)
        q=arrin(indxt)
        indx(ir)=indx(1)
        ir=ir-1
        if (ir.eq.1) then
           indx(1)=indxt
           return
        end if
     end if
     i=l
     j=l+l
     do while (j.le.ir)
        if (j.lt.ir) then
           if (arrin(indx(j)).lt.arrin(indx(j+1)))j=j+1
        end if
        if (q.lt.arrin(indx(j))) then
           indx(i)=indx(j)
           i=j
           j=j+j
        else
           j=ir+1
        endif
     end do
     indx(i)=indxt
  end do

end subroutine heap_index
