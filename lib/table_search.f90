integer function table_search(xarr,x)

! Finds position of entry in ordered table.
! The value returned is the lower of the two bounding indices;
! 0 is returned for underflows and size(xarr) for overflows.

! The function remembers previous results in order to allow efficient
! lookups.

  real, intent(in) :: xarr(:)         ! Input array of values
  real, intent(in) :: x               ! Value to search for  

  integer, save :: jlo=-1             ! jlo and jhi are the bracketing
  integer :: jm, jhi                  ! indices.  jlo is the function result
  integer :: inc                      ! Size of step to take in index
  integer :: n                        ! Size of xarr
  logical :: is_ascending             ! Is the data ascending or descending

  n=size(xarr)
  is_ascending=xarr(n).gt.xarr(1)
  
  ! If this is the first call or the array has obviously changed
  ! then search the whole range
  if (jlo.lt.0 .or. jlo.gt.n) then
     jlo=0
     jhi=n+1
     goto 3
  end if
  
  ! If this is not the first call, search near the previous result.
  inc=1
  if (x.ge.xarr(jlo) .eqv. is_ascending) then
1    jhi=jlo+inc
     if(jhi.gt.n)then
        jhi=n+1
     else if(x.ge.xarr(jhi).eqv.is_ascending)then
        jlo=jhi
        inc=inc+inc
        goto 1
     end if
  else
     jhi=jlo
2    jlo=jhi-inc
     if(jlo.lt.1)then
        jlo=0
     else if(x.lt.xarr(jlo).eqv.is_ascending)then
        jhi=jlo
        inc=inc+inc
        goto 2
     end if
  end if

  ! Now do a binary chop
3 do while (jhi-jlo.gt.1)
     jm=(jhi+jlo)/2
     if(x.gt.xarr(jm).eqv.is_ascending)then
        jlo=jm
     else
        jhi=jm
     end if
  end do
  
  table_search=jlo
  return

end function table_search
