subroutine partial_index(k,n,arr,ind)

! On input, ind should contain the values 1..n in order
! Does a partial sort of the indexing array:  on return elements 1:k-1 are 
! smaller than element k and elements k+1:n larger than element k.
! The array arr is left intact (unsorted).

  integer, intent(in) :: k,n
  real, intent(in) :: arr(n)
  integer, intent(inout) :: ind(n)

  integer :: i,ir,j,l,mid,itemp
  real :: a

  l=1
  ir=n
1 if (ir-l.le.1) then
     if (ir-l.eq.1) then
        if (arr(ind(ir)).lt.arr(ind(l))) then
           itemp=ind(l)
           ind(l)=ind(ir)
           ind(ir)=itemp
        end if
     end if
     return
  else
     mid=(l+ir)/2
     itemp=ind(mid)
     ind(mid)=ind(l+1)
     ind(l+1)=itemp
     if (arr(ind(l+1)).gt.arr(ind(ir))) then
        itemp=ind(l+1)
        ind(l+1)=ind(ir)
        ind(ir)=itemp
     end if
     if (arr(ind(l)).gt.arr(ind(ir))) then
        itemp=ind(l)
        ind(l)=ind(ir)
        ind(ir)=itemp
     end if
     if (arr(ind(l+1)).gt.arr(ind(l))) then
        itemp=ind(l+1)
        ind(l+1)=ind(l)
        ind(l)=itemp
     end if
     i=l+1
     j=ir
     a=arr(ind(l))
3    continue
     i=i+1

!     if (i.lt.1 .or. i.gt.n) then
!        print*,'i=',i
!        print*,'k=',k
!        print*,'n=',n
!        print*,'size(ind)=',size(ind)
!        print*,'size(arr)=',size(arr)
!     end if

     if(arr(ind(i)).lt.a) goto 3
4    continue
     j=j-1
     if (arr(ind(j)).gt.a) goto 4
     if(j.lt.i) goto 5
     itemp=ind(i)
     ind(i)=ind(j)
     ind(j)=itemp
     goto 3
5    itemp=ind(l)
     ind(l)=ind(j)
     ind(j)=itemp
     if (j.ge.k) ir=j-1
     if (j.le.k) l=i
  end if
  goto 1
      
end subroutine partial_index
