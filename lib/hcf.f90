integer function hcf(i1,i2)

! Finds the highest common factor of i1 & i2

  integer, intent(in) :: i1, i2

  integer :: j1, j2, j3       ! integers in order of decreasinng size

  j1=max(i1,i2)
  j2=min(i1,i2)
  j3=-1
  do while (j3.ne.0)
     j3=mod(j1,j2)
     j1=j2
     j2=j3
  end do
  hcf=j1

end function hcf
