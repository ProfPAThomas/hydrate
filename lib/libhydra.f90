module libhydra

! Interfaces for all the libhydra routines

  interface
     integer function hcf(i1,i2)
       integer, intent(in) :: i1, i2
     end function hcf
  end interface

  interface
     subroutine heap_index(n,arr,ind)
       integer, intent(in) :: n
       real, intent(in) :: arr(n)
       integer, intent(inout) :: ind(n)
     end subroutine heap_index
  end interface
  
  interface
     subroutine partial_index(k,n,arr,ind)
       integer, intent(in) :: k,n
       real, intent(in) :: arr(n)
       integer, intent(inout) :: ind(n)
     end subroutine partial_index
  end interface
  
  interface
     integer function table_search(xarr,x)
       real, intent(in) :: xarr(:)         ! Input array of values
       real, intent(in) :: x               ! Value to search for  
     end function table_search
  end interface
  
end module libhydra
