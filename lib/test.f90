program test

  use libhydra

  print*,'hcf(1,3)=',hcf(1,3)
  print*,'hcf(2,3)=',hcf(2,3)
  print*,'hcf(3,6)=',hcf(3,6)
  print*,'hcf(6,3)=',hcf(6,3)
  print*,'hcf(9,12)=',hcf(9,12)
  print*,'hcf(105,15)=',hcf(105,15)

end program test
