; Script to plot density and acceleration near wall, plus correction

fn='51'

data=read_hydra95('d00'+fn+'.00010',/accel,/debug)
x=(*data.position_ptr)[0,*]/(*data.smooth_ptr)

y=(*data.density_ptr)
yfit= 1 < (0.57+0.77*x)
yc=y/yfit

plot,x,y,/psym,xrange=[0,1],xtitle='x/h',ytitle='density'
ind=sort(x)
oplot,x[ind],yfit[ind],color=2
xyouts,0.4,0.4,'dn=1 < (0.57+0.77(x/h))',color=2 
oplot,x[ind],yc[ind],color=4,psym=4
xyouts,0.4,0.3,'corrected density',color=4
hard_jpg,'figs/dnx'+fn+'.jpg'

;pause

y=(*data.debug_ptr)
yfit= 0. < (x-0.8)*125.
plot,x,y,/psym,xrange=[0,1.5],xtitle='x/h',ytitle='a!dx!n'
oplot,x[ind],yfit[ind],color=2
oplot,x[ind],0.5*yfit[ind],color=4
xyouts,0.7,-25.,'a!dx!n=0 < (x-0.8)*125.',color=2 
xyouts,0.7,-30.,'a!dx!n=0 < (x-0.8)*67.5.',color=4
hard_jpg,'figs/ax'+fn+'.jpg'

y=(*data.debug_ptr)*(*data.smooth_ptr)^3/(*data.mass_ptr)
c=poly_fit(x,y,5,yfit=yfit)
plot,x,y,/psym,xrange=[0,1.5],xtitle='x/h',ytitle='a!dx!nh!u3!n/m'
oplot,x[ind],yfit[ind],color=2
xyouts,0.3,-100.,'a!dx!n=poly(5;-28.4,66.2,-46.6,6.43,3.77,-0.92)',color=2,size=1.2
hard_jpg,'figs/ax1_'+fn+'.jpg'

;; y=(*data.debug_ptr)*(*data.smooth_ptr)/(*data.density_ptr)
;; c=poly_fit(x,y,5,yfit=yfit)
;; plot,x,y,/psym,xrange=[0,1.5],xtitle='x/h',ytitle='a!dx!nh/!7r!3'
;; oplot,x[ind],yfit[ind],color=2
;; xyouts,0.3,-20.,'a!dx!n=poly(5;-28.4,66.2,-46.6,6.43,3.77,-0.92)',color=2,size=1.2
;; hard_jpg,'figs/ax2_'+fn+'.jpg'

;; y=(*data.debug_ptr)*(*data.smooth_ptr)/yc
;; yfit= 0. < (x-0.78)*21.
;; plot,x,y,/psym,xrange=[0,1.5],xtitle='x/h',ytitle='a!dx!nh/!7r!3!dcorr!n'
;; oplot,x[ind],yfit[ind],color=2
;; xyouts,0.7,-12.,'a!dx!n=0 < (x-0.78)*21.',color=2 
;; hard_jpg,'figs/ax3_'+fn+'.jpg'

