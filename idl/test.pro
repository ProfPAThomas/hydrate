; Test shapes for dissipationless extra force
x=findgen(500)*0.001+0.501
plot,x,((1-x)/(1-1./sqrt(3.)))^2
; I prefer this one
oplot,x,((1-x)/(1-1./sqrt(3.)))^4,linestyle=1
oplot,x,(2-sqrt(3.))/(sqrt(3.)-1)*(1-x)/(2*x-1),linestyle=2
oplot,x,(2-sqrt(3.))/(sqrt(3.)-2+1./sqrt(3.))*(1-x)^2/(2*x-1),linestyle=3
oplot,[1,1]/sqrt(3.),[0,1.5]

