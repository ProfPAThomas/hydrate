pro profile_ea3,data,properties,$
            axis=axis,range=range,n_per_bin=n_per_bin,itype=itype,$
            overplot=overplot,psym=psym

; Plots a linear profile of chosen data properties
; data: hydra95 data structure
; properties: string containing a list of one or more properties to plot
;             (see message below for a list)
; axis (optional): axis direction to use for profile
; range (optional): range over which to plot profile
; n_per_bin (optional): number of particles to use for smoothing
;             (defaults to 17; rounds even numbers up to next odd one)
; itype (optional): use only particles of this itype (defaults to all)
; overplot (optional): flag to determine whether plot is new or overplotted
; psym (optional): plot points as symbols on top of line

;Read in DEM
demfile='../test/run7/EA3_surface.asc'

fmt='A,I'
readcol,demfile,dummy,ncols,format=fmt,numline=1
readcol,demfile,dummy,nrows,format=fmt,numline=1,skipline=1
print,ncols,nrows

fmt='A,F'
readcol,demfile,dummy,xmin,format=fmt,numline=1,skipline=2
readcol,demfile,dummy,ymin,format=fmt,numline=1,skipline=3
readcol,demfile,dummy,cellsize,format=fmt,numline=1,skipline=4
print,xmin,ymin,cellsize

ncols=ncols[0]
nrows=nrows[0]
xmin=xmin[0]
ymin=ymin[0]
cellsize=cellsize[0]

openr,lun,demfile,/get_lun
dummy=strarr(6)
readf,lun,dummy
dem=fltarr(ncols,nrows)
readf,lun,dem
free_lun,lun

; Define midpoints of cells
xdem=xmin+(0.5+indgen(ncols))*cellsize
; Surface is height at midpoint fo cells in x-direction.
surface=dem[*,0]


if n_elements(properties) eq 0 then begin
    property_error:
    print,'Incorrect property list.  Must choose one or more of:'
    ;print,'    a - energy (thermal + kinetic) ratio dm/gas'
    ;print,'    b - beta: total energy dm/thermal energy gas'
    print,'    d - density (from SPH)'
    ;print,'    e - energy (thermal + kinetic)'
    ;print,'    F - cumulative baryon fraction'
    ;print,'    G - cumulative gas fraction'
    print,'    h - smoothing length (from SPH)'
    ;print,'    k - kinetic energy'
    ;print,'    l - emissivity'
    ;print,'    L - cumulative luminosity'
    ;print,'    m - density (from mass/volume)'
    ;print,'    M - cumulative mass'
    ;print,'    p - thermal pressure'
    ;print,'    P - predicted mass from thermal pressure'
    ;print,'    q - dynamical pressure'
    ;print,'    Q - predicted mass from dynamical pressure'
    ;print,'    s - entropy'
    ;print,'    t - temperature (thermal energy)'
    ;print,'    x - x-ray temperature (emission-weighted)'
    print,'    v - velocity in projection direction'
    message,'usage: profile,data,properties,axis=axis,range=range,n_per_bin=n_per_bin,itype=itype'
end

; Define axis direction for profile
if n_elements(axis) eq 0 then axis=1
case axis of
    1: xtitle='x'
    2: xtitle='y'
    else: message,'axis must be 1 or 2 (defaults to 1)'
endcase

; Define range for profile
; By default the range matches that of the data
case n_elements(range) of
    0: range=[min((*data.position_ptr)[axis-1,*],max=xmax),xmax]
    2: 
    else: message,'Wrong number of elements for range (must be 0 or 2)'
endcase

; Number of particles per bin
if n_elements(n_per_bin) eq 0 then n_per_bin=17
if n_per_bin ne round(n_per_bin) then message,'n_per_bin should be an odd integer'
if n_per_bin lt 1 then message,'Need at least 1 particle per bin (defaults to 17)'
n_per_half_bin=n_per_bin/2
n_per_bin=2*n_per_half_bin+1

; Select particles to use based on itype
@itype
index=where(*data.itype_ptr ne itype_none)
if n_elements(itype) ne 0 then begin
    if itype lt itype_low or itype gt itype_high then message,'Invalid itype'
    index=where(*data.itype_ptr eq itype)
end

; Set flag to determine whether to overplot
flag_overplot=0b
if keyword_set(overplot) then flag_overplot=1b

; Sort particles along profile direction
index=index(sort((*data.position_ptr)[axis-1,index]))

; Determine the x-values
x=(*data.position_ptr)[axis-1,index]

; Extract desired data (y-values)
y=fltarr(n_elements(index))
case properties[0] of
    'd': begin
        ytitle='SPH density'
        index_data=where(index lt n_elements(*data.density_ptr),n_data,$
                         complement=index_nodata,ncomplement=n_nodata)
        if n_data gt 0 then y[index_data]=$
          (*data.density_ptr)[index[index_data]]
        if n_nodata gt 0 then begin
            print,'Warning: property not defined for some data points'
            y[index_nodata]=!VALUES.F_NAN
         endif

        ;Adjust depths to account for underlying DEM
        for i=0L,n_elements(x)-1L do begin
           ix=floor((x[i]-xmin)/cellsize)
           y[i]=surface[ix]+y[i]
           print,x[i],ix,surface[ix]
        endfor
    end
    'e': begin
        ytitle='total energy'
        y=0.5*total((*data.velocity_ptr)[*,index]^2,1)
        index_data=where(index lt n_elements(*data.density_ptr),n_data,$
                         complement=index_nodata,ncomplement=n_nodata)
        if n_data gt 0 then y[index_data]=y[index_data]+$
          (*data.temperature_ptr)[index[index_data]]
    end
    'h': begin
        ytitle='SPH smoothing length'
        index_data=where(index lt n_elements(*data.smooth_ptr),n_data,$
                         complement=index_nodata,ncomplement=n_nodata)
        if n_data gt 0 then y[index_data]=$
          (*data.smooth_ptr)[index[index_data]]
        if n_nodata gt 0 then begin
            print,'Warning: property not defined for some data points'
            y[index_nodata]=!VALUES.F_NAN
        endif
    end
    'm': begin
        ytitle='M/V density'
        volume=product(round(max(*data.position_ptr,dim=2))-$
                       round(min(*data.position_ptr,dim=2)))
        area=volume/(round(max((*data.position_ptr)[axis-1,*]))-$
                       round(min((*data.position_ptr)[axis-1,*])))
        ; To get correct normalisation omit central mass
        for i=long(n_per_half_bin),n_elements(y)-n_per_half_bin-1 do begin
            mass=total((*data.mass_ptr)[index[i-n_per_half_bin:i+n_per_half_bin]])$
              -(*data.mass_ptr)[index[i]]
            y[i]=mass/(x[i+n_per_half_bin]-x[i-n_per_half_bin])/area
        end
        y[0:n_per_half_bin-1]=y[n_per_half_bin]
        y[n_elements(y)-n_per_half_bin:n_elements(y)-1]=$
          y[n_elements(y)-n_per_half_bin-1]
    end
    'p': begin
        ytitle='pressure'
        index_data=where(index lt n_elements(*data.density_ptr),n_data,$
                         complement=index_nodata,ncomplement=n_nodata)
        if n_data gt 0 then y[index_data]=$
          (*data.density_ptr)[index[index_data]]^data.gamma-1.
        if n_nodata gt 0 then begin
            print,'Warning: property not defined for some data points'
            y[index_nodata]=!VALUES.F_NAN
        endif
    end
    's': begin
        ytitle='entropy'
        index_data=where(index lt n_elements(*data.density_ptr),n_data,$
                         complement=index_nodata,ncomplement=n_nodata)
        if n_data gt 0 then y[index_data]=$
          (*data.temperature_ptr)[index[index_data]]/$
          (*data.density_ptr)[index[index_data]]^(2./3.)
        if n_nodata gt 0 then begin
            print,'Warning: property not defined for some data points'
            y[index_nodata]=!VALUES.F_NAN
        endif
    end
    't': begin
        ytitle='mass-weighted temperature'
        index_data=where(index lt n_elements(*data.density_ptr),n_data,$
                         complement=index_nodata,ncomplement=n_nodata)
        if n_data gt 0 then y[index_data]=$
          (*data.temperature_ptr)[index[index_data]]
        if n_nodata gt 0 then begin
            print,'Warning: property not defined for some data points'
            y[index_nodata]=!VALUES.F_NAN
        endif
    end
    'v': begin
        ytitle='velocity'
        y=(*data.velocity_ptr)[axis-1,index]
    end
    else: goto,property_error
endcase

; Find scatter between -1.2 and -0.2
;xsmooth=smooth(x,n_per_bin)
;index=where(xsmooth gt -1.2 and xsmooth lt -0.2)
;ysmooth=smooth(y,n_per_bin,/nan)
;print,'sigma(-1.2<z<-0.2)=',stddev(y[index]-ysmooth[index])


; Smooth data and throw away unsmoothed bins
x=smooth(x,n_per_bin)
x=x[n_per_half_bin:n_elements(x)-n_per_half_bin-1]
; Note: for weighted data could do y=yw/w where yw is data*weight and
; w is weight, both smoothed as below.
y=smooth(y,n_per_bin,/nan)
y=y[n_per_half_bin:n_elements(y)-n_per_half_bin-1]

; Initialise plot
if flag_overplot then $
    colour=2 $
else begin
    window,xsize=800,ysize=600
    start_plot
    plot,x,y,/nodata,xtitle=xtitle,ytitle=ytitle,xrange=range,xstyle=1,yrange=[9.7,11.0],ystyle=1
    colour=!p.color
endelse

; Plot y-values
if n_elements(psym) gt 0 then begin
    npoint=n_elements(x)/n_per_bin
    index=n_per_bin*indgen(npoint)+n_per_bin/2
    oplot,x[index],y[index],color=colour,psym=psym
endif else $
  oplot,x,y,color=colour,linestyle=0

;Plot DEM
oplot,xdem,surface,linestyle=2
end
