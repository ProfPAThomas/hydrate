; Script to do a 2-d plot of particle location

d1001=read_hydra95('d0063.01001',/accel,/debug)
data=d1001
size=0.6

start_plot

symbols,1,size,color=2
index=where((*data.density_ptr) gt 1.05)
x=(*data.position_ptr)[0,index]
y=(*data.position_ptr)[1,index]
plot,x,y,psym=8,/isotropic,xtitle='x',ytitle='y'

symbols,1,0.75*size,color=2
index=where((*data.density_ptr) gt 1.025 and (*data.density_ptr lt 1.05))
x=(*data.position_ptr)[0,index]
y=(*data.position_ptr)[1,index]
oplot,x,y,psym=8

symbols,1,0.5*size,color=3
index=where((*data.density_ptr) gt 0.975 and (*data.density_ptr lt 1.025))
x=(*data.position_ptr)[0,index]
y=(*data.position_ptr)[1,index]
oplot,x,y,psym=8

symbols,1,0.75*size,color=4
index=where((*data.density_ptr) gt 0.95 and (*data.density_ptr lt 0.975))
x=(*data.position_ptr)[0,index]
y=(*data.position_ptr)[1,index]
oplot,x,y,psym=8

symbols,1,size,color=4
index=where((*data.density_ptr) le 0.95)
x=(*data.position_ptr)[0,index]
y=(*data.position_ptr)[1,index]
oplot,x,y,psym=8
