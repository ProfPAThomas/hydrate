@ea6_den 
v2=(*data.velocity_ptr)[0,*]^2+(*data.velocity_ptr)[1,*]^2 
hist_plot,v2,/ylog
print,max(v2)
ivel=where(v2 gt 0.9*max(v2))
nup=9 < n_elements(ivel)-1
print,ivel[0:nup]
;@ea6_den
;oplot,(*data.position_ptr)[0,ivel],(*data.position_ptr)[1,ivel],/psym,color=4
print,(*data.position_ptr)[*,ivel[0:nup]]
