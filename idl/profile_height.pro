pro profile_height,data,floor,X0,cellsize,$
            axis=axis,range=range,n_per_bin=n_per_bin,$
            overplot=overplot,psym=psym,color=color,yrange=yrange,$
            noerase=noerase,isotropic=isotropic,position=position

; Plots a linear profile of chosen data height
; data: hydra95 data structure
; floor: Array containing heights at the centre of the cells of the surface
; floor from 1 cell before box to 1 cell after box.
; X0: the position of the centre of the first cell in the box (not before)
; cellsize: distance between each cell centre
; axis (optional): axis direction to use for profile
; range (optional): range over which to plot profile
; n_per_bin (optional): number of particles to use for smoothing
;             (defaults to 17; rounds even numbers up to next odd one)
; overplot (optional): flag to determine whether plot is new or overplotted
; psym (optional): plot points as symbols on top of line
; color (optional): plotting colour
; yrange (optional): range on y-axis
; noerase (optional): don't erase the previous plot
; isotropic (optional): use isotropic axes
; positions (optional): define position on page


; Define axis direction for profile
if n_elements(axis) eq 0 then axis=1
case axis of
    1: xtitle='x'
    2: xtitle='y'
    else: message,'axis must be 1 or 2 (defaults to 1)'
endcase

; Define range for profile
; By default the range matches that of the data
case n_elements(range) of
    0: range=[min((*data.position_ptr)[axis-1,*],max=xmax),xmax]
    2: 
    else: message,'Wrong number of elements for range (must be 0 or 2)'
endcase

; Number of particles per bin
if n_elements(n_per_bin) eq 0 then n_per_bin=17
if n_per_bin ne round(n_per_bin) then message,'n_per_bin should be an odd integer'
if n_per_bin lt 1 then message,'Need at least 1 particle per bin (defaults to 17)'
n_per_half_bin=n_per_bin/2
n_per_bin=2*n_per_half_bin+1



; Set flag to determine whether to erase
if not keyword_set(noerase) then noerase=0b

; Set flag to determine whether to overplot
flag_overplot=0b
if keyword_set(overplot) then flag_overplot=1b

; Determine color
if n_elements(color) eq 0 then $
   if flag_overplot then $
      color=2 $
   else $
      color=!p.color

; Set isotropic keyword
if not keyword_set(isotropic) then isotropic=0b

; Set isotropic keyword
if n_elements(position) eq 0 then $
   position=[0.15,0.1,0.95,0.95]

; Sort particles along profile direction
index=sort((*data.position_ptr)[axis-1,*])

; Determine the x-values
x=(*data.position_ptr)[axis-1,index]

; Extract height data (y-values)
y=fltarr(n_elements(index))

;Finds height of floor for each data point
j=fltarr(n_elements(index))
h=fltarr(n_elements(index))
for i=0,n_elements(index)-1 do begin 
   j[i]=CEIL((x[i]-X0)/cellsize)
   dx=x[i]-(X0+j[i]-1)*cellsize
   h[i]=Floor[j[i]]+dx*(Floor[j[i]+1]-Floor[j[i]])/cellsize
endfor

;ytitle='SPH density'
ytitle='Height of water surface'
y=(*data.density_ptr)[index]
y=y+h
   

; Find scatter between -1.2 and -0.2
;xsmooth=smooth(x,n_per_bin)
;index=where(xsmooth gt -1.2 and xsmooth lt -0.2)
;ysmooth=smooth(y,n_per_bin,/nan)
;print,'sigma(-1.2<z<-0.2)=',stddev(y[index]-ysmooth[index])


; Smooth data and throw away unsmoothed bins
if n_per_bin gt 2 then x=smooth(x,n_per_bin)
x=x[n_per_half_bin:n_elements(x)-n_per_half_bin-1]
; Note: for weighted data could do y=yw/w where yw is data*weight and
; w is weight, both smoothed as below.
if n_per_bin gt 2 then y=smooth(y,n_per_bin,/nan)
y=y[n_per_half_bin:n_elements(y)-n_per_half_bin-1]

; Initialise plot
if not flag_overplot then begin
    ;window,xsize=800,ysize=600
    start_plot
    if n_elements(yrange) ne 0 then $
       plot,x,y,/nodata,xtitle=xtitle,ytitle=ytitle, $
            xrange=range,xstyle=1,yrange=yrange,ystyle=1 $
            ,noerase=noerase,isotropic=isotropic,position=position $
    else $
       plot,x,y,/nodata,xtitle=xtitle,ytitle=ytitle, $
            xrange=range,xstyle=1, $
            noerase=noerase,isotropic=isotropic,position=position

endif

; Plot y-values
if n_elements(psym) gt 0 then begin
    npoint=n_elements(x)/n_per_bin
    index=n_per_bin*indgen(npoint)+n_per_bin/2
    oplot,x[index],y[index],color=color,psym=psym
endif else $
  oplot,x,y,color=color,linestyle=0

end
