pro density_plot,data

symbols,1,0.25,color=3
index=where((*data.density_ptr) ge 0.975 and (*data.density_ptr le 1.025))
x=(*data.position_ptr)[0,index]
y=(*data.position_ptr)[1,index]
plot,x,y,psym=8,/isotropic,$
     xstyle=1,xtickformat="(A1)",ystyle=1,ytickformat="(A1)"

for i=0,n_elements(*data.density_ptr)-1 do begin

   den=(*data.density_ptr)[i]
   if (den gt 1.025) then $
      symbols,1,10.*(den-1.),color=4 $
   else if (den lt 0.975) then $
      symbols,1,10.*(1.-den),color=2 $
   else $
      continue
   x=(*data.position_ptr)[0,i]
   y=(*data.position_ptr)[1,i]
   oplot,[x],[y],psym=8

endfor

end
