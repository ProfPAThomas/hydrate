pro frames

; Procedure to generate movie stills for the dam-break
; particles and density

NSTILLMIN=0
NSTILLMAX=500
RUNL=164
RUNH=173
XMIN=-4.
XMAX=10.
COLOURTABLE=39
DENMIN=0.01
DENMAX=1.05*0.4

DT=0.1
LEFT=75873
PXMIN=0.125
PXMAX=0.9
PYMIN1=0.075
PYMAX1=0.425
PYMIN2=0.48
PYMAX2=0.83
PYMINCB=0.89
PYMAXCB=0.915

slope=0.155/0.34
dx=0.1
ymin=-1.8
ymax=1.8

cgLoadCT, COLOURTABLE
nxbin=floor((XMAX-XMIN)/dx)
nybin=floor((ymax-ymin)/dx)
x=XMIN+(findgen(nxbin)+0.5)*dx
y=ymin+(findgen(nybin)+0.5)*dx

; House
theta=64*!pi/180.
ct=cos(theta)
st=sin(theta)
l1=0.4
l2=0.8
x0=[3.44, 1.75-1.8]
x1=[x0[0]+l1*st,x0[1]-l1*ct]
x2=[x1[0]+l2*ct,x1[1]+l2*st]
x3=[x2[0]-l1*st,x2[1]+l1*ct]
x4=[x3[0]-l2*ct,x3[1]-l2*st]

for istill = NSTILLMIN, NSTILLMAX do begin

   filename=string('d',RUNL,'.o',istill,format='(a1,i4.4,a2,i4.4)')
   data=read_hydra95(filename)
   ; Apply floor correction
   index1=where((*data.position_ptr)[1,*] le -1.46)
   (*data.density_ptr)[index1]+=(-1.46-(*data.position_ptr)[1,index1])*slope
   index2=where((*data.position_ptr)[1,*] gt 1.46)
   (*data.density_ptr)[index2]+=((*data.position_ptr)[1,index2]-1.46)*slope
   ; Create density array
   num=intarr(nxbin,nybin)
   den=fltarr(nxbin,nybin)
   ; Populate array
   index=where((*data.position_ptr)[0,*] gt XMIN and $
               (*data.position_ptr)[0,*] lt XMAX and $
               (*data.position_ptr)[1,*] gt ymin and $
               (*data.position_ptr)[1,*] lt ymax )
   ix=floor(((*data.position_ptr)[0,index]-XMIN)/dx)
   iy=floor(((*data.position_ptr)[1,index]-ymin)/dx)
   for i=0L,n_elements(index)-1 do $
      num[ix[i],iy[i]]+=1
   for i=0L,n_elements(index)-1 do $
      den[ix[i],iy[i]]+=(*data.density_ptr)[index[i]]
   index=where(num gt 0)
   den[index]=den[index]/num[index]
   den[where(den le DENMIN)]=DENMAX
   scaledImage = BytScl(alog10(den), MIN=alog10(DENMIN), MAX=alog10(DENMAX))
   cgImage, scaledImage, /Keep_Aspect, $
            position =   [PXMIN, PYMIN1, PXMAX, PYMAX1],/axes, $
            xrange=[XMIN,XMAX],yrange=[ymin,ymax]
   ; Walls
   oplot,[91.45,0.,0.,-0.8,-0.8,-7.55,-7.55,-0.8,-0.8,0.,0.,91.45,91.45],$
      [-1.8,-1.8,-0.5,-0.5,-1.8,-1.8,1.8,1.8,0.5,0.5,1.8,1.8,-1.8]
   polyfill,[-0.8,0.,0.,-0.8],[-1.8,-1.8,-0.5,-0.5],color=255
   polyfill,[-0.8,0.,0.,-0.8],[1.8,1.8,0.5,0.5],color=255
   ; House
   oplot,[x0[0],x1[0],x2[0],x3[0],x0[0]],[x0[1],x1[1],x2[1],x3[1],x0[1]]
   polyfill,[x0[0],x1[0],x2[0],x3[0]],[x0[1],x1[1],x2[1],x3[1]],color=255
   ; Measurement points
   oplot,[ 2.65, 2.65, 4.0, 4.0, 5.2, -1.87],[1.15,-0.6,1.15,-0.8,0.3,1.1],psym=1

   filename=string('d',RUNL,'.o',istill,format='(a1,i4.4,a2,i4.4)')
   data=read_hydra95(filename)
   ; Apply floor correction
   index1=where((*data.position_ptr)[1,*] le -1.46)
   (*data.density_ptr)[index1]+=(-1.46-(*data.position_ptr)[1,index1])*slope
   index2=where((*data.position_ptr)[1,*] gt 1.46)
   (*data.density_ptr)[index2]+=((*data.position_ptr)[1,index2]-1.46)*slope
   den=fltarr(nxbin,nybin)+DENMAX
   scaledImage = BytScl(alog10(den), MIN=alog10(DENMIN), MAX=alog10(DENMAX))
   cgImage, scaledImage, /Keep_Aspect, /noerase,$
            position =   [PXMIN, PYMIN2, PXMAX, PYMAX2],/axes, $
            xrange=[XMIN,XMAX],yrange=[ymin,ymax]
   symbols,1,0.25,color=500
   x=(*data.position_ptr)[0,0:LEFT]
   y=(*data.position_ptr)[1,0:LEFT]
   oplot,x,y,psym=8
   symbols,1,0.25,color=50
   x=(*data.position_ptr)[0,LEFT+1:*]
   y=(*data.position_ptr)[1,LEFT+1:*]
   oplot,x,y,psym=8
   ; Walls
   oplot,[91.45,0.,0.,-0.8,-0.8,-7.55,-7.55,-0.8,-0.8,0.,0.,91.45,91.45],$
      [-1.8,-1.8,-0.5,-0.5,-1.8,-1.8,1.8,1.8,0.5,0.5,1.8,1.8,-1.8]
   polyfill,[-0.8,0.,0.,-0.8],[-1.8,-1.8,-0.5,-0.5],color=255
   polyfill,[-0.8,0.,0.,-0.8],[1.8,1.8,0.5,0.5],color=255
   ; House
   oplot,[x0[0],x1[0],x2[0],x3[0],x0[0]],[x0[1],x1[1],x2[1],x3[1],x0[1]]
   polyfill,[x0[0],x1[0],x2[0],x3[0]],[x0[1],x1[1],x2[1],x3[1]],color=255
   ; Measurement points
   oplot,[ 2.65, 2.65, 4.0, 4.0, 5.2, -1.87],[1.15,-0.6,1.15,-0.8,0.3,1.1],psym=1

   ; Draw the color bar. Fit it to the location of the image. 
   ; Note have rescaled the color table to omit the last, white point.
   cgColorbar, Position= [PXMIN, PYMINCB, PXMAX, PYMAXCB], $
            Range=[alog10(DENMIN),alog10(DENMAX)], $
            Title='log!d10!n(height/m)', TLocation='Top',ncolors=255

   ; Time
   xyouts,0.77,0.94,string('t/s=',istill*DT,format='(a4,f5.2)'),$
          /normal,charsize=1.3

   ; Save the image
   filename=string('frames/den',istill,'.png',format='(a10,i4.4,a4)')
   print,'filesname=',filename
   print,'istill = ',istill,'; save status = ',$
        cgSnapshot(Filename=filename,/png,/nodialog)

endfor

set_basic_colours
;contour,den,x,y,/isotropic,/fill,

end
