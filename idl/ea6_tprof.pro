PS=0b

; 164 - low-res
; 173 - high-res
; 174 - high res, less damping.
; 175 - high-res, manning=0.011
; 176 - high-res, manning=0.015
; 177 - high-res, manning=0.012
; 182 - super-high res
FILE1='d0173.ea6'
FILE2='d0164.ea6'

start_plot

if PS then open_ps,'figs/ea6_tprof.ps' else window,0
plot,[0,1],[0,1],/nodata,xrange=[0,60],xtitle='time / s'$
     ,ytitle='depth / m',yrange=[0,0.2],ystyle=1
oplot,[0,60],[0.05,0.05],linestyle=1
oplot,[0,60],2*[0.05,0.05],linestyle=1
oplot,[0,60],3*[0.05,0.05],linestyle=1
oplot,[0,60],4*[0.05,0.05],linestyle=1
oplot,[0,60],5*[0.05,0.05],linestyle=1
oplot,[0,60],6*[0.05,0.05],linestyle=1
oplot,[0,60],7*[0.05,0.05],linestyle=1
oplot,[10,10],[0,0.42],linestyle=1
oplot,2*[10,10],[0,0.42],linestyle=1
oplot,3*[10,10],[0,0.42],linestyle=1
oplot,4*[10,10],[0,0.42],linestyle=1
oplot,5*[10,10],[0,0.42],linestyle=1

tprof=read_ascii('data/'+FILE1)
time=tprof.(0)[0,*]
den1=tprof.(0)[1,*]
den2=tprof.(0)[4,*]
den3=tprof.(0)[7,*]
den4=tprof.(0)[10,*]
den5=tprof.(0)[13,*]
den6=tprof.(0)[16,*]
;oplot,time,den1,color=!p.color
;oplot,time,den2,color=2
;oplot,time,den3,color=3
oplot,time,den4,color=4
oplot,time,den5,color=5
;oplot,time,den6,color=6

tprof=read_ascii('data/'+FILE2)
time=tprof.(0)[0,*]
den1=tprof.(0)[1,*]
den2=tprof.(0)[4,*]
den3=tprof.(0)[7,*]
den4=tprof.(0)[10,*]
den5=tprof.(0)[13,*]
den6=tprof.(0)[16,*]
;oplot,time,den1,color=!p.color,thick=2
;oplot,time,den2,color=2,thick=2
;oplot,time,den3,color=3,thick=2
oplot,time,den4,color=4,thick=2
oplot,time,den5,color=5,thick=2
;oplot,time,den6,color=6,thick=2

if PS then close_ps else hard_jpg,'figs/ea6_tprof.jpg'

if PS then open_ps,'figs/ea6_vprof.ps' else window,1
plot,[0,1],[0,1],/nodata,xrange=[0,60],xtitle='time / s'$
     ,ytitle='speed / ms!u-1!n',yrange=[0,2.5],ystyle=1
oplot,[0,60],[0.2,0.2],linestyle=1
oplot,[0,60],2*[0.2,0.2],linestyle=1
oplot,[0,60],3*[0.2,0.2],linestyle=1
oplot,[0,60],4*[0.2,0.2],linestyle=1
oplot,[0,60],5*[0.2,0.2],linestyle=1
oplot,[0,60],6*[0.2,0.2],linestyle=1
oplot,[0,60],7*[0.2,0.2],linestyle=1
oplot,[0,60],8*[0.2,0.2],linestyle=1
oplot,[0,60],9*[0.2,0.2],linestyle=1
oplot,[0,60],10*[0.2,0.2],linestyle=1
oplot,[0,60],11*[0.2,0.2],linestyle=1
oplot,[0,60],12*[0.2,0.2],linestyle=1
oplot,[10,10],[0,0.42],linestyle=1
oplot,2*[10,10],[0,0.42],linestyle=1
oplot,3*[10,10],[0,0.42],linestyle=1
oplot,4*[10,10],[0,0.42],linestyle=1
oplot,5*[10,10],[0,0.42],linestyle=1

tprof=read_ascii('data/'+FILE1)
time=tprof.(0)[0,*]
v1=sqrt((tprof.(0)[2,*])^2+(tprof.(0)[3,*])^2)
v2=sqrt((tprof.(0)[5,*])^2+(tprof.(0)[6,*])^2)
v3=sqrt((tprof.(0)[8,*])^2+(tprof.(0)[9,*])^2)
v4=sqrt((tprof.(0)[11,*])^2+(tprof.(0)[12,*])^2)
v5=sqrt((tprof.(0)[14,*])^2+(tprof.(0)[15,*])^2)
v6=sqrt((tprof.(0)[17,*])^2+(tprof.(0)[18,*])^2)
oplot,time,v1,color=!p.color
oplot,time,v2,color=2
;oplot,time,v3,color=3
;oplot,time,v4,color=4
;oplot,time,v5,color=5
oplot,time,v6,color=6

tprof=read_ascii('data/'+FILE2)
time=tprof.(0)[0,*]
v1=sqrt((tprof.(0)[2,*])^2+(tprof.(0)[3,*])^2)
v2=sqrt((tprof.(0)[5,*])^2+(tprof.(0)[6,*])^2)
v3=sqrt((tprof.(0)[8,*])^2+(tprof.(0)[9,*])^2)
v4=sqrt((tprof.(0)[11,*])^2+(tprof.(0)[12,*])^2)
v5=sqrt((tprof.(0)[14,*])^2+(tprof.(0)[15,*])^2)
v6=sqrt((tprof.(0)[17,*])^2+(tprof.(0)[18,*])^2)
oplot,time,v1,color=!p.color,thick=2
oplot,time,v2,color=2,thick=2
;oplot,time,v3,color=3,thick=2
;oplot,time,v4,color=4,thick=2
;oplot,time,v5,color=5,thick=2
oplot,time,v6,color=6,thick=2

if PS then close_ps else hard_jpg,'figs/ea6_vprof.jpg'

