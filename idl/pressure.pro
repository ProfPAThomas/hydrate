; Script to draw pressure profile

h0=10.
P0=1.
gamma=7.
h=7.6

z=0.1*indgen(77)
P=P0*((gamma-1.)/gamma*(h-z)/h0+1.)^(gamma/(gamma-1))-1.

z=[-reverse(z),z]
P=[reverse(P),P]

oplot,z,P,color=2
