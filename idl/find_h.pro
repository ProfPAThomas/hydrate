;;This procedure and function are used to solve for the root of the
;;cubic =(2*g*H-v^2)*(d+H-H0)^2+d^2*v^2

;Function used by fx_root, shares variables with find_h. g is
;gravitational acceleration
FUNCTION finding, H
common heightshare, g,d,v,H0
  g=9.80665D
  RETURN, (2*g*H-v^2)*(d+H-H0)^2+d^2*v^2
end


;Procedure to take in the density(depth) at the start of the flow, and
;the velocity at the start of the flow, the height of the floor in an array and
;the length of in x to plot over. Then will loop through all the
;values of predefined floor height in array Floor and generates the height change,height_rel and also the final height, defined predictably under height.

pro find_h,density,velocity,floor,length
common heightshare,g,d,v,H0
d=density                 ;depth before bump    (also shares variable)
v=velocity                ;velocity before bump (also shares variable)
h=[0.0,-d/2,d/2]              ;require initial guess vector for fx_root
height_rel=fltarr(n_elements(floor))   ;Change in height
height=fltarr(n_elements(floor))       ;Change in height + initial depth of flow
for i=0,n_elements(floor)-1 do begin
   H0=floor[i]
   height_rel[i]=fx_root(h,'finding',/DOUBLE)
   height[i]=height_rel[i]+d
endfor
;Plotting
x=dindgen(n_elements(floor))*length/(n_elements(floor)-2)   
;The -2 is because currently the floor array is defined from 1 cell before the box we are interested
;in to 1 cell afterwards, so that no extrapolation is need for the floor surface
x=x-0.5
;Shift to reflect cells height defined at their centre

oplot,x,height,color=3,thick=3

;Debugging
;print,'height=',height           ;for debugging
;print,'height_rel=',height_rel   ;for debugging
;print,'x=',x                     ;for debugging
;print,'d=',d,'v=',v              ;for debugging


;Give's the Froude Number
print, 'Froude Number = ', v^2./(g*d)
end



