pro acc,x,p,f,dfdp

; functional form for fit to acceleration correction

f=-exp(p[0]*(p[1]-x))
if n_params() ge 4 then dfdp=[[(p[1]-x)*f],[p[0]*f]]
help,dfdp

end
