damping='linear'
;damping='quadratic'
;n_per_bin=1
n_per_bin=1

if damping eq 'linear' then qdp1=read_qdp('d0106',nstep=100000)
if damping eq 'linear' then qdp2=read_qdp('d0107',nstep=100000)
if damping eq 'linear' then qdp3=read_qdp('d0110',nstep=100000)
if damping eq 'linear' then qdp5=read_qdp('d0105',nstep=100000)
if damping eq 'quadratic' then qdp1=read_qdp('d0103',nstep=100000)
if damping eq 'quadratic' then qdp2=read_qdp('d0108',nstep=100000)
if damping eq 'quadratic' then qdp3=read_qdp('d0109',nstep=100000)
if damping eq 'quadratic' then qdp5=read_qdp('d0104',nstep=100000)

data=read_hydra95('d0103.00000')
if damping eq 'linear' then data1=read_hydra95('d0106.04743')
if damping eq 'linear' then data2=read_hydra95('d0107.04676')
if damping eq 'linear' then data3=read_hydra95('d0110.04466')
if damping eq 'linear' then data5=read_hydra95('d0105.05558')
if damping eq 'quadratic' then data1=read_hydra95('d0103.05025')
if damping eq 'quadratic' then data2=read_hydra95('d0108.08005')
if damping eq 'quadratic' then data3=read_hydra95('d0109.09598')
if damping eq 'quadratic' then data5=read_hydra95('d0104.11215')

window,0
vterm=5.
tau=50.
t=qdp1.time
v=sqrt(2*qdp1.ke/(data.nobj*(*data.mass_ptr)[0]))
plot,t,v,/ylog,yrange=[0.01,1],/xlog,xrange=[0.1,10]
t=qdp2.time
v=sqrt(2*qdp2.ke/(data.nobj*(*data.mass_ptr)[0]))
oplot,t,v,color=2
t=qdp3.time
v=sqrt(2*qdp3.ke/(data.nobj*(*data.mass_ptr)[0]))
oplot,t,v,color=3
t=qdp5.time
v=sqrt(2*qdp5.ke/(data.nobj*(*data.mass_ptr)[0]))
oplot,t,v,color=4
oplot,t,vterm*tanh(t/tau),color=0,linestyle=1
if damping eq 'linear' then xyouts, 2,0.09,'linear, 1',color=0
if damping eq 'linear' then xyouts, 2,0.06,'linear, 2',color=2
if damping eq 'linear' then xyouts, 2,0.04,'linear, 3',color=3
if damping eq 'linear' then xyouts, 2,0.0266,'linear, 5',color=4
if damping eq 'quadratic' then xyouts, 2,0.09,'quadratic, 1',color=0
if damping eq 'quadratic' then xyouts, 2,0.06,'quadratic, 2',color=2
if damping eq 'quadratic' then xyouts, 2,0.04,'quadratic, 3',color=3
if damping eq 'quadratic' then xyouts, 2,0.0266,'quadratic, 5',color=4
;hard_jpg,'figs/manning_vt.jpg'

window,1
vterm=5.
tau=50.
t=qdp1.time
v=sqrt(2*qdp1.ke/(data.nobj*(*data.mass_ptr)[0]))/(vterm*tanh(t/tau))-1
plot,t,v,/xlog,xrange=[0.1,10],/ylog,yrange=[0.0001,10]
t=qdp2.time
v=sqrt(2*qdp2.ke/(data.nobj*(*data.mass_ptr)[0]))/(vterm*tanh(t/tau))-1
oplot,t,v,color=2
t=qdp3.time
v=sqrt(2*qdp3.ke/(data.nobj*(*data.mass_ptr)[0]))/(vterm*tanh(t/tau))-1
oplot,t,v,color=3
t=qdp5.time
v=sqrt(2*qdp5.ke/(data.nobj*(*data.mass_ptr)[0]))/(vterm*tanh(t/tau))-1
oplot,t,v,color=4
if damping eq 'linear' then xyouts, 0.16,0.01,'linear, 1',color=0
if damping eq 'linear' then xyouts, 0.16,0.003,'linear, 2',color=2
if damping eq 'linear' then xyouts, 0.16,0.001,'linear, 3',color=3
if damping eq 'linear' then xyouts, 0.16,0.0003,'linear, 5',color=4
if damping eq 'quadratic' then xyouts, 0.16,0.01,'quadratic, 1',color=0
if damping eq 'quadratic' then xyouts, 0.16,0.003,'quadratic, 2',color=2
if damping eq 'quadratic' then xyouts, 0.16,0.001,'quadratic, 3',color=3
if damping eq 'quadratic' then xyouts, 0.16,0.0003,'quadratic, 5',color=4

window,3
(*data1.velocity_ptr)=(*data1.velocity_ptr)+4-vterm*tanh(100./tau)
profile_linear,data1,'v',n_per_bin=n_per_bin,yrange=[0,5]
(*data2.velocity_ptr)=(*data2.velocity_ptr)+3-vterm*tanh(100./tau)
profile_linear,data2,'v',n_per_bin=n_per_bin,color=2,/overplot
(*data3.velocity_ptr)=(*data3.velocity_ptr)+2-vterm*tanh(100./tau)
profile_linear,data3,'v',n_per_bin=n_per_bin,color=3,/overplot
(*data5.velocity_ptr)=(*data5.velocity_ptr)+1-vterm*tanh(100./tau)
profile_linear,data5,'v',n_per_bin=n_per_bin,color=4,/overplot
if damping eq 'linear' then xyouts,1.75,4.5,'linear'
if damping eq 'quadratic' then xyouts,1.5,4.5,'quadratic'
