pro plot_2d,data,range=range,psym=psym,overplot=overplot,itype=itype

; Plots the particle postions

; data: hydra95 data structure

; Define range for projection
; By default the range matches that of the data
range0=[min((*data.position_ptr)[0,*],max=xmax),xmax]
range1=[min((*data.position_ptr)[1,*],max=ymax),ymax]
; Redfine range to match input range parameter
case n_elements(range) of
    ; If no input range, use whole data range (ie do nothing)
    0: 
    ; If four numbers are input use these for the x and y ranges
    ; The coding allows for 1x4, 2x2 or 4x1
    4: begin 
       range0=[range[0],range[1]]
       range1=[range[2],range[3]]
       break
    end
    else: message,'Wrong number of elements for range (must 4)'
 endcase

; Select particles to use based on itype
@itype
index=where(*data.itype_ptr ne itype_none)
if n_elements(itype) ne 0 then begin
    if itype lt itype_low or itype gt itype_high then message,'Invalid itype'
    index=where(*data.itype_ptr eq itype)
end

if n_elements(psym) eq 0 then psym=3

x=(*data.position_ptr)[0,index] & xtitle='x' & xrange=range0
y=(*data.position_ptr)[1,index] & ytitle='y' & yrange=range1

if not keyword_set(overplot) then $
   plot,x,y,/isotropic,psym=psym,$
        xminor=-1,xrange=xrange,xstyle=1,xtickinterval=1,xtitle=xtitle,$
        yminor=-1,yrange=yrange,ystyle=1,ytickinterval=1,ytitle=ytitle $
else $
   oplot,x,y,psym=psym

end
