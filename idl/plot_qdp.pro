; Script to plot qdp file

qdp=read_qdp('d0171',nstep=2000)

energy=qdp.ke+qdp.the+qdp.pe

start_plot

time=qdp.time
;time=findgen(n_elements(qdp.time))
;plot,time,energy,/ylog,xtitle='time',ytitle='energy',xrange=[800,820]
plot,time,energy,xtitle='time',ytitle='energy'
oplot,time,qdp.ke,color=2
oplot,time,qdp.the,color=3
oplot,time,qdp.pe,color=4
oplot,time,qdp.de,color=5
oplot,time,-qdp.de,color=5,linestyle=2
oplot,time,qdp.error,color=6


