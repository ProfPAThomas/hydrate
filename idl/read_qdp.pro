function read_qdp,file,nstep=nstep

; Reads in a hydra95 qdp file

; Set array sizes
if (n_elements(nstep) eq 0) then nstep=10000

; Check arguments
if (n_elements(file) eq 0) then message, 'read_qdp: filename not specified'

; Create arrays
step=intarr(nstep)
time=fltarr(nstep)
ke=fltarr(nstep)
the=fltarr(nstep)
pe=fltarr(nstep)
de=fltarr(nstep)
error=fltarr(nstep)

; Open data file
openr, lun, 'data/'+file+'.qdp', /get_lun

;Read in lines one at a time and process
istep=-1
line=''
words=''
while (not eof(lun) and istep lt nstep-1) do begin
    readf,lun,line
    ; reset counter if this is a header line, else add to arrays
    if (strpos(line,'!') ne -1) then istep=-1 else begin
        istep=istep+1
        words=strsplit(line,/extract)
        step[istep]=words[0]
        time[istep]=words[1]
        ke[istep]=words[2]
        the[istep]=words[3]
        pe[istep]=words[4]
        de[istep]=words[5]
        error[istep]=words[6]
    endelse
endwhile


; Close header file
free_lun,lun

; Remove unused portions of arrays
step=step[0:istep]
time=time[0:istep]
ke=ke[0:istep]
the=the[0:istep]
pe=pe[0:istep]
de=de[0:istep]
error=error[0:istep]

; Create structure
record=create_struct('nstep',istep+1,$
                     'step',step,$
                     'time',time,$
                     'ke',ke,$
                     'the',the,$
                     'pe',pe,$
                     'de',de,$
                     'error',error)


; Return record
return,record

end
