; House
theta=64*!pi/180.
ct=cos(theta)
st=sin(theta)
l1=0.4
l2=0.8
x0=[3.44, 1.75-1.8]
x1=[x0[0]+l1*st,x0[1]-l1*ct]
x2=[x1[0]+l2*ct,x1[1]+l2*st]
x3=[x2[0]-l1*st,x2[1]+l1*ct]
x4=[x3[0]-l2*ct,x3[1]-l2*st]
print,x0
print,x1
print,x2
print,x3
print,x4

; Resolution
area=3.6*99.0
nelement=100.*area
print,'nelement=',nelement
nsph=24
nparticle=nelement*nsph
print,'nparticle=',nparticle
denleft=0.4
denright=0.02
arealeft=3.6*7.55
arearight=area-arealeft
nparticleleft=nparticle*arealeft*denleft/(arealeft*denleft+arearight*denright)
nparticleright=nparticle-nparticleleft
particleperbox=512
nboxleft=nparticleleft/float(particleperbox)
print,'nboxleft=',nboxleft
boxleft=sqrt(arealeft/nboxleft)
print,'boxleft=',boxleft
print,'iboxleft=',[3.6,7.55]/boxleft
nboxright=nparticleright/float(particleperbox)
print,'nboxright=',nboxright
boxright=sqrt(arearight/nboxright)
print,'boxright=',boxright
print,'iboxright=',[3.6,99.-7.55]/boxright

; Floor  and edge correction
ymid=1.80-0.34
print,'binsize=',2*ymid
slope=0.155/0.34
print,'slope=',slope
print,'slope*binsize=',slope*2*ymid
; Where we need to start to end up at 1.80
Ymaxleft=1.80-0.5*(slope/denleft)*0.34^2
print,'Ymaxleft=',Ymaxleft
; Where we need to start to end up at 1.80
Ymaxright=1.80-0.34+denright/(2*slope)
print,'Ymaxright=',Ymaxright
