function pmtest,p,pl,pr,gamma,musq,cl,cr

lhs=(p/pr-1.)*sqrt((1.-musq)/gamma/(p/pr+musq))
rhs=2.*cl/(gamma-1.)/cr*(1.-(p/pl)^(0.5*(gamma-1.)/gamma))

return,lhs-rhs

end

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

function tabulate_sod,time,rhol=rhol,rhor=rhor,pl=pl,pr=pr,gamma=gamma

; Tabulate the analytic solution to the Sod shock.
; Adapted from fortran code by Steve Hopton and Frazer Pearce
; (University of Nottingham).
;
if n_elements(time) eq 0 then message,$
    'usage: sod=sod(time,rhol=rhol,rhor=rhor,pl=pl,pr=pr,gamma=gamma)'
if n_elements(rhol) eq 0 then rhol=4.
if n_elements(rhor) eq 0 then rhor=1.
if n_elements(pl) eq 0 then pl=1.
if n_elements(pr) eq 0 then pr=0.1795
if n_elements(gamma) eq 0 then gamma=5./3.

musq=(gamma-1.)/(gamma+1.)
cl=sqrt(gamma*pl/rhol)
cr=sqrt(gamma*pr/rhor)

; bisection to get pm
tol=1.e-5
pu=pl
pd=pr
while (abs(pu-pd) gt tol) do begin
    pm=(pu+pd)/2.
    pmt=pmtest(pm,pl,pr,gamma,musq,cl,cr)
    if (pmt lt 0.) then pd=pm
    if (pmt gt 0.) then pu=pm      
endwhile

rhoml=rhol*(pm/pl)^(1./gamma)
vm=2.*cl/(gamma-1.)*(1.-(pm/pl)^(0.5*(gamma-1.)/gamma))
vt=cl-vm/(1.-musq)
rhomr=rhor*(pm+musq*pr)/(pr+musq*pm)
vs=vm/(1.-rhor/rhomr)

pos=fltarr(108) & den=fltarr(108) & pre=fltarr(108) & vel=fltarr(108)

pos[0]=-100. & den[0]=rhol & pre[0]=pl & vel[0]=0.
pos[1]=-cl*time & den[1]=rhol & pre[1]=pl & vel[1]=0.
for i=1,100 do begin
    xc=-(cl+0.01*i*(vt-cl))*time
    f=xc/(cl*time)
    rhoc=rhol*((1.-musq)-musq*f)^(2./(gamma-1.))
    pc=pl*((1.-musq)-musq*f)^(2.*gamma/(gamma-1.))
    vc=cl*(1.-musq)*(1.+f)
    j=i+1
    pos[j]=xc & den[j]=rhoc & pre[j]=pc & vel[j]=vc
endfor
pos[102]=-vt*time & den[102]=rhoml & pre[102]=pm & vel[102]=vm
pos[103]= vm*time & den[103]=rhoml & pre[103]=pm & vel[103]=vm
pos[104]= vm*time & den[104]=rhomr & pre[104]=pm & vel[104]=vm
pos[105]= vs*time & den[105]=rhomr & pre[105]=pm & vel[105]=vm
pos[106]= vs*time & den[106]=rhor  & pre[106]=pr & vel[106]=0.
pos[107]= 100. & den[107]=rhor  & pre[107]=pr & vel[107]=0.

tem=1.5*pre/den
ent=pre/den^(5./3.)

; Create structure
record=create_struct(name='sod',$
                     'position',pos,$
                     'density',den,$
                     'pressure',pre,$
                     'temperature',tem,$
                     'entropy',ent,$
                     'velocity',vel)

return,record

end

