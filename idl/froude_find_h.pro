 ;;This procedure and function are used to solve for the root of the
;;cubic =(2*H_s/Froude-1)*(1+H_s-H0_s)^2+1

;Function used by fx_root, shares variables with find_h. g is
;gravitational acceleration
FUNCTION finding, H_s
common heightshare, g,Froude,H0_s
  g=9.80665D
  RETURN, (2*H_s/Froude-1)*(1+H_s-H0_s)^2+1
end


;Procedure to take in the density(depth) at the start of the flow, and
;the velocity at the start of the flow, the height of the floor in an array and
;the length of in x to plot over. Then will loop through all the
;values of predefined floor height in array Floor and generates the height change,height_rel and also the final height, defined predictably under height.

pro froude_find_h,density,velocity,floor,length
common heightshare,g,Froude,H0_s
g=9.80665D
d=density                 ;depth before bump    (also shares variable)
v=velocity                ;velocity before bump (also shares variable)
Froude=v^2./(g*d)         ;Froude number calculated from depth, velocity, and g
h_s=[0.0,-d/2,d/2]              ;require initial guess vector for fx_root
height_change_s=fltarr(n_elements(floor))   ;Change in height (dimensionless)
height=fltarr(n_elements(floor))            ;Final height of surface
for i=0,n_elements(floor)-1 do begin
   H0_s=floor[i]/d
   height_change_s[i]=fx_root(h_s,'finding',/DOUBLE)
endfor
height=height_change_s*d+d


;Plotting
x=dindgen(n_elements(floor))*length/(n_elements(floor)-2)   
;The -2 is because currently the floor array is defined from 1 cell before the box we are interested
;in to 1 cell afterwards, so that no extrapolation is need for the floor surface
x=x-0.5
;Shift to reflect cells height defined at their centre

oplot,x,height,color=3,thick=3

;Debugging
;print,'height=',height                     ;for debugging
;print,'height_change_s=',height_change_s   ;for debugging
;print,'x=',x                               ;for debugging
;print,'d=',d,'v=',v                        ;for debugging


;Give's the Froude Number
print, 'Froude Number = ', Froude
end



