function read_hydra95,file,real4=real4,accel=accel,debug=debug,test=test

; Reads in a hydra95_water_2d data file.
; Returns a structure of values plus pointers to arrays.
; Note that values are returned as strings: there seems no way to
; automatically convert them to the appropriate type.

; For now, set type of flt or dbl here.
; Should really make this dynamic
flag_dbl=1b
if keyword_set(real4) then flag_dbl=0b

; Version number (really only there to allow record creation!)
version=1.0

; Check arguments
if (n_elements(file) eq 0) then message, 'read_hydra95: filename not specified'

; Create structure
record=create_struct(name='hydra95','version',version)

; Open header file
openr, lun, 'data/'+file+'.head', /get_lun

line=''
words=''
while not eof(lun) do begin
    readf,lun,line
    ; Find '=' and surround them by white space
    ipos=strpos(line,'=')
    ilen=strlen(line)
    if ipos ge 0 then line=strmid(line,0,ipos)+' = '+ $
                           strmid(line,ipos+1,ilen-ipos-1)
    words=[words,strsplit(line,' ,',/extract)]
endwhile

; Close header file
free_lun,lun

; Extract variables
; Note that no attempt here is made to handle arrays
equals=where(words eq '=')
for i=0,n_elements(equals)-1 do begin
    tag=words[equals[i]-1]
    value=words[equals[i]+1]
    record=create_struct(record,tag,value)
endfor

; Add tag for nbary if one does not exist; use character 0 for consistency
location=where(tag_names(record) eq 'NBARY')
if location lt 0 then record=create_struct(record,'nbary','0')

; Extract sizes of data arrays
nobj=long(record.nobj)
nbary=long(record.nbary)

; Open data file
openr, lun, 'data/'+file+'.data', $
  /get_lun, /f77_unformatted, /swap_if_little_endian

; Read in itype
itype=lonarr(nobj)
readu,lun,itype
itype_ptr=ptr_new(itype,/no_copy)
record=create_struct(record,'itype_ptr',itype_ptr)

; Read in mass
if flag_dbl then mass=dblarr(nobj) else mass=fltarr(nobj)
readu,lun,mass
mass_ptr=ptr_new(mass,/no_copy)
record=create_struct(record,'mass_ptr',mass_ptr)

; Read in position
if flag_dbl then position=dblarr(2,nobj) else position=fltarr(2,nobj)
readu,lun,position
position_ptr=ptr_new(position,/no_copy)
record=create_struct(record,'position_ptr',position_ptr)

; Read in velocity
if flag_dbl then velocity=dblarr(2,nobj) else velocity=fltarr(2,nobj)
readu,lun,velocity
velocity_ptr=ptr_new(velocity,/no_copy)
record=create_struct(record,'velocity_ptr',velocity_ptr)

; Read in smoothing length
if (nbary gt 0) then begin
    if flag_dbl then smooth=dblarr(nbary) else position=fltarr(nbary)
    readu,lun,smooth
    smooth_ptr=ptr_new(smooth,/no_copy)
    record=create_struct(record,'smooth_ptr',smooth_ptr)
endif else smooth_ptr=ptr_new()

; Read in density
if (nbary gt 0) then begin
    if flag_dbl then density=dblarr(nbary) else density=fltarr(nbary)
    readu,lun,density
    density_ptr=ptr_new(density,/no_copy)
    record=create_struct(record,'density_ptr',density_ptr)
endif else density_ptr=ptr_new()

if keyword_set(accel) then begin
; Read in acceleration
if flag_dbl then acceleration=dblarr(2,nobj) else acceleration=fltarr(2,nobj)
readu,lun,acceleration
acceleration_ptr=ptr_new(acceleration,/no_copy)
record=create_struct(record,'acceleration_ptr',acceleration_ptr)
endif

if keyword_set(debug) then begin
; Read in extra debugging info
if flag_dbl then debug=dblarr(nobj) else debug=fltarr(nobj)
readu,lun,debug
debug_ptr=ptr_new(debug,/no_copy)
record=create_struct(record,'debug_ptr',debug_ptr)
endif

if keyword_set(test) then begin

; Read in separation to nearest neighbour
if flag_dbl then nnsep=dblarr(nobj) else nnsep=fltarr(nobj)
readu,lun,nnsep
nnsep_ptr=ptr_new(nnsep,/no_copy)
record=create_struct(record,'nnsep_ptr',nnsep_ptr)

; Read in mean of one
if flag_dbl then onebar=dblarr(nobj) else onebar=fltarr(nobj)
readu,lun,onebar
onebar_ptr=ptr_new(onebar,/no_copy)
record=create_struct(record,'onebar_ptr',onebar_ptr)

; Read in mean of r^2
if flag_dbl then r2bar=dblarr(nobj) else r2bar=fltarr(nobj)
readu,lun,r2bar
r2bar_ptr=ptr_new(r2bar,/no_copy)
record=create_struct(record,'r2bar_ptr',r2bar_ptr)

; Read in mean of r^4
if flag_dbl then r4bar=dblarr(nobj) else r4bar=fltarr(nobj)
readu,lun,r4bar
r4bar_ptr=ptr_new(r4bar,/no_copy)
record=create_struct(record,'r4bar_ptr',r4bar_ptr)

; Read in mean velocity
if flag_dbl then vbar=dblarr(2,nobj) else vbar=fltarr(2,nobj)
readu,lun,vbar
vbar_ptr=ptr_new(vbar,/no_copy)
record=create_struct(record,'vbar_ptr',vbar_ptr)

; Read in mean velocity*r^2
if flag_dbl then vr2bar=dblarr(2,nobj) else vr2bar=fltarr(2,nobj)
readu,lun,vr2bar
vr2bar_ptr=ptr_new(vr2bar,/no_copy)
record=create_struct(record,'vr2bar_ptr',vr2bar_ptr)

endif

; Close data file
free_lun,lun

; Return record
return,record

end
