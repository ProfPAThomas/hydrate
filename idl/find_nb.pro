pro find_nb

data=read_hydra95('d0027.00010')
np=data.nobj

index=where((*data.position_ptr)[0,*] gt 1. and $
            (*data.position_ptr)[0,*] lt 2. and $
            (*data.position_ptr)[1,*] gt 1. and $
            (*data.position_ptr)[1,*] lt 2. )

seps=fltarr(np*13)

is=-1
for ip=0,n_elements(index)-1 do begin
   for jp=0,np-1 do begin 
      sep=sqrt( $
      ((*data.position_ptr)[0,jp]-(*data.position_ptr)[0,index[ip]])^2 + $
      ((*data.position_ptr)[1,jp]-(*data.position_ptr)[1,index[ip]])^2 ) / $
      (*data.smooth_ptr)[ip]
      if sep lt 1. and jp ne ip then begin
         is=is+1
         seps[is]=sep
      endif
   endfor
endfor
seps=seps[0:is]
print,is

hist_plot,seps,min=0.1,xtitle='neighbour separation',ytitle='number'
oplot,[1,1]*0.25,[0,250]
oplot,[1,1]*0.5/sqrt(3.),[0,250],linestyle=2
oplot,[1,1]*0.5,[0,250]

end

