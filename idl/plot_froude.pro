FUNCTION findingf, H_s
common Froudesharing, g,F,H0_s
  g=9.80665D
  RETURN, (2*H_s/F-1)*(1+H_s-H0_s)^2+1
end

pro plot_froude,density,Froude,floor,length
common Froudesharing,g,F,H0_s
g=9.80665D
d=density                 ;depth before bump    (also shares variable)
F=Froude
h_s=[0.0,-1./10.,1./10.]              ;require initial guess vector for fx_root
height_change_s=fltarr(n_elements(floor))   ;Change in height (dimensionless)
height=fltarr(n_elements(floor))            ;Final height of surface
for i=0,n_elements(floor)-1 do begin
   H0_s=floor[i]/d
   height_change_s[i]=fx_root(h_s,'findingf',/DOUBLE)
endfor
height=height_change_s*d+d

;Plotting
x=dindgen(n_elements(floor))*length/(n_elements(floor)-2)   
;The -2 is because currently the floor array is defined from 1 cell before the box we are interested
;in to 1 cell afterwards, so that no extrapolation is need for the floor surface
x=x-0.5
;Shift to reflect cells height defined at their centre

plot,x,height,color=0,thick=3,xrange=[0,length],xtitle='x', $
     ytitle='Height of Water Surface', Title='Froude='+string(F)

;Debugging
;print,'height=',height                     ;for debugging
;print,'height_change_s=',height_change_s   ;for debugging
;print,'x=',x                               ;for debugging
print,'d=',d,' Froude=',F                   ;for debugging


end
