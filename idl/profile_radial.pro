pro profile_radial,data,properties,$
            centre=centre,$
            range=range,n_per_bin=n_per_bin,itype=itype,$
            xlog=xlog,ylog=ylog,overplot=overplot,psym=psym

; Plots a linear profile of chosen data properties
; data: hydra95 data structure
; properties: string containing a list of one or more properties to plot
;             (see message below for a list)
; centre (optional): centre of radial binning, defaults to origin
; range (optional): range over which to plot profile
; n_per_bin (optional): number of particles to use for smoothing
;             (defaults to 17; rounds even numbers up to next odd one)
; itype (optional): use only particles of this itype (defaults to all)
; xlog (optional): flag to decide whether to plot logarithmically
; ylog (optional): flag to decide whether to plot logarithmically
; overplot (optional): flag to determine whether plot is new or overplotted
; psym (optional): plot points as symbols on top of line

if n_elements(properties) eq 0 then begin
    property_error:
    print,'Incorrect property list.  Must choose one or more of:'
    ;print,'    a - energy (thermal + kinetic) ratio dm/gas'
    ;print,'    b - beta: total energy dm/thermal energy gas'
    print,'    d - density (from SPH)'
    print,'    e - energy (thermal + kinetic)'
    ;print,'    F - cumulative baryon fraction'
    ;print,'    G - cumulative gas fraction'
    print,'    h - smoothing length (from SPH)'
    ;print,'    k - kinetic energy'
    ;print,'    l - emissivity'
    ;print,'    L - cumulative luminosity'
    print,'    m - density (from mass/volume)'
    ;print,'    M - cumulative mass'
    ;print,'    p - thermal pressure'
    ;print,'    P - predicted mass from thermal pressure'
    ;print,'    q - dynamical pressure'
    ;print,'    Q - predicted mass from dynamical pressure'
    print,'    s - entropy'
    print,'    t - temperature (thermal energy)'
    ;print,'    x - x-ray temperature (emission-weighted)'
    print,'    v - velocity in projection direction'
    message,'usage: profile,data,properties,axis=axis,range=range,n_per_bin=n_per_bin,itype=itype'
end

; Determine radial position of particles
case n_elements(centre) of
   0: centre=[0.,0.,0.]
   3: 
   else: message,'Wrong number of elements for centre (must be 0 or 3)'
endcase
radius=sqrt(total(((*data.position_ptr)[*,*]-centre#(fltarr(data.nobj)+1))^2,1))

; Check range.  For case where not defined it is set below after
; sorting of radius
case n_elements(range) of
    0: 
    2: 
    else: message,'Wrong number of elements for range (must be 0 or 2)'
endcase

; Number of particles per bin
if n_elements(n_per_bin) eq 0 then n_per_bin=17
if n_per_bin ne round(n_per_bin) then message,'n_per_bin should be an odd integer'
if n_per_bin lt 1 then message,'Need at least 1 particle per bin (defaults to 17)'
n_per_half_bin=n_per_bin/2
n_per_bin=2*n_per_half_bin+1

; Select particles to use based on itype
@itype
index=where(*data.itype_ptr ne itype_none)
if n_elements(itype) ne 0 then begin
    if itype lt itype_low or itype gt itype_high then message,'Invalid itype'
    index=where(*data.itype_ptr eq itype)
end

; Set flag to determine whether to overplot
flag_overplot=0b
if keyword_set(overplot) then flag_overplot=1b

; Sort particles along profile direction
index=index[sort(radius[index])]

; Define range for profile if not already set
case n_elements(range) of
    0: range=[radius[index[n_per_half_bin]],$
              radius[index[n_elements(index)-n_per_half_bin-1]]]
    2:
endcase

; Determine the x-values
x=radius[index]

; Extract desired data (y-values)
y=fltarr(n_elements(index))
case properties[0] of
    'd': begin
        ytitle='SPH density'
        index_data=where(index lt n_elements(*data.density_ptr),n_data,$
                         complement=index_nodata,ncomplement=n_nodata)
        if n_data gt 0 then y[index_data]=$
          (*data.density_ptr)[index[index_data]]
        if n_nodata gt 0 then begin
            print,'Warning: property not defined for some data points'
            y[index_nodata]=!VALUES.F_NAN
        endif
    end
    'e': begin
        ytitle='total energy'
        y=0.5*total((*data.velocity_ptr)[*,index]^2,1)
        index_data=where(index lt n_elements(*data.density_ptr),n_data,$
                         complement=index_nodata,ncomplement=n_nodata)
        if n_data gt 0 then y[index_data]=y[index_data]+$
          (*data.temperature_ptr)[index[index_data]]
    end
    'h': begin
        ytitle='SPH smoothing length'
        index_data=where(index lt n_elements(*data.smooth_ptr),n_data,$
                         complement=index_nodata,ncomplement=n_nodata)
        if n_data gt 0 then y[index_data]=$
          (*data.smooth_ptr)[index[index_data]]
        if n_nodata gt 0 then begin
            print,'Warning: property not defined for some data points'
            y[index_nodata]=!VALUES.F_NAN
        endif
    end
    'm': begin
        ytitle='M/V density'
        ; To get correct normalisation omit central mass
        for i=long(n_per_half_bin),n_elements(y)-n_per_half_bin-1 do begin
            mass=total((*data.mass_ptr)[index[i-n_per_half_bin:i+n_per_half_bin]])$
              -(*data.mass_ptr)[index[i]]
            y[i]=3.*mass/(4.*!PI*(x[i+n_per_half_bin]^3-x[i-n_per_half_bin]^3))
        end
        y[0:n_per_half_bin-1]=y[n_per_half_bin]
        y[n_elements(y)-n_per_half_bin:n_elements(y)-1]=$
          y[n_elements(y)-n_per_half_bin-1]
    end
    'p': begin
        ytitle='pressure'
        index_data=where(index lt n_elements(*data.density_ptr),n_data,$
                         complement=index_nodata,ncomplement=n_nodata)
        if n_data gt 0 then y[index_data]=(2./3.)*$
          (*data.temperature_ptr)[index[index_data]]*$
          (*data.density_ptr)[index[index_data]]
        if n_nodata gt 0 then begin
            print,'Warning: property not defined for some data points'
            y[index_nodata]=!VALUES.F_NAN
        endif
    end
    's': begin
        ytitle='entropy'
        index_data=where(index lt n_elements(*data.density_ptr),n_data,$
                         complement=index_nodata,ncomplement=n_nodata)
        if n_data gt 0 then y[index_data]=$
          (*data.temperature_ptr)[index[index_data]]/$
          (*data.density_ptr)[index[index_data]]^(2./3.)
        if n_nodata gt 0 then begin
            print,'Warning: property not defined for some data points'
            y[index_nodata]=!VALUES.F_NAN
        endif
    end
    't': begin
        ytitle='mass-weighted temperature'
        index_data=where(index lt n_elements(*data.density_ptr),n_data,$
                         complement=index_nodata,ncomplement=n_nodata)
        if n_data gt 0 then y[index_data]=$
          (*data.temperature_ptr)[index[index_data]]
        if n_nodata gt 0 then begin
            print,'Warning: property not defined for some data points'
            y[index_nodata]=!VALUES.F_NAN
        endif
    end
    'v': begin
        ytitle='velocity'
        y=total((*data.velocity_ptr)[*,index]*(*data.position_ptr)[*,index],1)$
          /radius[index]
    end
    else: goto,property_error
endcase

; Smooth data and throw away unsmoothed bins
x=smooth(x,n_per_bin)
x=x[n_per_half_bin:n_elements(x)-n_per_half_bin-1]
; Note: m is already smoothed---no need to do it again
case properties[0] of
    'm':
    else: begin
; Note: for weighted data could do y=yw/w where yw is data*weight and
; w is weight, both smoothed as below.
        y=smooth(y,n_per_bin,/nan)
    end
endcase
y=y[n_per_half_bin:n_elements(y)-n_per_half_bin-1]

; Initialise plot
if flag_overplot then $
    colour=2 $
else begin
    window,xsize=800,ysize=600
    start_plot
    plot,x,y,/nodata,xlog=xlog,ylog=ylog,$
      xtitle=xtitle,ytitle=ytitle,xrange=range,xstyle=1
    colour=!p.color
endelse

; Plot y-values
if n_elements(psym) gt 0 then begin
    npoint=n_elements(x)/n_per_bin
    index=n_per_bin*indgen(npoint)+n_per_bin/2
    oplot,x[index],y[index],color=colour,psym=psym
endif else $
  oplot,x,y,color=colour,linestyle=0

end
