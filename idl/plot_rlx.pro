; Script to plot rlx file

rlx=read_ascii('data/d0064.rlx',data_start=1)
;istep    time     dn_min     dn_max     v_max    
istep=nint((rlx.(0))[0,*])
time=(rlx.(0))[1,*]
dn_min=(rlx.(0))[2,*]
dn_max=(rlx.(0))[3,*]
v_max=(rlx.(0))[4,*]

plot,istep,v_max,yrange=[0.,0.05]
oplot,istep,(dn_max-dn_min)/(dn_max+dn_min),color=2
