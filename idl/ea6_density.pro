slope=0.155/0.34
;data=read_hydra95('d0160.00002')
;data=read_hydra95('d0162.00537')
;data=read_hydra95('d0162.back1')
data=read_hydra95('d0164.02474')

dx=0.1
xmin=-2.
xmax=8.
;xmin=-2.
;xmax=12.
ymin=-1.8
ymax=1.8

; Apply floor correction
index1=where((*data.position_ptr)[1,*] le -1.46)
(*data.density_ptr)[index1]+=(-1.46-(*data.position_ptr)[1,index1])*slope
index2=where((*data.position_ptr)[1,*] gt 1.46)
(*data.density_ptr)[index2]+=((*data.position_ptr)[1,index2]-1.46)*slope

; Create density array
nxbin=floor((xmax-xmin)/dx)
nybin=floor((ymax-ymin)/dx)
num=intarr(nxbin,nybin)
den=fltarr(nxbin,nybin)

; Populate array
index=where((*data.position_ptr)[0,*] gt xmin and $
            (*data.position_ptr)[0,*] lt xmax and $
            (*data.position_ptr)[1,*] gt ymin and $
            (*data.position_ptr)[1,*] lt ymax )
ix=floor(((*data.position_ptr)[0,index]-xmin)/dx)
iy=floor(((*data.position_ptr)[1,index]-ymin)/dx)
print,'nxbin,min,max=',nxbin,min(ix),max(ix)
print,'nybin,min,max=',nybin,min(iy),max(iy)
help,num
for i=0L,n_elements(index)-1 do num[ix[i],iy[i]]+=1
help,den
for i=0L,n_elements(index)-1 do den[ix[i],iy[i]]+=(*data.density_ptr)[index[i]]
index=where(num gt 0)
den[index]=den[index]/num[index]
help,den

x=xmin+(findgen(nxbin)+0.5)*dx
y=ymin+(findgen(nybin)+0.5)*dx
denmax=1.05*max(den)
den[where(den le 0.001)]=denmax
denmin=min(den)
scaledImage = BytScl(alog10(den), MIN=alog10(denmin), MAX=alog10(denmax))
;scaledImage = BytScl(den, MIN=denmin, MAX=denmax)
;scaledImage[where(scaledImage eq 0)]=255
cgLoadCT, 39
cgImage, scaledImage, /Keep_Aspect, position =   [0.125, 0.2, 0.9, 0.875],/axes, xrange=[xmin,xmax],yrange=[ymin,ymax]
; Draw the color bar. Fit it to the location of the image.  Note have rescaled
; the color table to omit the last, white point.
cgColorbar, Position= [0.125, 0.825, 0.9, 0.85], Range=[alog10(denmin),alog10(denmax)], $
      Title='log!d10!n(height/m)', TLocation='Top',ncolors=255

set_basic_colours
;contour,den,x,y,/isotropic,/fill,
