pro start_plot,thick=thick,size=size
; Defines plotting symbol/character size and thickness

; Check arguments and set defaults
if (n_elements(thick) eq 0) then begin
    thick=1.
    if !d.name eq 'X' then thick=1
    if !d.name eq 'PS' then thick=3
endif
if (n_elements(size) eq 0) then size=1.5

; Redfine system variables
!p.thick=thick
!p.charthick=thick
!x.thick=thick
!y.thick=thick
!z.thick=thick
!p.symsize=size
!p.charsize=size

end
