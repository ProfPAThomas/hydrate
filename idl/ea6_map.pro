slope=0.155/0.34
data=read_hydra95('d0160.00000')
data=read_hydra95('d0163.back0')
;xmin=-7.55
;xmax=0.
xmin1=-6.
xmax1=-2.
xmin2=50.
xmax2=54.

; Profiles across flow
window,0

; Apply floor correction
index1=where((*data.position_ptr)[1,*] le -1.46)
(*data.density_ptr)[index1]+=(-1.46-(*data.position_ptr)[1,index1])*slope
index2=where((*data.position_ptr)[1,*] gt 1.46)
(*data.density_ptr)[index2]+=((*data.position_ptr)[1,index2]-1.46)*slope

; Isolate example regions
(*data.itype_ptr)[where((*data.position_ptr)[0,*] gt xmin1 and $
                        (*data.position_ptr)[0,*] lt xmax1)]=-1
profile_linear,data,'d',n_per_bin=1,/psym,axis=2,itype=-1,$
               color=4,range=[-2,2],yrange=[0,0.03];0.45],/isotropic
(*data.itype_ptr)[where((*data.position_ptr)[0,*] gt xmin2 and $
                        (*data.position_ptr)[0,*] lt xmax2)]=-2
profile_linear,data,'d',n_per_bin=1,/psym,axis=2,itype=-2,/overplot

profile_linear,data,'v',range=[xmin2,xmax2],n_per_bin=1,/psym,itype=-2
index2=where((*data.itype_ptr)[*] eq -2)
index3=index2[where((*data.position_ptr)[1,index2] lt -1.459) or ((*data.position_ptr)[1,index2] gt 1.459)]
(*data.itype_ptr)[index3]=-3
profile_linear,data,'v',n_per_bin=1,/psym,itype=-3,/overplot,color=2

; Plot  Floor and walls
oplot,[-1.8,-1.8,-1.46,1.46,1.8,1.8],[0.45,0.245,0.,0.,0.245,0.45]



; Map of density
window,1

profile_linear,data,'h',n_per_bin=1,/psym,axis=2,itype=-2,$
               color=2,range=[-2,2]
profile_linear,data,'h',n_per_bin=1,/psym,axis=2,itype=-1,/overplot,color=4

;; symbols,2,0.01
;; plot_2d,data,range=[-5,5,-2,2],psym=8
;; symbols,2,0.1,color=2
;; (*data.itype_ptr)[*]=-1
;; (*data.itype_ptr)[where((*data.density_ptr)[*] gt 0.39 and (*data.density_ptr) lt 4.01)]=1
;; plot_2d,data,/overplot,psym=8


