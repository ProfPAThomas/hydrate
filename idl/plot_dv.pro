; Script to plot difference between particle speed and the mean speed
; in the neighbourhood.

data=read_hydra95('d0026.02000',/test)

dv=sqrt($
   ((*data.velocity_ptr)[0,*]-(*data.vbar_ptr)[0,*])^2+$
   ((*data.velocity_ptr)[1,*]-(*data.vbar_ptr)[1,*])^2 )
vbar=sqrt((*data.vbar_ptr)[0,*]^2+(*data.vbar_ptr)[1,*]^2)
v=sqrt((*data.velocity_ptr)[0,*]^2+(*data.velocity_ptr)[1,*]^2)
index=where(*data.nnsep_ptr lt 0.)

hist_plot,v,xtitle='|v|',ytitle='number'
hist_plot,v[index],/overplot,color=2
pause

plot,v,abs(*data.nnsep_ptr),/psym,xtitle='|v|',ytitle='nnsep'
oplot,v[index],abs((*data.nnsep_ptr)[index]),/psym,color=2
pause

plot,v,*data.smooth_ptr,/psym,xtitle='|v|',ytitle='h'
oplot,v[index],(*data.smooth_ptr)[index],/psym,color=2
pause

plot,dv,vbar,xtitle='|v-<v>|',ytitle='|<v>|',/psym
oplot,dv[index],vbar[index],/psym,color=2
pause

plot,dv,v,xtitle='|v-<v>|',ytitle='|v|',/psym
oplot,dv[index],v[index],/psym,color=2

;plot,dv/vbar,*data.nnsep_ptr,/psym,xtitle='|v-<v>|/|<v>|',ytitle='nnsep'

