;open_ps,'PEC.ps'
;open_ps,'PECE.ps'
open_ps,'viscosity.ps'
start_plot,size=1.
;plot,data3.time,data3.ke,color=0,xtitle='time',ytitle='KE',/nodata
;oplot,data3.time,data3.ke,color=2
;oplot,data4.time,data4.ke,color=3
;oplot,data5.time,data5.ke,color=4
;plot,data7.time,data9.ke,color=0,xtitle='time',ytitle='KE',/nodata
;oplot,data7.time,data7.ke,color=2
;oplot,data8.time,data8.ke,color=3
;oplot,data9.time,data9.ke,color=4
;oplot,data6.time,data6.ke,color=5,linestyle=2
plot,data20.time,data20.ke,color=0,xtitle='time',ytitle='KE',/nodata
oplot,data20.time,data20.ke,color=2
oplot,data21.time,data21.ke,color=4
oplot,data22.time,data22.ke,color=3
;xyouts,0.2,180,'alpha=0,   dta/dt=0.25,  PEC',color=2
;xyouts,0.2,170,'alpha=0.5, dta/dt=0.25,  PEC',color=3
;xyouts,0.2,160,'alpha=1,   dta/dt=0.25,  PEC',color=4
;xyouts,0.2,180,'alpha=1/2,   dta/dt=0.25,  PECE',color=2
;xyouts,0.2,170,'alpha=2/3, dta/dt=0.25,  PECE',color=3
;xyouts,0.2,160,'alpha=1,   dta/dt=0.25,  PECE',color=4
;xyouts,0.2,150,'alpha=0,   dta/dt=0.125, PEC',color=5
xyouts,0.3,14,'No viscosity',color=2
xyouts,0.3,12,'Actual viscosity of water',color=3
xyouts,0.3,10,'Artificial viscosity',color=4
close_ps
