pro depth_ea3

depthfile='data/d0008.ea3'
; floor depth at measured points
depth=9.7525
fmt='L,F,F,F,F,F'
readcol,depthfile,istep,timer7,dn1r7,dn2r7,v1r7,v2r7,format=fmt,skipline=1,/silent

;; depthfile='/Users/cs266/My_SPH_water_2D/hydra95_water_2d/data/run8/d0008.ea3'
;; fmt='L,F,F,F,F,F'
;; readcol,depthfile,istep,timer8,dn1r8,dn2r8,v1r8,v2r8,format=fmt,skipline=1,/silent

;; depthfile='/Users/cs266/My_SPH_water_2D/hydra95_water_2d/data/run9/d0009.ea3'
;; fmt='L,F,F,F,F,F'
;; readcol,depthfile,istep,timer9,dn1r9,dn2r9,v1r9,v2r9,format=fmt,skipline=1,/silent

;; depthfile='/Users/cs266/My_SPH_water_2D/hydra95_water_2d/data/run10/d0010.ea3'
;; fmt='L,F,F,F,F,F'
;; readcol,depthfile,istep,timer10,dn1r10,dn2r10,v1r10,v2r10,format=fmt,skipline=1,/silent

;; depthfile='/Users/cs266/My_SPH_water_2D/hydra95_water_2d/data/run11/d0011.ea3'
;; fmt='L,F,F,F,F,F'
;; readcol,depthfile,istep,timer11,dn1r11,dn2r11,v1r11,v2r11,format=fmt,skipline=1,/silent

;; depthfile='/Users/cs266/My_SPH_water_2D/hydra95_water_2d/data/run12/d0012.ea3'
;; fmt='L,F,F,F,F,F'
;; readcol,depthfile,istep,timer12,dn1r12,dn2r12,v1r12,v2r12,format=fmt,skipline=1,/silent

;; depthfile='/Users/cs266/My_SPH_water_2D/hydra95_water_2d/data/run13/d0013.ea3'
;; fmt='L,F,F,F,F,F'
;; readcol,depthfile,istep,timer13,dn1r13,dn2r13,v1r13,v2r13,format=fmt,skipline=1,/silent

;; depthfile='/Users/cs266/My_SPH_water_2D/hydra95_water_2d/data/run14/d0014.ea3'
;; fmt='L,F,F,F,F,F'
;; readcol,depthfile,istep,timer14,dn1r14,dn2r14,v1r14,v2r14,format=fmt,skipline=1,/silent

;; depthfile='/Users/cs266/My_SPH_water_2D/hydra95_water_2d/data/run16/d0016.ea3'
;; fmt='L,F,F,F,F,F'
;; readcol,depthfile,istep,timer16,dn1r16,dn2r16,v1r16,v2r16,format=fmt,skipline=1,/silent

;; ;Open plot for depth at point 1
;; file='/Users/cs266/My_SPH_water_2D/hydra95_water_2d/plots/EA3_dn1.ps'
;; file=strcompress(file,/remove_all)
;; entry_device=!d.name
;; set_plot,'ps'
;; device,filename=file,/color,bits_per_pixel=8,ysize=15
;; loadcolours
window, 1

;Axes
plot,[0.0,900.0],[9.7,10.05],/nodata,xstyle=1,ystyle=1,xtitle='t [s]',ytitle='Water level [m]',charsize=1.4

;Plot data
;Resolution test
oplot,timer7,depth+dn1r7,color=0
;oplot,timer8,dn1r8,color=0,linestyle=1
;oplot,timer9,dn1r9,color=0,linestyle=2
;oplot,timer14,dn1r14,color=0,linestyle=3

;Nsph test
;oplot,timer10,dn1r10,color=4,linestyle=0
;oplot,timer11,dn1r11,color=4,linestyle=1

;dt test
;oplot,timer12,dn1r12,color=5,linestyle=0

;stopping inlet test
;oplot,timer16,dn1r16,color=6,linestyle=0

;Close plot
;device,/close_file
;set_plot,entry_device

;Open plot for depth at point 2
;file='/Users/cs266/My_SPH_water_2D/hydra95_water_2d/plots/EA3_dn2.ps'
;file=strcompress(file,/remove_all)
;entry_device=!d.name
;set_plot,'ps'
;device,filename=file,/color,bits_per_pixel=8,ysize=15
;loadcolours
window,2

;Axes
plot,[0.0,900.0],[9.74,9.86],/nodata,xstyle=1,ystyle=1,xtitle='t [s]',ytitle='Water level [m]',charsize=1.4

;Plot data
;Resolution test
oplot,timer7,depth+dn2r7,color=0
;oplot,timer8,dn2r8,color=0,linestyle=1
;oplot,timer9,dn2r9,color=0,linestyle=2
;oplot,timer14,dn2r14,color=0,linestyle=3

;Nsph test
;oplot,timer10,dn2r10,color=4,linestyle=0
;oplot,timer11,dn2r11,color=4,linestyle=1

;dt test
;oplot,timer12,dn2r12,color=5,linestyle=0

;stopping inlet test
;oplot,timer16,dn2r16,color=6,linestyle=0

;Close plot
;device,/close_file
;set_plot,entry_device

;Open plot for velocity at point 1
;file='/Users/cs266/My_SPH_water_2D/hydra95_water_2d/plots/EA3_v1.ps'
;file=strcompress(file,/remove_all)
;entry_device=!d.name
;set_plot,'ps'
;device,filename=file,/color,bits_per_pixel=8,ysize=15
;loadcolours
window,3

;Axes
plot,[0.0,900.0],[-0.5,3.0],/nodata,xstyle=1,ystyle=1,xtitle='t [s]',ytitle='Velocity [m/s]',charsize=1.4

;Plot data
;Resolution test
oplot,timer7,v1r7,color=0
;oplot,timer8,v1r8,color=0,linestyle=1
;oplot,timer9,v1r9,color=0,linestyle=2
;oplot,timer14,v1r14,color=0,linestyle=3

;Nsph test
;oplot,timer10,v1r10,color=4,linestyle=0
;oplot,timer11,v1r11,color=4,linestyle=1

;dt test
;oplot,timer12,v1r12,color=5,linestyle=0

;stopping inlet test
;oplot,timer16,v1r16,color=6,linestyle=0

;Close plot
;device,/close_file
;set_plot,entry_device

;Open plot for velocity at point 2
;file='/Users/cs266/My_SPH_water_2D/hydra95_water_2d/plots/EA3_v2.ps'
;file=strcompress(file,/remove_all)
;entry_device=!d.name
;set_plot,'ps'
;device,filename=file,/color,bits_per_pixel=8,ysize=15
;loadcolours
window,4

;Axes
plot,[0.0,900.0],[-0.5,1.5],/nodata,xstyle=1,ystyle=1,xtitle='t [s]',ytitle='Velocity [m/s]',charsize=1.4

;Plot data
;Resolution test
oplot,timer7,v2r7,color=0
;oplot,timer8,v2r8,color=0,linestyle=1
;oplot,timer9,v2r9,color=0,linestyle=2
;oplot,timer14,v2r14,color=0,linestyle=3

;Nsph test
;oplot,timer10,v2r10,color=4,linestyle=0
;oplot,timer11,v2r11,color=4,linestyle=1

;dt test
;oplot,timer12,v2r12,color=5,linestyle=0

;stopping inlet test
;oplot,timer16,v2r16,color=6,linestyle=0

;Close plot
;device,/close_file
;set_plot,entry_device
end

