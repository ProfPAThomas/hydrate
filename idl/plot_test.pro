; Script to plot pressure test

; Read in end-state and plot

data=read_hydra95('d0001.0031')
profile_linear,data,axis=3,'p',range=[-2,2]

; Overplot sod theoretical profile

.comp sod

sod=tabulate_sod(2.)
oplot,sod.position,sod.pressure
