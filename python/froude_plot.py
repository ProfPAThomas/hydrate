def froude_plot(data,surface,xlims=[],color='m',psym='-'):
    import numpy as np
    import copy as cp
    import scipy.optimize as sco
    import matplotlib.pyplot as py
    index=np.argsort(data.pos[:,0])
    tmpdata=cp.deepcopy(data)


    for i in xrange (0,len(index)):
        tmpdata.density[index[i]]+=surface.surface_height(
                                                     tmpdata.pos[index[i],:])

    d=np.average(tmpdata.density[index[:200]])
    v=np.average(tmpdata.vel[index[:200],0])

    g=9.80665
    
    Froude=v**2./(g*d)
    
    floor=surface.cell[:,0]

    x=(np.arange(len(floor))+1)*surface.cellsize+surface.x0

    
    
    Height_change_s=np.zeros(len(floor))
    Height_fin=np.zeros(len(floor))

    def func(H_s):
        return (2*H_s/Froude-1)*(1+H_s-H0_s)**2+1

    for i in range(0,len(floor)):
        H0_s=floor[i]/d
        Height_change_s[i]=sco.fsolve(func,0.0)
    
    print d,v,Froude
    Height_fin=Height_change_s*d+d
    
    py.plot(x,Height_fin,color+psym,label='Froude= '+str(np.round(Froude,decimals=3)),lw=2.)
