class wall():
    """Class to read in a wall file and store the data within the object. Information is stored within arrays, where the zeroth element of each array refers to the first wall, and the nth element refers to the nth+1 wall."""

    def read(self,file):
        import numpy as np
        import sys

        #Read in file

        walls=open('../rundir/data/'+file,mode='r')
        wall_lines=walls.readlines()
        walls.close()

        # Get point 1 coords, point 2 coords and thickness for each wall

        i=0
        
        self.xpoint1=[]
        self.ypoint1=[]
        self.xpoint2=[]
        self.ypoint2=[]
        self.thickness=[]

        for line in wall_lines:
            if not '!' in line:
                words=line.split( )
                self.xpoint1.append(words[0])
                self.ypoint1.append(words[1])
                self.xpoint2.append(words[2])
                self.ypoint2.append(words[3])
                self.thickness.append(words[4])
                i+=1
        
        self.wallnumber=i
