# Import modules

import numpy as np
import matplotlib.pyplot as py
from datafile import *
from qdpdata import *
from profile_linear import *

# Read data

qdp=qdpdata()
qdp.read('d0001')
data1=data()
data1.read('d0001.t0001')
data2=data()
data2.read('d0001.t0020')
data3=data()
data3.read('d0001.t0040')
data4=data()
data4.read('d0001.t0060')
data5=data()
data5.read('d0001.t0080')
data6=data()
data6.read('d0001.t0100')
nobj=data1.nobj

# Plot actual speed vs theoretical speed

py.figure()
vterm=5.
tau=50.
t=qdp.time
v=np.sqrt(2*qdp.ke/(nobj*data1.mass[0]))

py.plot(t,v,color='blue')
py.yscale('log')
py.xscale('log')
py.ylim(0.1,10)
py.xlim(1,1000)
py.xlabel('time')
py.ylabel('speed')
py.plot([1.**-10.,1000],[5,5],'k--')
py.plot(t,vterm*np.tanh(t/tau),'r-')
py.title('Manning vt')

#Plot of velocity in x direction along profile length

profile_linear(data1,'v',color='k',lgndlabel='t0001')
profile_linear(data2,'v',color='r',overplot='True',lgndlabel='t0020')
profile_linear(data3,'v',color='g',overplot='True',lgndlabel='t0040')
profile_linear(data4,'v',color='b',overplot='True',lgndlabel='t0060')
profile_linear(data5,'v',color='y',overplot='True',lgndlabel='t0080')
profile_linear(data6,'v',color='m',overplot='True',lgndlabel='t0100')

py.title('Velocity profile of test d0001')
py.legend()
py.show()
