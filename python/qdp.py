# Class to handle qdp files
class data:
    """Date type (class) for Hydra95 qdp files.
    Contains methods to read in and plot the files."""
    
    def read(self,file):
        "Reads in a qdp file"
        from scipy import zeros
        # Add extension .qdp to filename if not already present
        if file.find('.qdp') == -1:
            file=file+'.qdp'
        fp=open(file,mode='r')
        # Create arrays long enough to hold data
        # Use internal variable nstep rather than self.nstep to save typing
        nstep=len(fp.readlines())
        self.step=zeros(nstep,int)
        self.time=zeros(nstep)
        self.ke=zeros(nstep)
        self.the=zeros(nstep)
        self.pe=zeros(nstep)
        self.de=zeros(nstep)
        self.error=zeros(nstep)
        # Reset file pointer and read in data
        i=-1
        fp.seek(0)
        for line in fp.readlines():
            if line.find('!') == -1:
                i=i+1
                words=line.split()
                self.step[i]=words[0]
                self.time[i]=words[1]
                self.ke[i]=words[2]
                self.the[i]=words[3]
                self.pe[i]=words[4]
                self.de[i]=words[5]
                self.error[i]=words[6]
        # Truncate arrays to actual length (omits ! lines)
        nstep=i+1
        self.nstep=nstep
        self.step=self.step[0:nstep-1]
        self.time=self.time[0:nstep-1]
        self.ke=self.ke[0:nstep-1]
        self.the=self.the[0:nstep-1]
        self.pe=self.pe[0:nstep-1]
        self.de=self.de[0:nstep-1]
        self.error=self.error[0:nstep-1]
        fp.close()

    def plot(self,prop='ke'):
        """Plots chosen propery versus time.
           Defaults to ke."""
        from pylab import plot
        if prop == 'ke':
            plot(self.time,self.ke)
        elif prop == 'the':
            plot(self.time,self.the)
        elif prop == 'pe':
            plot(self.time,self.pe)
        elif prop == 'de':
            plot(self.time,self.de)
        elif prop == 'error':
            plot(self.time,self.error)
        else:
            print('usage: plot([ke|the|pe|de|error])')
