# Class to handle hydraTE data files
class data:
    """Date type (class) for Hydra data files.
    Contains methods to read in and plot the files."""
    
    def read(self,file,accel='False',sphflr='False',test='False',debug='False',damp='False'):
        "Reads in an Hydra data file, if you have defined a compliation option, then set the corresponding argument to 'True'"
        from scipy import zeros
        import fortranfile

        # First read in header file
        # Prepend directory data/ to filename if not already present
        if file.find('../rundir/data/') == -1:
            file='../rundir/data/'+file
        # Add extension .head to filename if not already present
        file=file+'.head'
        fp=open(file,mode='r')
        lines=fp.readlines()
        fp.close()
        
        templines=[]
        for line in lines: #Create white space around equal signs
            ipos=line.find('=')
            ilen=len(line)
            if ipos>= 0:
                line=(line[:ipos]+' = ' + line[ipos+1:])
                templines.append(line)
        lines=templines
        phrases=[] # Empty list to hold phrases separated by commas
        for line in lines:
            phrases.extend(line.split(','))
        # Extract variables
        # Note that no attempt here is made to handle arrays
        self.header={} # Empty dictionary to hold header values
        for phrase in phrases:
            words=phrase.split()
            if words.count('=') == 1:
                ieq=words.index('=')
                if ieq > 0 and ieq < len(words)-1:
                    self.header[words[ieq-1].capitalize()]=words[ieq+1]
        self.nobj=int(self.header['Nobj'])

        # Now define the arrays and read in the binary data
        # Could adapt this to read in optional arrays using the information
        # extract from the header.
        file=file.replace('head','data')
        fp=fortranfile.FortranFile(file,endian='>')
        self.itype=fp.readInts()
        self.mass=fp.readReals('d')
        #Read in the two dimensional arrays, must be a better way of doing this
        temp_pos=fp.readReals('d')
        self.pos=zeros((len(temp_pos)/2.,2))
        self.pos[:,0]=temp_pos[::2]
        self.pos[:,1]=temp_pos[1::2]

        temp_vel=fp.readReals('d')
        self.vel=zeros((len(temp_pos)/2.,2))
        self.vel[:,0]=temp_vel[::2]
        self.vel[:,1]=temp_vel[1::2]

        self.smth_len=fp.readReals('d')
        self.density=fp.readReals('d')
        #Optional data
        if accel=='True':
            temp_accel=fp.readReals('d')
            self.accel=np.zeros((len(temp_accel)/2.,2))
            self.accel[:,0]=temp_accel[::2]
            self.accel[:,1]=temp_accel[1::2]
        if sphflr=='True':
            self.flr_height=fp.readReals('d')
        if debug=='True':
            self.debug=fp.readReals('d')
        if test=='True':
            self.nnsep=fp.readReals('d')
        if damp=='True':
            self.avg_1=fp.readReals('d')
            self.avg_r2=fp.readReals('d')
            self.avg_r4=fp.readReals('d')
            temp_avg_vel=fp.readReals('d')
            self.avg_vel=np.zeros((len(temp_avg_vel)/2.,2))
            self.avg_vel[:,0]=temp_avg_vel[::2]
            self.avg_vel[:,1]=temp_avg_vel[1::2]
            temp_avg_vr2=fp.readReals('d')
            self.avg_vr2=np.zeros((len(temp_avg_vr2)/2.,2))
            self.avg_vr2[:,0]=temp_avg_vr2[::2]
            self.avg_vr2[:,1]=temp_avg_vr2[1::2]
        fp.close()
