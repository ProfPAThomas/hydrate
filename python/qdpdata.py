class qdpdata():
    """ Data type (class) for Hydra qdp files.
    Contains methods to read in and plot the files."""


    def read(self, data='' , nstep=10000):
        import numpy as np
    
    
        # Create arrays
        self.step=np.zeros(nstep)
        self.time=np.zeros(nstep)
        self.ke=np.zeros(nstep)
        self.the=np.zeros(nstep)
        self.pe=np.zeros(nstep)
        self.de=np.zeros(nstep)
        self.error=np.zeros(nstep)

    
        # Open data file
        qdpfile=open('../rundir/data/'+ data +'.qdp','r')
    
        # Read in lines one at a time and process

        istep=-1
        
    
        lines=qdpfile.readlines()
        qdpfile.close()

        for line in lines:
        #Reset counter if this is a header file else add to arrays
            if line.find('!')!= -1:
                istep=-1
            else:
                istep=istep+1
                words=line.split()
                self.step[istep]=words[0]
                self.time[istep]=words[1]
                self.ke[istep]=words[2]
                self.the[istep]=words[3]
                self.pe[istep]=words[4]
                self.de[istep]=words[5]
                self.error[istep]=words[6]
    
    
