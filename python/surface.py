class surface():
    """Class to read in a surface file and contain the data within, should be used in conjunction with method surface_height to find the height at a given point on the surface."""
    def read(self,file):
        import numpy as np
        import sys
        
        #Read in file
        surface=open('../rundir/data/'+file,mode='r')
        surfacelines=surface.readlines()
        surface.close()
        
        #Get keyword information and find location of arrays
        i=0
        for line in surfacelines:
            i+=1
            if 'location' in line:
                words=line.split( )
                self.location=words[1]
                continue
            elif 'order' in line:
                words=line.split( )
                self.order=words[1]
            elif 'ncols' in line:
                words=line.split( )
                self.ncols=int(words[1])
            elif 'nrows' in line:
                words=line.split( )
                self.nrows=int(words[1])
            elif 'xllcorner' in line:
                words=line.split( )
                self.xllcorner=float(words[1])
            elif 'yllcorner' in line:
                words=line.split( )
                self.yllcorner=float(words[1])
            elif 'cellsize' in line:
                words=line.split( )
                self.cellsize=float(words[1])
            elif 'nodata_value' in line:
                words=line.split( )
                self.nodata_value=float(words[1])
                break

        #Gain surface height array info

        templines=surfacelines[i:]

        # Determine order and assign cell data 
        if self.order=='row':
            self.cell=np.zeros((self.ncols,self.nrows))
            for i in range(0,len(templines)):
                words=np.array(templines[i].split( ))
                numbers=words.astype(float)
                self.cell[:,i]=numbers[:]


        elif self.order=='column':
            self.cell=np.zeros((self.nrows,self.ncols))
            for i in range(0,len(templines)):
                words=np.array(templines[i].split( ))
                numbers=words.astype(float)
                self.cell[i,:]=numbers[:]
            tmp=self.nrows
            self.nrows=self.ncols
            self.ncols=tmp

        else:
            sys.stderror.write('Surface file must specify order as column or row')


        #Determine x0 and y0
        if self.location=='centre':
            self.x0=self.xllcorner-self.cellsize
            self.y0=self.yllcorner-self.cellsize
        elif self.location=='corner':
            self.x0=self.xllcorner-0.5*self.cellsize
            self.y0=self.yllcorner-0.5*self.cellsize
        else:
            sys.stderror.write('Surface file must specify corner or centre for location')

        # Find the amount that the cell height changes by dx,dy,and dx*dy
            
        self.cell_dx=np.zeros((self.ncols-1,self.nrows-1))
        self.cell_dy=np.zeros((self.ncols-1,self.nrows-1))
        self.cell_dxdy=np.zeros((self.ncols-1,self.nrows-1))
        
        for j in range (0,self.nrows-1):
            for i in range (0,self.ncols-1):
                self.cell_dx[i,j]=((self.cell[i+1,j]-self.cell[i,j])
                                   /self.cellsize)
                self.cell_dy[i,j]=((self.cell[i,j+1]-self.cell[i,j])
                                   /self.cellsize)
                self.cell_dxdy[i,j]=((self.cell[i,j]+self.cell[i+1,j+1]
                                      -self.cell[i+1,j]-self.cell[i,j+1])
                                     /self.cellsize**2)
                            
    def surface_height(self,r,extrapolate='False'):
        import numpy as np
        import sys
        
# Function returns height of surface, self, at a given location specified by 
# r=[xcoordinate,ycoordinate]. Set extrapolate to 'True' if you wish function
# to extrapolate outside known cell area height.

        if extrapolate=='True':
            i=min(max(int((r[0]-self.x0)/self.cellsize)-1,0),self.ncols-1)
            j=min(max(int((r[1]-self.y0)/self.cellsize)-1,0),self.nrows-1)
            dx=r[0]-(self.x0+(i+1)*self.cellsize)
            dy=r[0]-(self.y0+(j+1)*self.cellsize)
            surface_height=(self.cell[i,j]+dx*self.cell_dx[i,j]
                            +dy*self.cell_dy[i,j]+dx*dy*self.cell_dxdy[i,j])
            return surface_height


        elif extrapolate=='False':
            i=int(r[0]-self.x0)/self.cellsize-1
            j=int(r[1]-self.y0)/self.cellsize-1
        
            if i<0 or i>self.ncols-1:
                sys.stderr.write("Error: asked to extrapolate from rows, specify extrapolate='True', or include more info in surface file")
            if j<0 or j>self.nrows-1:
                sys.stderr.write("Error: asked to extrapolate from columns specify extrapolate='True' or include more info in surface file")
        
            dx=r[0]-(self.x0+(1+i)*self.cellsize)
            dy=r[0]-(self.y0+(1+j)*self.cellsize)
            surface_height=(self.cell[i,j]+dx*self.cell_dx[i,j]
                        +dy*self.cell_dy[i,j]+dx*dy*self.cell_dxdy[i,j])
            return surface_height

        else:
            sys.stderr.write("Error: specify extrapolate='True' or extrapolate='False'")
