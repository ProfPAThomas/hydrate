from itype_def import *
import numpy as np
import matplotlib.pylab as py
import sys as sys

def plot_2d(data,limits=[],psym=',',color='b',overplot='False',itype=[],lgndlabel='_no_legend_',title='None',isotropic='box'):
    
    # Plots particle positions
    # data: hydra data structure


    # Determine whether to overplot

    if overplot=='False':
        py.figure()
        py.xlabel('x')
        py.ylabel('y')
        if title!='None':
            py.title(title)

        if isotropic=='box':
            py.axes().set_aspect('equal','box')
        elif isotropic=='equal':
            py.axis('equal')
        elif isotropic=='none':
            pass
        else:
            sys.stderr.write("Isotropic must be one of the following cases: 'equal','box' or 'none'")

        if len(limits)==4:
            py.xlim(limits[0],limits[1])
            py.ylim(limits[2],limits[3])
        elif len(limits)==0:
            pass
        else:
            sys.stderr.write("limits takes an array of length four, where the first two terms are the x limits, and the last two terms are the y limits")


    elif overplot=='True':
        pass
    else:
        sys.stderr.write("Choose overplot to be 'False' or 'True'")

    # Select particles to use based on itype

    index=np.where(data.itype!= itype_none)[0]
    if len(itype)!= 0:
        if itype< itype_low or itype> itype_high:
            sys.stderr.write("Invalid type")
        index=np.where(data.itype==itype)[0]

    x=data.pos[index,0]
    
    y=data.pos[index,1]
    
    py.plot(x,y,psym+color,label=lgndlabel)
