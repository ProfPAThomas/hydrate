import numpy as np
from scipy.ndimage import uniform_filter
import matplotlib.pylab as py
import sys
from itype_def import *
import copy as cp


def profile_linear(data,properties, surface='None', Extrapolate='False', 
                   axis=1, xlims=[], n_per_bin=17,overplot='False',psym='-',
                   color='b',ylims=[],itype=[],lgndlabel='_no_legend_',
                   title='None',isotropic='none'):

### Function creates a profile plot of the specified property.
### It also uses a surface class file, see surface.py, which reads in
### with the surface_height method is to find the surface height and
### apply a correction to the density parameter of the particles. 
### Extropolate can be set to be 'True' to enable the surface_height 
### extroplation option.  
### Axis is used to specify the direction or projection, xlims and ylims to set
### the limit on the x and y axis specifically, by default they are all data.
### n_per_bin is number of particles per bin, psym and color determine different
### symbols and colors used for the plot. Overplot can be set to be 'True' to 
### plot over an exisiting figure.
### itype can be chosen to select specific itypes.
### isotropic is used to set the axis to be equal in different ways, by default
### it is disabled.

    
    if properties not in ['a','b','c','d','h','v','w','x','y']:
        print 'Incorrect property list. Must choose one or more of:'
        print '    a - acceleration in projection direction'
        print '    b - extra array generated by save_debug flag'
        print '    d - density (from SPH)'
        #print '    e - energy (thermal +kinetic)'
        #print '    F - cumulative baryon fraction'
        #print '    G - cumulative gas fraction'
        print '    h - smoothing length (from SPH)'
        #print '    k - kinetic length (from SPH)'
        print '    v - velocity in projection direction'
        print '    w - vbar in projection direction'
        print '    x - vbar with curvature, 1'
        print '    y - vbar with curvature, 2'
        sys.stderr.write("usage/defaults: profile_linear(data,properties, axis=1, xlims=[], n_per_bin=17,overplot='False',psym='-',color='b',ylims=[],itype=[],lgndlabel='_no_legend_',title='None',isotropic='none')")


### Create a copy of the data object so we do not alter it.
    tmpdata=cp.deepcopy(data)

# Defines if we overplot
    
    if overplot=='False':
        py.figure()

# Range of data, by default over all the data

        if len(xlims)==0:
            xlims=[min(tmpdata.pos[:,axis-1]),max(tmpdata.pos[:,axis-1])]
        elif len(xlims)==2:
            pass
        else:
            sys.stderr.write('Wrong number of elements for xlims (must be 0 or 2)')

        if len(ylims)==0:
            pass
        elif len(ylims)==2:
            py.ylim(ylims[0],ylims[1])
        else:
            print 'Specify an array of two numbers for ylims'
        py.xlim(xlims[0],xlims[1])
    

# Labels axis based on direction of projection

        if axis==1:
            py.xlabel('x')
        elif axis==2:
            py.xlabel('y')
        else: 
            sys.stderr.write('axis must be 1 or 2 (defaults to 1)')

# Label Title if specified

        if title!='None':
            py.title(title)

#Change scale of the axis if isotropic specified

        if isotropic=='scaled':
            py.axis('scaled')
        elif isotropic=='equal':
            py.axis('equal')
        elif isotropic=='none':
            pass
        else:
            sys.stderr.write("Isotropic must be one of the following cases: 'equal','scaled' or 'none'")

    elif overplot== 'True':
        pass
    else:
        sys.stderr.write("You must choose overplot='False' or overplot='True'")


    # Number of particles per bin

    if n_per_bin!=round(n_per_bin) or n_per_bin%2==0:
        sys.stderr.write('n_per_bin should be an odd integer')
    if n_per_bin<1:
        sys.stderr.write('Need at least 1 particle per bin (defaults to 17)')
    n_per_half_bin=n_per_bin/2
    n_per_bin=2*n_per_half_bin+1

    # Select particles to use based on itype
    index=np.where(tmpdata.itype!=itype_none)[0]
    if len(itype)!=0:
        if itype<itype_low or itype>itype_high:
            sys.stderr.write('Invalid type')
        index=np.where(tmpdata.itype==itype)[0]

    # Sort particles along profile direction
    
    index=np.argsort(tmpdata.pos[index,axis-1])

    # Determine the x-values.

    x=tmpdata.pos[index,axis-1]

    #Extract desired data (y-values)

    y=np.zeros(len(index))
    
    if properties[0]== 'a':
        py.ylabel('acceleration')
        y=tmpdata.accel[index,axis-1]

    elif properties[0]=='b':
        py.ylabel('debug')
        y=tmpdata.debug[index]

    elif properties[0]=='d':
        py.ylabel('Height of Water Surface')
        index_data=np.where(index<len(tmpdata.density))[0]
        ndata=np.where(index>=len(tmpdata.density))[0]

### Floor correction
        if surface!='None':
            for i in range(0,len(index_data)):
                tmpdata.density[index[index_data[i]]]+=surface.surface_height(tmpdata.pos[index[index_data[i]],:],extrapolate=Extrapolate)
            py.ylabel('Height of Water Surface with Floor Correction')
        

        if len(index_data)>0:
            y[index_data]=tmpdata.density[index[index_data]]
        if len(ndata)>0:
            print 'Warning: property not defined for some data points'
            y[ndata]=np.nan

    elif properties[0]=='h':
        py.ylabel('SPH smoothing length')
        index_data=np.where(index<len(tmpdata.smth_len))[0]
        no_data=np.where(index>=len(tmpdata.smth_len))[0]
        if len(index_data) > 0:
            y[index_data]=tmpdata.smth_len[index_data]
        if len(no_data)>= 0:
            print 'Warning: property not defined for some data points'
            y[no_data]=np.nan

    elif properties[0]=='m':
        py.ylabel('M/V density')
        volume=np.product(round(np.max(tmpdata.pos,axis=0))-round(np.min(tmpdata.pos,axis=0)))
        area=volume/(round(np.max(tmpdata.pos[:,axis-1]))-round(np.min(tmpdata.pos[:,axis-1])))
        # To get correct normalisation omit central mass
        for i in range(n_per_half_bin,len(y)-n_per_half_bin-1):
            mass=sum(tmpdata.mass[index[i-n_per_half_bin:i+n_per_half_bin]])-tmpdata.mass[index[i]]
            y[i]=mass/(x[i+n_per_half_bin]-x[i-n_per_half_bin])/area
        y[0:n_per_half_bin-1]=y[n_per_half_bin]/area
        y[len(y)-n_per_half_bin:len(y)-1]=y[len(y)-n_per_half_bin-1]

    elif properties[0]=='v':
        py.ylabel('velocity')
        y=tmpdata.vel[index,axis-1]
    
    elif properties[0]=='w':
        py.ylabel('(<v><r!u4!n>-<vr!u2!n><r!u2!n>)/(<1><r!u4!n>-<r!u2!n>!u2!n')
        y=(tmpdata.avg_vel[index,axis-1]*tmpdata.avg_r4[index]-tmpdata.avg_vr2[index,axis-1]
            *tmpdata.avg_r2[index]/(tmpdata.avg_1[index]*tmpdata.avg_r4[index]
                                 -tmpdata.avg_r2[index]*tmpdata.avg_r2[index]))

    elif properties[0]=='x':
        py.ylabel('<v>')
        y=tmpdata.avg_v[index,axis-1]

    elif properties[0]=='y':
        py.ylabel('<v>/<1>')
        y=tmpdata.avg_v[index,axis-1]/(tmpdata.avg_1)[index]
    else:
        print 'Incorrect property list. Must choose one or more of:'
        print '    a - acceleration in projection direction'
        print '    b - extra array generated by save_debug flag'
        print '    d - density (from SPH)'
        #print '    e - energy (thermal +kinetic)'
        #print '    F - cumulative baryon fraction'
        #print '    G - cumulative gas fraction'
        print '    h - smoothing length (from SPH)'
        #print '    k - kinetic length (from SPH)'
        print '    v - velocity in projection direction'
        print '    w - vbar in projection direction'
        print '    x - vbar with curvature, 1'
        print '    y - vbar with curvature, 2'
        sys.stderr.write("usage/defaults: profile_linear(data,properties, axis=1, xlims=[], n_per_bin=17,overplot='False',psym='-',color='b',ylims=[],itype=[],lgndlabel='_no_legend_',title='None',isotropic='none')")

    #Smooth data and throw away unsmoothed bins
    if n_per_bin>2:
        x=uniform_filter(x,n_per_bin)
        x=x[n_per_half_bin:len(x)-n_per_half_bin-1]
        y=uniform_filter(y,n_per_bin,)
        y=y[n_per_half_bin:len(y)-n_per_half_bin-1]

    #Initialise plot
    py.plot(x,y,psym+color,label=lgndlabel)

    
