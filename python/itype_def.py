# itype_definitions

itype_low=-3
itype_none=-3       # non-existent particle
itype_gal=-2        # galaxy
itype_star=-1       # star
itype_dark=0        # dark matter
itype_gas=1         # gas (sph calculation pending)
itype_done=2        # gas (sph calculation complete)
itype_high=2
