module itype_definitions

! Note: it simplifies the book-keeping if all particles that need to take
! part in the SPH calculation are given positive types.  That is what we
! assume here.

  integer, parameter :: itype_low=0
  integer, parameter :: itype_none=0        ! non-existent particle
  integer, parameter :: itype_gas=1         ! sph (calculation pending)
  integer, parameter :: itype_done=2        ! sph (calculation complete)
  integer, parameter :: itype_boundary=3    ! sph (boundary)
  integer, parameter :: itype_high=3

end module itype_definitions
