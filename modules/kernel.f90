module kernel

  integer, parameter :: Nkernel=30000
  real :: wtab(0:Nkernel+1) ! Smoothing kernel
  real :: dwtab(0:Nkernel+1) ! Derivtive of smoothing kernel / x
  real :: wbtab(0:Nkernel+1) ! Smoothng kernel for bounce

contains

!-------------------------------------------------------------------

subroutine kernel_initialise()

! Creates lookup tables for the smoothing kernels and derivatives
! This version has a maximum extent of unity (thus differing from the
! previous version that went out to 2)

  ! Local variables
  integer :: itab
  real :: rad, rad2

  wtab(0)=kernel_w(0.)
  wbtab(0)=kernel_wb(0.)
  dwtab(0)=0.
  do itab=1,Nkernel
     rad2=float(itab)/float(Nkernel)
     rad=sqrt(rad2)
     wtab(itab)=kernel_w(rad)
     wbtab(itab)=kernel_wb(rad)
     dwtab(itab)=kernel_dw(rad)
  enddo
  wtab(Nkernel+1)=0.
  wbtab(Nkernel+1)=0.
  dwtab(Nkernel+1)=0.

end subroutine kernel_initialise

!--------------------------------------------------------------
 
real function kernel_w(x)

! Smoothing kernel in 2-d (only normalisation changes from 3-d)

  real, intent(in) :: x

  real, parameter :: pi=3.141592654, wnorm=40./(7.*pi) !wnorm=8./pi

  if (x.lt.0.0) then
     STOP 'kernel_w: invalid range for argument'
  else if (x.le.0.5) then
     kernel_w=wnorm*(1.-6.*x**2+6.*x**3)
  else if (x.le.1.0) then
     kernel_w=wnorm*2.*(1.-x)**3
  else
     STOP 'kernel_w: invalid range for argument'
  end if

end function kernel_w

!---------------------------------------------------------

real function kernel_dw(x)

! (1/r)(d/dr)smoothing kernel

! Note that the gradient of the kernel has been taken to be constant
! at small separations to prevent artificial clumping of particles.
! This is valid provided h does not change rapidly during the time
! that the particles approach then separate (ie the force is conservative).

  real, intent(in) :: x

  real, parameter :: pi=3.141592654, gnorm=-40./(7.*pi) !gnorm=-8./pi

  if (x.le.0.0) then
     STOP 'kernel_dw: invalid range for argument'
  else if (x.le.1./3.) then
     kernel_dw=gnorm*2./x
  else if (x.le.0.5) then
     kernel_dw=gnorm*6.*(2.-3.*x)
  else if (x.le.1.0) then
     kernel_dw=gnorm*6.*(1.-x)**2/x
  else
     STOP 'kernel_dw: invalid range for argument'
  end if

end function kernel_dw

!--------------------------------------------------------------
 
real function kernel_wb(x)

! Function describing repuslive force.
! Zero outside x=xmax
! Normalised to unity at x=xmin
! Set equal to huge(1.) within x=xmin as a flag
! Formula is ((xmax-x)/(xmax-xmin))^nbounce

  real, intent(in) :: x

  real, parameter :: bnorm=0.
  real, parameter :: xmin=0.5
  real, parameter :: xmax=0.85
  real, parameter :: nbounce=4

  if (x.lt.0.) then
     STOP 'kernel: invalid range for argument'
  else if (x.le.xmin) then
     kernel_wb=huge(1.)
  else if (x.le.xmax) then
     kernel_wb=((xmax-x)/(xmax-xmin))**nbounce
  else
     kernel_wb=0.
  end if

end function kernel_wb

!---------------------------------------------------------------------

end module kernel
