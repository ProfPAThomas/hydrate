module units

! Defines units of various physical quantities

  ! To prevent overflows, define a suitable kind_type
  integer, parameter :: big=selected_real_kind(14,200)
  
  ! Mathematical constants
  real(big), parameter :: tpi=6.283185307179
  real(big), parameter :: pi=tpi/2

  ! Physical constants in si units
  real(big), parameter :: const_g_si=9.81 ! gravitational accel.
  real(big), parameter :: const_rho_si=1000. ! density of water
  real(big) :: const_g, const_rho, const_frc

  ! Units
  real(big) :: units_mass_density_si=1000. ! mass density/(kg/m^3)
  real(big) :: units_length_si=1.          ! length/m
  real(big) :: units_time_si=1.            ! time/s
  real(big) :: units_mass_si               ! mass/kg
  real(big) :: units_surface_density_si    ! number density/(kg/m^2)
  real(big) :: units_depth_si              ! water depth (m)
  real(big) :: units_speed_si              ! speed,velocity/(m/s)
  real(big) :: units_specific_energy_si    ! specific energy/(J/kg)

  namelist /nml_units/units_mass_density_si,units_length_si,&
       &units_time_si,units_mass_si,units_surface_density_si,&
       &units_depth_si,units_speed_si,units_specific_energy_si,&
       &const_g,const_rho,const_frc

  namelist /nml_set_units/ units_length_si,units_mass_density_si,units_time_si

contains

!---------------------------------------------------------------------

subroutine initialise_units

! Evaluates units

  use run_parameters, only: verbosity
  use unit_numbers

  ! grav accel in code units
  const_g=const_g_si*units_time_si**2/units_length_si
  ! density of water in code units
  const_rho=const_rho_si/units_mass_density_si
  ! Magnitude of hydro force (tested to be correct)
  const_frc=0.5*const_g/const_rho
  ! Derived units
  units_mass_si=units_mass_density_si*units_length_si**3
  units_surface_density_si=units_mass_density_si*units_length_si
  units_depth_si=units_length_si/const_rho
  units_speed_si=units_length_si/units_time_si
  units_specific_energy_si=units_speed_si**2

  if (verbosity.gt.1) then
     write(lun_log,*) 'Units:'
     write(lun_log,*) 'length/m =',units_length_si
     write(lun_log,*) 'mass/kg =',units_mass_si
     write(lun_log,*) 'time/s =',units_time_si
     write(lun_log,*) 'speed/(m/s) =',units_speed_si
     write(lun_log,*) 'mass density/(kg/m^3) =',units_mass_density_si
     write(lun_log,*) 'surface density/(kg/m^2) =',units_surface_density_si
     write(lun_log,*) 'depth/m =',units_depth_si
     write(lun_log,*) 'specific energy/(m^2/s^2) =',units_specific_energy_si
     write(lun_log,*) 'gravity/(m/s^2) =',const_g_si
     write(lun_log,*) 'gravity/(code units) =',const_g
     write(lun_log,*) 'mean density/(kg/m^3) =',const_rho_si
     write(lun_log,*) 'mean density/(code units) =',const_rho
  end if

end subroutine initialise_units

!---------------------------------------------------------------

end module units
