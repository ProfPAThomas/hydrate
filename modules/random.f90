module random

! Code to generate pseudo-random numbers.

! subroutine ran_uniform_reset: regenerates the random number table.
! function ran_uniform: returns a uniform random number in the unit interval.

  private
  public :: ran_uniform_reset, ran_uniform

  ! Magic numbers that should not be changed
  integer, parameter :: ia=16807, im=2147483647, iq=127773, ir=2836
  
  ! Default seed in case none is specifed on first call
  integer, parameter :: iseed_default=1234321

  ! Need to save the state of the system between calls
  real, save :: am
  integer, save :: ix=-1, iy=-1

contains
  
subroutine ran_uniform_reset(iseed)

! Regenerates table using iseed

  integer, optional, intent(in) :: iseed
  integer :: iseed_use

  if (present(iseed)) then
     if (iseed.gt.0) then
        iseed_use=iseed
     else if (iseed.eq.0) then
        print*,'ran_uniform: iseed=0; reverting to default iseed'
        iseed_use=iseed_default
     else
        iseed_use=-iseed
     end if
  else
     print*,'ran_uniform: using default iseed'
     iseed_use=iseed_default
  end if
  
  am=nearest(1.0,-1.0)/im
  iy=ior(ieor(888889999,abs(iseed_use)),1)
  ix=ieor(777755555,abs(iseed_use))

end subroutine ran_uniform_reset

real function ran_uniform()

! Returns uniform random number from unit interval

  integer :: k

  ! if table has not yet been generated then do so now
  if (iy.lt.0) call ran_uniform_reset()
  
  ! Perform shift and generate new random number
  ix=ieor(ix,ishft(ix,13))
  ix=ieor(ix,ishft(ix,-17))
  ix=ieor(ix,ishft(ix,5))
  k=iy/iq
  iy=ia*(iy-k*iq)-ir*k
  if (iy .lt. 0) iy=iy+im
  ran_uniform=am*ior(iand(im,ieor(ix,iy)),1)

end function ran_uniform

end module random
