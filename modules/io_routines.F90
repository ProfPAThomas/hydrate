module io_routines

private
public :: check_flags, dump_data, read_data, read_nml

contains

!---------------------------------------------------------------------

subroutine check_flags

! Checks important compilation flags for consistency with data
! Should give a runtime read error if the flags are inconsistent with
! those in the header file.
! If istep=0 then set flags instead
! Also reads in header data for some optional flags

  use run_parameters
  use unit_numbers

  character :: infile*60
  integer :: ier

  if (istep.eq.0) then

#ifdef ISOLATED
     flag_isolated=.true.
     if (verbosity.gt.1) write(lun_log,*) 'Compiled using -DISOLATED'
#endif
#ifdef PERIODIC
     flag_periodic=.true.
     if (verbosity.gt.1) write(lun_log,*) 'Compiled using -DPERIODIC'
#endif

  else

  ! header data (parameters and variables)
     infile='data/'//datafile_start//'.head'
     open(lun_read,file=infile,status='old')

#ifdef ISOLATED
     call read_nml(lun_read,'nml_isolated',ier)
     if (.not.flag_isolated) &
          &STOP 'Data not generated using ISOLATED'
     if (verbosity.gt.1) write(lun_log,*) 'Compiled using -DISOLATED'
#endif
#ifdef PERIODIC
     call read_nml(lun_read,'nml_periodic',ier)
     if (.not.flag_periodic) &
          &STOP 'Data not generated using PERIODIC'
     if (verbosity.gt.1) write(lun_log,*) 'Compiled using -DPERIODIC'
#endif

     close(lun_read)

  end if

end subroutine check_flags

!---------------------------------------------------------------------

subroutine dump_data

! writes out data file
! ideally should use something like HDF5 but until then we will split
! the data into two files.

  use particle_data
  use run_parameters
  use unit_numbers
  use units
  use sph_parameters

  character :: outfile*60

  ! header data (parameters and variables)
  outfile='data/'//datafile//'.head'
  open(lun_dump,file=outfile)
  write(lun_dump,nml=nml_parameters)
  write(lun_dump,nml=nml_sph_parameters)
  write(lun_dump,nml=nml_variables)
  write(lun_dump,nml=nml_units)
#ifdef ISOLATED
  write(lun_dump,nml=nml_isolated)
#endif
#ifdef PERIODIC
  write(lun_dump,nml=nml_periodic)
#endif
  close(lun_dump)

  ! particle data
  outfile='data/'//datafile//'.data'
  open(lun_dump,file=outfile,form='unformatted')
  write(lun_dump) itype
  write(lun_dump) rm
  write(lun_dump) r
  write(lun_dump) v
  write(lun_dump) h
  write(lun_dump) dn
#ifdef SAVE_ACCEL
  write(lun_dump) a
#endif
#ifdef SPH_FLOOR
  write(lun_dump) fl
#endif
#ifdef SAVE_DEBUG
  write(lun_dump) debug
#endif
#ifdef TEST
  write(lun_dump) nnsep
#ifdef DAMP
  write(lun_dump) avg_1
  write(lun_dump) avg_r2
  write(lun_dump) avg_r4
  write(lun_dump) avg_v
  write(lun_dump) avg_vr2
#endif
#endif
  close(lun_dump)

end subroutine dump_data

!---------------------------------------------------------------------

subroutine read_data

! reads in data file from start file
! ideally should use something like HDF5 but until then we will split
! the data into two files.

  use particle_data
  use run_parameters
  use sph_parameters
  use unit_numbers
  use units

  character :: infile*60
  integer :: irun_new

  irun_new=irun

  ! header data (parameters and variables)
  infile='data/'//datafile_start//'.head'
  open(lun_read,file=infile,status='old')
  read(lun_read,nml=nml_parameters)
  read(lun_read,nml=nml_sph_parameters)
  read(lun_read,nml=nml_variables)
  read(lun_read,nml=nml_units)
#ifdef ISOLATED
  read(lun_read,nml=nml_isolated)
#endif
#ifdef PERIODIC
  read(lun_read,nml=nml_periodic)
#endif
  close(lun_read)

  ! New run number has already been read in.  Print warning if it differs
  ! from the one in the data header file.
  if (irun.ne.irun_new) write(lun_err,*) '**Warning: irun changed**'
  irun=irun_new

  ! particle data
  infile='data/'//datafile_start//'.data'
  print*,'infile=',infile
  open(lun_read,file=infile,form='unformatted',status='old')
  allocate(itype(Nobj)); read(lun_read) itype
  allocate(rm(Nobj));    read(lun_read) rm
  allocate(r(2,Nobj));   read(lun_read) r
  allocate(v(2,Nobj));   read(lun_read) v
  allocate(a(2,Nobj))
  allocate(h(Nbary));     read(lun_read) h
  allocate(dn(Nbary));    read(lun_read) dn
  ! No need to read in the following variables - they are diagnostic output
#ifdef SPH_FLOOR
  allocate(fl(Nbary))
#endif
#ifdef SAVE_DEBUG
  allocate(debug(Nobj))
#endif
#ifdef TEST
  allocate(nnsep(Nobj))
#ifdef DAMP
  allocate(avg_1(Nobj))
  allocate(avg_r2(Nobj))
  allocate(avg_r4(Nobj))
  allocate(avg_v(2,Nobj))
  allocate(avg_vr2(2,Nobj))
#endif
#endif
  close(lun_read)

end subroutine read_data

!---------------------------------------------------------------------

subroutine read_nml(lun,nml,ier)

! Reads namelist.
! Subroutine call is necessary because the order of namelists may be unknown.
! It seems that variable names are not allowed in namelist read statements,
! so I have had to resort to a case statement that lists all the possibilities.

  use run_parameters
  use units
  use sph_parameters

  integer, intent(in) :: lun   ! unit number of namelist file
  character, intent(in) :: nml*(*)      
  integer, intent(out) :: ier  ! Error flag: 0 success, 
                               !            -1 end of file
                               !            -2 namelist not recognised
                               !               (currently causes STOP)

  ier=-3
  rewind(lun)
  do while (ier.ne.0)
     select case (nml)
        case ('nml_control_parameters')
           read(lun,nml=nml_control_parameters,iostat=ier,end=10)
        case ('nml_set_units')
           read(lun,nml=nml_set_units,iostat=ier,end=10)
#ifdef ISOLATED
        case ('nml_isolated')
           read(lun,nml=nml_isolated,iostat=ier,end=10)
#endif
#ifdef PERIODIC
        case ('nml_periodic')
           read(lun,nml=nml_periodic,iostat=ier,end=10)
#endif
        case ('nml_sph_control_parameters')
           read(lun,nml=nml_sph_control_parameters,iostat=ier,end=10)
        case ('nml_startfile')
           read(lun,nml=nml_startfile,iostat=ier,end=10)
#if defined TRACK_PARTICLE || TRACE_ACCEL
        case ('nml_track_particle')
           read(lun,nml=nml_track_particle,iostat=ier,end=10)
#endif
        case ('nml_verbosity')
           read(lun,nml=nml_verbosity,iostat=ier,end=10)
#ifdef BOUNCE
	case ('nml_bounce_parameters')
           read(lun,nml=nml_bounce_parameters,iostat=ier,end=10)
#endif
        case default
           ier=-2 
           print*,'read_nml: namelist ',nml,' not recognised'
           STOP
     end select   
  end do
! return here on successful read
  return

! return here if requested namelist not found
10 ier=-1; return

end subroutine read_nml

!---------------------------------------------------------------------

end module io_routines
